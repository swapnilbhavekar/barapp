import React, { Component } from 'react';
import { I18nManager, View, Text, TouchableOpacity, SafeAreaView, TextInput, Dimensions, Keyboard, UIManager, Animated } from 'react-native';
import { reusableStyles } from './Helpers/CommonStyles.js'
import SplashScreen from './Components/SplashScreen.js';
import Signup from './Components/Signup.js';
import Login from './Components/Login.js';
import ForgotPassword from './Components/ForgotPassword.js';
import ResetPassword from './Components/ResetPassword.js';
import VerificationCode from './Components/VerificationCode.js';
import MenuViewer from './Components/MenuViewer.js';
import ImageSlider from './Components/ImageSlider.js';
import Home from './Components/Home/Home';
import BrowseCuisines from './Components/BrowseCuisines.js';
import SearchLanding from './Components/SearchLanding.js';
import SearchLocation from './Components/SearchLocation.js';
import SearchTable from './Components/SearchTable.js';
import SearchRestaurants from './Components/SearchRestaurants.js';
import RestaurantListing from './Components/RestaurantListing.js';
import RestaurantDetails from './Components/RestaurantDetails.js';
import Profile from './Components/Profile.js';
import Notifications from './Components/Notifications';
import Settings from './Components/Settings.js';
import BookingTable from './Components/BookingTable.js';
import Booking from './Components/Booking.js';
import BookingConfirmation from './Components/BookingConfirmation.js';
import CustomWebView from './Components/CustomWebView.js';
import CodePush from 'react-native-code-push';
import {createAppContainer, createStackNavigator } from 'react-navigation';
import MapViewComponent from './Components/MapViewComponent.js';
import Favourites from './Components/Favourites';

const KB_SHIFT = new Animated.Value(0);

const { State: TextInputState } = TextInput;

console.reportErrorsAsExceptions = false;

const RootStack = createStackNavigator({
  SplashScreen: {
    screen: SplashScreen,
    navigationOptions: {
      title: '',
      headerTitleStyle: { color: '#fff' },
      headerStyle: reusableStyles.blkTop,
    }
  },
  Home: {
    screen: Home,
    navigationOptions: {
      title: '',
      headerTitleStyle: { color: '#fff' },
      headerStyle: reusableStyles.blkTop,
    }
  },
  BrowseCuisines: {
    screen: BrowseCuisines,
    navigationOptions: {
      title: '',
      headerTitleStyle: { color: '#fff' },
      headerStyle: reusableStyles.blkTop,
    }
  },
  SearchLanding: {
    screen: SearchLanding,
    navigationOptions: {
      title: '',
      headerTitleStyle: { color: '#fff' },
      headerStyle: reusableStyles.blkTop,
    }
  },
  SearchLocation: {
    screen: SearchLocation,
    navigationOptions: {
      title: '',
      headerTitleStyle: { color: '#fff' },
      headerStyle: reusableStyles.blkTop,
    }
  },
  SearchTable: {
    screen: SearchTable,
    navigationOptions: {
      title: '',
      headerTitleStyle: { color: '#fff' },
      headerStyle: reusableStyles.blkTop,
    }
  },
  SearchRestaurants: {
    screen: SearchRestaurants,
    navigationOptions: {
      title: '',
      headerTitleStyle: { color: '#fff' },
      headerStyle: reusableStyles.blkTop,
    }
  },
  ForgotPassword: {
    screen: ForgotPassword,
    navigationOptions: {
      title: '',
      headerTitleStyle: { color: '#fff' },
      headerStyle: reusableStyles.blkTop,
    }
  },
  Signup: {
    screen: Signup,
    navigationOptions: {
      title: '',
      headerTitleStyle: { color: '#fff' },
      headerStyle: reusableStyles.blkTop,
    }
  },
  ResetPassword: {
    screen: ResetPassword,
    navigationOptions: {
      title: '',
      headerTitleStyle: { color: '#fff' },
      headerStyle: reusableStyles.blkTop,
    }
  },
  Login: {
    screen: Login,
    navigationOptions: {
      title: '',
      headerTitleStyle: { color: '#fff' },
      headerStyle: reusableStyles.blkTop,
    }
  },
  VerificationCode: {
    screen: VerificationCode,
    navigationOptions: {
      title: '',
      headerTitleStyle: { color: '#fff' },
      headerStyle: reusableStyles.blkTop,
    }
  },
  RestaurantListing: {
    screen: RestaurantListing,
    navigationOptions: {
      title: '',
      headerTitleStyle: { color: '#fff' },
      headerStyle: reusableStyles.blkTop,
    }
  }, 
  RestaurantDetails: {
    screen: RestaurantDetails,
    navigationOptions: {
      title: '',
      headerTitleStyle: { color: '#fff' },
      headerStyle: reusableStyles.blkTop,
    }
  },
  BookingTable: {
    screen: BookingTable,
    navigationOptions: {
      title: '',
      headerTitleStyle: { color: '#fff' },
      headerStyle: reusableStyles.blkTop,
    }
  },
  MenuViewer: {
    screen:MenuViewer,
    navigationOptions: {
      title: '',
      headerTitleStyle :{color:'#fff'},
      headerStyle: reusableStyles.blkTop,
    }
  },
  ImageSlider: {
    screen:ImageSlider,
    navigationOptions: {
      title: '',
      headerTitleStyle :{color:'#fff'},
      headerStyle: reusableStyles.blkTop,
    }
  },
  Booking: {
    screen:Booking,
    navigationOptions: {
      title: '',
      headerTitleStyle :{color:'#fff'},
      headerStyle: reusableStyles.blkTop,
    }
  },
  BookingConfirmation: {
    screen:BookingConfirmation,
    navigationOptions: {
      title: '',
      headerTitleStyle :{color:'#fff'},
      headerStyle: reusableStyles.blkTop,
    }
  },
  Settings: {
    screen:Settings,
    navigationOptions: {
      title: '',
      headerTitleStyle :{color:'#fff'},
      headerStyle: reusableStyles.blkTop,
    }
  },
  Profile: {
    screen:Profile,
    navigationOptions: {
      title: '',
      headerTitleStyle :{color:'#fff'},
      headerStyle: reusableStyles.blkTop,
    }
  },
  Notifications: {
    screen:Notifications,
    navigationOptions: {
      title: '',
      headerTitleStyle :{color:'#fff'},
      headerStyle: reusableStyles.blkTop,
    }
  },
  CustomWebView: {
    screen:CustomWebView,
    navigationOptions: {
      title: '',
      headerTitleStyle :{color:'#fff'},
      headerStyle: reusableStyles.blkTop,
    }
  },
  MapViewComponent: {
    screen:MapViewComponent,
    navigationOptions: {
      title: '',
      headerTitleStyle :{color:'#fff'},
      headerStyle: reusableStyles.blkTop,
    }
  },
  Favourites: {
    screen:Favourites,
    navigationOptions: {
      title: '',
      headerTitleStyle :{color:'#fff'},
      headerStyle: reusableStyles.blkTop,
    }
  },
}, {
    headerMode: 'none',
    initialRouteName: 'SplashScreen'
  }
);

const App = createAppContainer(RootStack);

export default class BookAResto extends React.Component {

  state = {
    keyboardDidShow : false
  }

  componentWillMount() {
    this.keyboardDidShowSub = Keyboard.addListener('keyboardDidShow', this.handleKeyboardDidShow);
    this.keyboardDidHideSub = Keyboard.addListener('keyboardDidHide', this.handleKeyboardDidHide);
  }

  componentWillUnmount() {
    this.keyboardDidShowSub.remove();
    this.keyboardDidHideSub.remove();
  }

  componentDidMount() {
    console.log('splash screen to');
  }

  handleKeyboardDidShow = (event) => {

    this.setState({
      keyboardDidShow : true
    });

    const { height: windowHeight } = Dimensions.get('window');
    const keyboardHeight = event.endCoordinates.height;
    const currentlyFocusedField = TextInputState.currentlyFocusedField();
    UIManager.measure(currentlyFocusedField, (originX, originY, width, height, pageX, pageY) => {
      const fieldHeight = height;
      const fieldTop = pageY;
      let gap = (windowHeight - keyboardHeight) - (fieldTop + fieldHeight);
      if (gap >= 0) {
        return;
      } else {
        gap -= 10;
      }
      Animated.timing(
        KB_SHIFT,
        {
          toValue: gap,
          duration: 500
        }
      ).start();
    });
  }

  handleKeyboardDidHide = () => {
    this.setState({
      keyboardDidShow : false
    });
    Animated.timing(
      KB_SHIFT,
      {
        toValue: 0,
        duration: 500
      }
    ).start();
  }
  async changeLang() {
    const IS_RTL = I18nManager.isRTL;
    console.log(IS_RTL);

    if (IS_RTL) {
      I18nManager.forceRTL(false);
    } else {
      I18nManager.forceRTL(true);
    }
    CodePush.restartApp();
  }
  render() {
    // this.changeLang(true);
    return (
      <Animated.View style={{flex:1,position:'relative'}}>
        <App style={{backgroundColor:'#333',flex:1}} screenProps={{ IS_RTL: false, keyboardDidShow: this.state.keyboardDidShow }} />
        {/* // <SafeAreaView style={{flex:1,position:'relative'}}>
        //   <TouchableOpacity onPress={this.changeLang.bind(this)}>
        //     <Text style={{position:'relative',zIndex:999,backgroundColor:'#ffffff',top:10,left:10,width:200,height:50}}>Toggle RTL</Text>
        //   </TouchableOpacity>
        //   <App screenProps={{IS_RTL:true}}/>
        // </SafeAreaView> */}
      </Animated.View>
    )
  }
}

