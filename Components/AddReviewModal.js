import React, { Component } from 'react';
import { Text, View, Image, Linking, ScrollView, TouchableOpacity, ImageBackground, SafeAreaView, StyleSheet } from 'react-native';
import { reusableStyles } from '../Helpers/CommonStyles.js';
import { TextField } from 'react-native-material-textfield';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import iconmoonConfig from '../selection.json';
const Icon = createIconSetFromIcoMoon(iconmoonConfig);
import I18n from '../Helpers/i18n';
import { getLanguages } from 'react-native-i18n';
import ToastUtils from "../Helpers/ToastUtils";
import constants from "../Helpers/AppConstants";
import RequestHelper from '../Helpers/RequestHelper.js'
import NetworkHelper from '../Helpers/NetworkHelper.js'
import Logo from '../assets/images/book-a-resto-logo-white.png';
import Slide from '../assets/images/slide1.jpg';
import BottomNav from './BottomNav/BottomNav';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { TextInput } from 'react-native-gesture-handler';

var themeRed = "#f01616",
    themeGrey = "#a3a3a3",
    themelightGrey = "#595959",
    fnt1 = 'OpenSans-Regular',
    fnt2 = 'OpenSans-Bold';
fnt3 = 'OpenSans-SemiBold';

const IS_RTL = false;

const styles = StyleSheet.create({
    mainWrapper: {
        flex:1,
        justifyContent: 'flex-start',
        paddingHorizontal: 20,
    },
    restoNamewrapper:{
        marginTop:30,
    },
    restoName:{
        fontFamily:fnt2,
        fontSize: 18,
        color:'#000000'
    },
    restoAddress:{
        fontFamily:fnt1,
        fontSize: 12,
        color:'#000000'
    },
    ratingWrapper:{
        marginTop:30,
    },
    ratingLabel:{
        fontFamily:fnt2,
        fontSize: 12,
        color:'#000000',
        marginBottom:10
    },
    ratingStarWrapper:{
        flexDirection:'row',
        alignItems: 'center',
    },
    ratingStarBtn:{
        marginRight: 10
    },
    ratingStarIcon:{
        color:'#000000',
        fontSize:20
    },
    reviewWrapper:{
        marginTop: 30
    },
    reviewLabel:{
        fontFamily:fnt2,
        fontSize: 12,
        color:'#000000',
        marginBottom:10
    },
    reviewInput:{
        borderColor:'#e3e3e3',
        borderWidth: 1,
        height:80
    },
    uploadWrapper:{
        marginTop: 30,
        alignItems:'flex-start'
    },
    uploadBtn:{
        backgroundColor:themeRed,
        padding: 15,
        borderRadius: 5,
    },
    uploadIcon:{
        fontSize:15,
        color:'#ffffff'
    }
});

export default class ReviewsModal extends React.Component {
    render() {
        return(
        <View style={styles.mainWrapper}>
            <ScrollView>
                <View style={styles.restoNamewrapper}>
                    <Text style={styles.restoName}>Jumjoji - The Parsi Diner</Text>
                    <Text style={styles.restoAddress}>Riyadh, Saudi Arabia</Text>
                </View>
                <View style={styles.ratingWrapper}>
                    <Text style={styles.ratingLabel}>Rate Your Experience</Text>
                    <View style={styles.ratingStarWrapper}>
                        <TouchableOpacity style={styles.ratingStarBtn}>
                            <Icon name="rating" style={styles.ratingStarIcon}></Icon>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.ratingStarBtn}>
                            <Icon name="rating" style={styles.ratingStarIcon}></Icon>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.ratingStarBtn}>
                            <Icon name="rating" style={styles.ratingStarIcon}></Icon>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.ratingStarBtn}>
                            <Icon name="rating" style={styles.ratingStarIcon}></Icon>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.ratingStarBtn}>
                            <Icon name="rating" style={styles.ratingStarIcon}></Icon>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={styles.reviewWrapper}>
                    <Text style={styles.reviewLabel}>Start Writing Your Review</Text>
                    <TextInput style={styles.reviewInput} multiline={true} numberOfLines={4}></TextInput>
                </View>
                <View style={styles.uploadWrapper}>
                    <TouchableOpacity style={styles.uploadBtn}>
                        <Icon name="camera" style={styles.uploadIcon}></Icon>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        </View>
        )
    }
}