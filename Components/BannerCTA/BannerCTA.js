import React, { Component } from 'react';
import { ImageBackground, StyleSheet, Dimensions, TouchableOpacity, View, Text } from 'react-native';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import iconmoonConfig from '../../selection.json';
import I18n from '../../Helpers/i18n';
import Urls from '../../Helpers/Helpers.js'


var themeRed = "#f01616",
    themeGrey = "#a3a3a3",
    themelightGrey = "#595959",
    fnt1 = 'OpenSans-Regular',
    fnt2 = 'OpenSans-Bold';
fnt3 = 'OpenSans-SemiBold';

const { width, height } = Dimensions.get('window');

const IS_RTL = true;


const styles = StyleSheet.create({
    banner: {
        width: width - 40,
        paddingVertical: 40,
        paddingHorizontal: 20,
        marginBottom: 30,
        borderRadius: 10
    },
    bannerTitle: {
        fontFamily: fnt2,
        fontSize: 24,
        color: '#ffffff',
        lineHeight: 30,
        marginBottom: 20,
        textAlign: IS_RTL ? 'left' : 'right'
    },
    btnCTA: {
        paddingVertical: 10,
        paddingHorizontal: 14,
        backgroundColor: '#ed1c24',
        alignSelf: 'flex-start',
        borderRadius: 5
    },
    btnCTAText: {
        fontFamily: fnt2,
        color: '#ffffff',
        fontSize: 12,
        textTransform: 'uppercase',
    }
})

export default class BannerCTA extends React.Component {

    learnMoreClicked()
    {
        this.props.navigation.navigate('CustomWebView',{url : Urls.learnMoreUrl})
    }

    render() {
        return (
            <ImageBackground source={require('../../assets/images/HomeBanner.png')}
                imageStyle={{ borderRadius: 5 }}
                style={styles.banner}>
                <View>
                    <Text style={styles.bannerTitle}>{I18n.t('homeBannerCTAText')}</Text>
                    <TouchableOpacity activeOpacity={0.9} style={styles.btnCTA} onPress={() => this.learnMoreClicked()}>
                        <Text style={styles.btnCTAText}>{I18n.t('homeBannerCTAViewMoreText')}</Text>
                    </TouchableOpacity>
                </View>
            </ImageBackground>
        )
    }
}