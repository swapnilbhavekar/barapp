import React, { Component } from 'react';
import { Text, View, Image, Linking, ScrollView, TouchableOpacity, SafeAreaView, Animated, StyleSheet, ImageBackground, Dimensions, Picker } from 'react-native';
import { reusableStyles } from '../Helpers/CommonStyles.js';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import iconmoonConfig from '../selection.json';
const Icon = createIconSetFromIcoMoon(iconmoonConfig);
import I18n from '../Helpers/i18n';
import { getLanguages } from 'react-native-i18n';
import BottomNav from './BottomNav/BottomNav';
import { TextField } from 'react-native-material-textfield';
import { Dropdown } from 'react-native-material-dropdown';
import RestoBarModal from './RestoBarModal.js';


var themeRed = "#f01616",
    themeGrey = "#a3a3a3",
    themelightGrey = "#595959",
    lightRed = "#f8a4a7",
    fnt1 = 'OpenSans-Regular',
    fnt2 = 'OpenSans-Bold';
fnt3 = 'OpenSans-SemiBold';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
    header: {
        height: 180
    },
    filter: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    filterIconWrapper: {
        paddingHorizontal: 15,
    },
    filterIcon: {
        color: '#fff',
    },
    ratingWrapper: {
        width: 35,
        height: 16,
        flexDirection: 'row',
        alignItems: 'flex-start',
        paddingLeft: 10
    },
    ratingIcon: {
        width: 10,
        height: 10
    },
    ratingText: {
        color: '#fff',
        fontFamily: fnt1,
        fontSize: 12,
        marginRight: 5
    },
    HeaderRestoDetails: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-end',
        paddingTop: 70,
        paddingLeft: 20,
        paddingBottom: 10
    },
    restoAddressWrapper: {

    },
    restoAddressName: {
        color: '#fff',
        fontFamily: fnt2,
        fontSize: 16,
        marginBottom: 10,
    },
    restoTags: {
        color: '#fff',
        fontFamily: fnt1,
        fontSize: 12,
        fontStyle: 'italic',
    },
    blockSubTitle: {
        fontFamily: fnt2,
        fontSize: 10,
    },
    restoActions: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: 15
    },
    restoActionItem: {
        borderRightWidth: 1,
        borderRightColor: '#e8e8e8',
        paddingRight: 15,
        paddingLeft: 15,
    },
    restoActionItemBtn: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    restoActionItemIcon: {
        fontSize: 14,
        lineHeight: 14,
        margin: 5
    },
    restoActionItemText: {
        color: themeRed,
        fontFamily: fnt2,
        fontSize: 10,
        textTransform: 'uppercase'
    },
    separator: {
        width: width - 40,
        height: 2,
        backgroundColor: '#e8e8e8',
        marginBottom: 20
    },
    accordion: {
        marginBottom: 20,
    },
    accordionTitle: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingBottom: 5
    },
    accordionTitleText: {
        color: themeRed,
        fontFamily: fnt2,
        fontSize: 12,
        marginRight: 10
    },
    accordionTitleicon: {
        width: 10,
        height: 10
    },
    accordionBody: {
        paddingTop: 20,
        overflow: 'hidden'
    },
    timingWrapper: {
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    timingToggleWrapper: {
        marginRight: 10,
        marginBottom: 10
    },
    timingToggle: {
        paddingVertical: 5,
        paddingHorizontal: 10,
        backgroundColor: '#fff',
        borderColor: themeRed,
        borderWidth: 1,
        borderRadius: 5
    },
    timingText: {
        color: themeRed,
        fontFamily: fnt1,
        fontSize: 10
    },
    timingOfferText: {
        color: themeRed,
        fontSize: 10,
        marginTop: 3,
        paddingLeft: 7
    },
    albumBlock: {
        marginBottom: 20,
        width: width - 40
    },
    albumWrapper: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    albumRight: {
        marginLeft: 10,
    },
    albumBottom: {
        marginTop: 10,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    albumItem: {
        flex: 1,
        position: 'relative',
        borderRadius: 5,
        overflow: 'hidden'
    },
    albumItemCategory: {
        position: 'absolute',
        flex: 1,
        left: 0,
        bottom: 0,
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
        paddingVertical: 3,

    },
    albumItemCategoryText: {
        color: '#ffffff',
        fontFamily: fnt1,
        fontSize: 10,
        textAlign: 'center'
    },
    albumItemFoods: {
        width: width - 225,
        height: 100,
    },
    albumItemAmbience: {
        width: width - 200,
        height: 50,
    },
    albumItemAll: {
        width: width - 260,
        height: 40,
    },
    albumUpload: {
        width: width - 325,
        height: 40,
        backgroundColor: themeRed,
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
        borderRadius: 5
    },
    albumUploadIcon: {
        color: '#fff'
    },
    cusineItem: {
        paddingHorizontal: 15,
        paddingVertical: 3,
        backgroundColor: '#f4f4f4',
        borderRadius: 10,
        marginRight: 10
    },
    cusineItemWrapper: {
        flexDirection: 'row',
        marginBottom: 20
    },
    cusineItemText: {
        fontFamily: fnt1,
        fontSize: 10,
        color: themeRed
    },
    infoItemsWrapper: {
        flexDirection: 'row',
        flexWrap: 'wrap'
    },
    infoItem: {
        flexDirection: 'row',
        alignItems: 'center',
        width: width / 2 - 20,
        marginBottom: 10
    },
    infoItemIcon: {
        width: 4,
        height: 8,
        marginRight: 10
    },
    infoItemText: {
        fontFamily: fnt1,
        fontSize: 12
    },

    // Booking
    whiteBand: {
        backgroundColor: '#fff',
        borderRadius: 5,
        padding: 20,
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: -80,
        marginBottom: 20,
        shadowColor: "#fff1ee",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    },
    hotelHeader: {
        flex: 3,
        flexDirection: 'column',
        alignItems: 'flex-start',
        justifyContent: 'center',
        borderRightWidth: 1,
        borderRightColor: '#ececec',
    },
    featuresContainer: {
        flex: 1,
        flexDirection: 'row',
    },
    hotelFeatures: {
        marginBottom: 10,
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
    },
    hotelFeatureIcon: {
        fontSize: 14,
        lineHeight: 14,
    },
    hotelFeatureText: {
        fontFamily: fnt1,
        fontSize: 12,
        lineHeight: 12,
        color: '#000',
        marginLeft: 10
    },
    hotelHeading: {
        fontFamily: fnt2,
        fontSize: 16,
        lineHeight: 16,
        color: '#000',
        marginBottom: 10,
    },
    textBlock: {
        width:'100%',
        borderBottomWidth: 1,
        borderBottomColor: '#f8f8f8',
        paddingBottom: 20,
        marginBottom: 20
    },
    blockTitle: {
        fontFamily: fnt2,
        fontSize: 12,
        lineHeight: 12,
        color: '#000',
        marginBottom: 10,
    },
    blockText: {
        fontFamily: fnt1,
        fontSize: 12,
        lineHeight: 16,
        color: '#000',
        marginBottom: 5
    },
    slideViewMore: {
        fontFamily: fnt2,
        marginTop: 20,
        color: '#ed1c24',
        textTransform: 'uppercase'
    }

});


export default class Booking extends React.Component {
    constructor() {
        super();
        this.state = {
            email: '',
            mobileno: ''
        }
    }

    toggleSwitch1 = (value) => {
        this.setState({ switch1Value: value })
        console.log('Switch 1 is: ' + value)
    }

    state = {
        //accordHeight: new Animated.Value(),
        accordMaxHeight: 0,
        accordionExpanded: true,
        heightFlag: false,
        modalVisible: false
    }

    setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }

    // _setmaxHeight(event) {
    //     if (!this.state.heightFlag) {
    //         this.setState({
    //             heightFlag: true,
    //             accordMaxHeight: event.nativeEvent.layout.height
    //         });
    //     }
    // }

    // toggleAccordion() {

    //     let height = this.state.accordionExpanded ? 0 : this.state.accordMaxHeight;

    //     this.setState({
    //         accordionExpanded: !this.state.accordionExpanded
    //     });

    //     this.state.accordHeight.setValue(height);
    //     Animated.spring(     //Step 4
    //         this.state.accordHeight,
    //         {
    //             toValue: height
    //         }
    //     ).start();
    // }

    render() {
        //let accordHeight = this.state.accordHeight;
        let { email } = this.state;
        let { mobileno } = this.state;
        let data = [{
            value: 'Banana',
          }, {
            value: 'Mango',
          }, {
            value: 'Pear',
          }];


        return (
            <View style={{ ...reusableStyles.mainWrapper }}>
                <ScrollView>
                    <ImageBackground source={require('../assets/images/restaurant-details.png')} style={styles.header}>
                        <TouchableOpacity>
                            <Icon name="arrow-left" color="#ffffff" style={{ marginLeft: 15, marginTop: 55, fontSize: 20, lineHeight: 20, }}></Icon>
                        </TouchableOpacity>
                    </ImageBackground>
                    <View style={reusableStyles.scrollWrapper}>
                        <View style={styles.whiteBand}>
                            <View style={styles.hotelHeader}>
                                <Text style={styles.hotelHeading}>Jumjoji - The Parsi Diner</Text>
                                <View style={styles.featuresContainer}>
                                    <View style={styles.hotelFeatures}>
                                        <Icon name="user" color="#c5c5c5" style={styles.hotelFeatureIcon}></Icon>
                                        <Text style={styles.hotelFeatureText}>Table 02</Text>
                                    </View>
                                    <View style={styles.hotelFeatures}>
                                        <Icon name="time" color="#c5c5c5" style={styles.hotelFeatureIcon}></Icon>
                                        <Text style={styles.hotelFeatureText}>9.30 pm</Text>
                                    </View>
                                </View>
                                <View style={styles.featuresContainer}>
                                    <View style={[styles.hotelFeatures, { marginBottom: 0 }]}>
                                        <Icon name="diamond" color="#c5c5c5" style={styles.hotelFeatureIcon}></Icon>
                                        <Text style={styles.hotelFeatureText}>1000 pts</Text>
                                    </View>
                                </View>
                            </View>
                            <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={{ fontFamily: fnt1, fontSize: 12, lineHeight: 12, color: '#000', fontWeight: 'bold', marginBottom: 5 }}>FEB</Text>
                                <Text style={{ fontFamily: fnt1, fontSize: 28, lineHeight: 28, color: '#000', fontWeight: 'bold', marginBottom: 5 }}>07</Text>
                                <Text style={{ fontFamily: fnt1, fontSize: 12, lineHeight: 12, color: '#000', fontWeight: 'bold', marginBottom: 5 }}>Tuesday</Text>
                            </View>
                        </View>
                        <View style={styles.textBlock}>
                            <Text style={styles.blockTitle}>A Note From The Restaurant</Text>
                            <Text style={styles.blockText}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</Text>
                            <Text style={styles.blockText}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</Text>
                        </View>
                        <View style={styles.textBlock}>
                            <Text style={styles.blockTitle}>Offers</Text>
                            <View style={{width:150,marginTop:-25}}>
                                <Dropdown data={data} rippleOpacity={0}   />
                            </View>
                        </View>
                        <View style={styles.textBlock}>
                            <Text style={styles.blockTitle}>What's The Occations?</Text>
                            <View style={styles.timingWrapper}>
                                <View style={styles.timingToggleWrapper}>
                                    <TouchableOpacity style={styles.timingToggle}>
                                        <Text style={styles.timingText}>Birthday</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={styles.timingToggleWrapper}>
                                    <TouchableOpacity style={styles.timingToggle}>
                                        <Text style={styles.timingText}>Date</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={styles.timingToggleWrapper}>
                                    <TouchableOpacity style={styles.timingToggle}>
                                        <Text style={styles.timingText}>Anniversary</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={styles.timingToggleWrapper}>
                                    <TouchableOpacity style={styles.timingToggle}>
                                        <Text style={styles.timingText}>Business Meeting</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                        <View style={[styles.textBlock, { width: '100%' }]}>
                            <TextField label={I18n.t('bookingSpecialRequestText')} value={email}
                                onChangeText={(email) => this.setState({ email })}
                                tintColor={inputColor} containerStyle={{ width: '100%' }} />
                            <Text>Not all request can be accommodate</Text>
                            <TextField label={I18n.t('bookingMobileText')} value={mobileno}
                                onChangeText={(mobileno) => this.setState({ mobileno })}
                                tintColor={inputColor} containerStyle={{ width: '100%' }} />
                        </View>
                        <View style={styles.textBlock}>
                            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
                                <Text style={styles.blockTitle}>Allow Text Updates</Text>
                                <View style={{ margin: -10 }} >
                                    <SwitchElement toggleSwitch1={this.toggleSwitch1} switch1Value={this.state.switch1Value} />
                                </View>
                            </View>
                            <Text style={[styles.blockText, { marginTop: 10 }]}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis. </Text>
                            <TouchableOpacity>
                                <Text style={styles.slideViewMore}>Learn More</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.textBlock}>
                            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
                                <Text style={styles.blockTitle}>Receive Restaurant Emails</Text>
                                <View style={{ margin: -10 }} >
                                    <SwitchElement toggleSwitch1={this.toggleSwitch1} switch1Value={this.state.switch1Value} />
                                </View>
                            </View>
                            <Text style={[styles.blockText, { marginTop: 10 }]}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis. </Text>
                            {/* <TouchableOpacity>
                                <Text style={styles.slideViewMore}>View More</Text>
                            </TouchableOpacity> */}
                        </View>
                        <View style={styles.textBlock}>
                            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
                                <Text style={styles.blockTitle}>Receive BookaResto Emails</Text>
                                <View style={{ margin: -10 }} >
                                    <SwitchElement toggleSwitch1={this.toggleSwitch1} switch1Value={this.state.switch1Value} />
                                </View>
                            </View>
                            <Text style={[styles.blockText, { marginTop: 10 }]}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis. </Text>
                            <TouchableOpacity>
                                <Text style={styles.slideViewMore}>View More</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={[reusableStyles.termAndConditionContainer]} >
                            <Text style={reusableStyles.termAndCondition}>
                                {I18n.t('termsandconditionsText')}<Text style={reusableStyles.redLinkSmall} onPress={() => { Linking.openURL('http://www.example.com/') }} > {I18n.t('termsofuserandconditionsText')} </Text>
                                {I18n.t('termsandconditionsandText')} <Text style={reusableStyles.redLinkSmall} onPress={() => { Linking.openURL('http://www.example.com/') }} >{I18n.t('privacyPolicyText')}</Text>
                            </Text>
                        </View>
                    </View>
                </ScrollView>
                <TouchableOpacity style={[reusableStyles.themeBtn,{borderRadius:0,marginTop:10}]} onPress={() => this.onPressContinueButton()}>
                    <Text style={reusableStyles.themeBtnTxt} >
                        {I18n.t('ctaBtn')}
                    </Text>
                </TouchableOpacity>
            </View>
        )
    }
}