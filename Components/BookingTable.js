import React, { Component } from 'react';
import { Text, View, Image, Linking, ScrollView, TouchableOpacity, SafeAreaView, StyleSheet, Dimensions, StatusBar } from 'react-native';
import { reusableStyles } from '../Helpers/CommonStyles.js';
import { TextField } from 'react-native-material-textfield';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import iconmoonConfig from '../selection.json';
const Icon = createIconSetFromIcoMoon(iconmoonConfig);
import I18n from '../Helpers/i18n';
import { getLanguages } from 'react-native-i18n';
import ToastUtils from "../Helpers/ToastUtils";
import constants from "../Helpers/AppConstants";
import RequestHelper from '../Helpers/RequestHelper.js'
import NetworkHelper from '../Helpers/NetworkHelper.js'
import BottomNav from './BottomNav/BottomNav';
import { TabView, TabBar, SceneMap, type NavigationState, } from 'react-native-tab-view';

var themeRed = "#f01616",
    themeGrey = "#a3a3a3",
    themelightGrey = "#595959",
    fnt1 = 'OpenSans-Regular',
    fnt2 = 'OpenSans-Bold';
fnt3 = 'OpenSans-SemiBold';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
    mainWrapper: {
        flex: 1,
        justifyContent: 'flex-start'
    },
    // header: {
    //     flexDirection: 'row',
    //     alignItems: 'center',
    //     backgroundColor: themeRed,
    //     paddingHorizontal: 20,
    //     paddingVertical:30,
    // },
    header: {
        backgroundColor: themeRed,
        height:55,
        alignItems:'flex-start',
        justifyContent:'center',
        paddingHorizontal:20,
    },
    headerTitleWrapper: {
        flex:1,
        width:'100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent:'flex-start',
    },
    backBtn: {
        width: 20,
        height: 20,
        marginTop: 10,
        marginRight: 10
    },
    backBtnIcon: {
        fontSize: 18,
        lineHeight:18,
        color: '#fff',
        marginTop:-2
    },
    contentWrapper: {
        backgroundColor: '#fff9f7',
        paddingBottom: 50,
        flex: 1,
        justifyContent: 'flex-start'
    },
    currentLocationBtn: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#ffffff',
        borderBottomWidth: 1,
        borderBottomColor: '#f4f4f4',
        paddingHorizontal: 15,
        paddingVertical: 10,
    },
    currentLocationBtnIcon: {
        color: themeRed,
        width: 20,
        height: 20,
        marginRight: 10,
        marginTop: 8
    },
    currentLocationBtnText: {
        fontFamily: fnt2,
        color: themeRed,
        textTransform: 'uppercase',
        fontSize: 10
    },
    headerTitle: {
        fontFamily: fnt2,
        fontSize: 16,
        fontWeight: 'bold',
        color: '#fff',
        marginTop:2
    },
    container: {
        marginTop: StatusBar.currentHeight
    },
    tabbar: {
        backgroundColor: '#fff',
    },
    tab: {
        width: 120,
    },
    indicator: {
        backgroundColor: themeRed,
    },
    label: {
        fontWeight: '400',
        color: themeRed
    },
    listItemContainer: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#fff9f8',
        padding: 15,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.34,
        shadowRadius: 6.27,
        elevation: 10,
    },
    listItem: {
        width: '100%',
        height: 190,
        borderRadius: 15,
        backgroundColor: '#fff',
        padding: 15,
        borderBottomColor: '#ddd',
        borderBottomWidth: 1,
        marginBottom: 15,
    },
    hotelImage: {
        width: 77,
        height: 78
    },
    hotelDetails: {
        marginLeft: 15,
        width: '70%'
    },
    statusLine: {
        fontSize: 14,
        lineHeight: 14,
        fontFamily: fnt3,
        marginTop: 3
    },
    statusGreen: {
        color: '#7fc41d'
    },
    hotelName: {
        fontSize: 14,
        lineHeight: 14,
        fontFamily: fnt3,
        color: '#000',
        marginTop: 10
    },
    hotelAddress: {
        width: '100%',
        fontSize: 10,
        lineHeight: 13,
        fontFamily: fnt1,
        color: '#000',
        marginTop: 7,
    },
    bookingDetails: {
        flex: 1,
        flexDirection: 'row'
    },
    bookingDetailsRow: {
        flex: 3,
        flexDirection: 'row',
        marginTop: 10,
    },
    redLink: {
        color: themeRed,
        fontSize: 12,
        lineHeight: 12,
        fontFamily: fnt2,
        textTransform:'uppercase'
    },
})

const Upcoming = () => (
    <View style={styles.listItemContainer}>
        <ScrollView style={{ height: '100%', backgroundColor: '#fff' }}>
            <View style={styles.listItem}>
                <View style={{ width: '100%', height: 80 }}>
                    <View style={{ flex: 1, flexDirection: 'row', }}>
                        <Image source={require('../assets/images/booking-image.png')}
                            style={styles.hotelImage}></Image>
                        <View style={styles.hotelDetails}>
                            <View>
                                <Text style={[styles.statusLine, styles.statusGreen]}>{I18n.t('bookingTableGreenText')}</Text>
                            </View>
                            <View>
                                <Text style={styles.hotelName}>Jumjoji - The Parsi Diner</Text>
                            </View>
                            <View>
                                <Text style={styles.hotelAddress}>
                                    As Sulimaniyah, Hanifa Valley Street, Jeddah, Riyadh 12214, Saudi Arabia
                            </Text>
                            </View>
                        </View>
                    </View>
                </View>
                <View style={{ width: '100%', height: 50 }}>
                    <View style={styles.bookingDetailsRow}>
                        <View style={{ flex: 5 }}>
                            <Text style={styles.bookingHead}>
                                {I18n.t('bookingTableDateHeadingText')}
                            </Text>
                            <Text style={styles.bookingDetails}>
                                March 07 at 9.30pm
                    </Text>
                        </View>
                        <View style={{ flex: 2 }}>
                            <Text style={styles.bookingHead}>
                                {I18n.t('bookingTableGuestHeadingText')}
                            </Text>
                            <Text style={styles.bookingDetails}>
                                02
                    </Text>
                        </View>
                        <View style={{ flex: 4 }}>
                            <Text style={styles.bookingHead}>
                                {I18n.t('bookingTableNameText')}
                            </Text>
                            <Text style={styles.bookingDetails}>
                                Hussain Shaikh
                    </Text>
                        </View>

                    </View>
                </View>
                <TouchableOpacity style={{ marginTop: 15 }}>
                    <Text style={styles.redLink}>{I18n.t('bookingTableCancelBookingText')}</Text>
                </TouchableOpacity>
            </View>
            <View style={styles.listItem}>
                <View style={{ width: '100%', height: 80 }}>
                    <View style={{ flex: 1, flexDirection: 'row', }}>
                        <Image source={require('../assets/images/booking-image.png')}
                            style={styles.hotelImage}></Image>
                        <View style={styles.hotelDetails}>
                            <View>
                                <Text style={[styles.statusLine, styles.statusGreen]}>{I18n.t('bookingTableGreenText')}</Text>
                            </View>
                            <View>
                                <Text style={styles.hotelName}>Jumjoji - The Parsi Diner</Text>
                            </View>
                            <View>
                                <Text style={styles.hotelAddress}>
                                    As Sulimaniyah, Hanifa Valley Street, Jeddah, Riyadh 12214, Saudi Arabia
                            </Text>
                            </View>
                        </View>
                    </View>
                </View>
                <View style={{ width: '100%', height: 50 }}>
                    <View style={styles.bookingDetailsRow}>
                        <View style={{ flex: 5 }}>
                            <Text style={styles.bookingHead}>
                                {I18n.t('bookingTableDateHeadingText')}
                            </Text>
                            <Text style={styles.bookingDetails}>
                                March 07 at 9.30pm
                    </Text>
                        </View>
                        <View style={{ flex: 2 }}>
                            <Text style={styles.bookingHead}>
                                {I18n.t('bookingTableGuestHeadingText')}
                            </Text>
                            <Text style={styles.bookingDetails}>
                                02
                    </Text>
                        </View>
                        <View style={{ flex: 4 }}>
                            <Text style={styles.bookingHead}>
                                {I18n.t('bookingTableNameText')}
                            </Text>
                            <Text style={styles.bookingDetails}>
                                Hussain Shaikh
                    </Text>
                        </View>

                    </View>
                </View>
                <TouchableOpacity style={{ marginTop: 15 }}>
                    <Text style={styles.redLink}>{I18n.t('bookingTableCancelBookingText')}</Text>
                </TouchableOpacity>
            </View>
            <View style={styles.listItem}>
                <View style={{ width: '100%', height: 80 }}>
                    <View style={{ flex: 1, flexDirection: 'row', }}>
                        <Image source={require('../assets/images/booking-image.png')}
                            style={styles.hotelImage}></Image>
                        <View style={styles.hotelDetails}>
                            <View>
                                <Text style={[styles.statusLine, styles.statusGreen]}>{I18n.t('bookingTableGreenText')}</Text>
                            </View>
                            <View>
                                <Text style={styles.hotelName}>Jumjoji - The Parsi Diner</Text>
                            </View>
                            <View>
                                <Text style={styles.hotelAddress}>
                                    As Sulimaniyah, Hanifa Valley Street, Jeddah, Riyadh 12214, Saudi Arabia
                            </Text>
                            </View>
                        </View>
                    </View>
                </View>
                <View style={{ width: '100%', height: 50 }}>
                    <View style={styles.bookingDetailsRow}>
                        <View style={{ flex: 5 }}>
                            <Text style={styles.bookingHead}>
                                {I18n.t('bookingTableDateHeadingText')}
                            </Text>
                            <Text style={styles.bookingDetails}>
                                March 07 at 9.30pm
                    </Text>
                        </View>
                        <View style={{ flex: 2 }}>
                            <Text style={styles.bookingHead}>
                                {I18n.t('bookingTableGuestHeadingText')}
                            </Text>
                            <Text style={styles.bookingDetails}>
                                02
                    </Text>
                        </View>
                        <View style={{ flex: 4 }}>
                            <Text style={styles.bookingHead}>
                                {I18n.t('bookingTableNameText')}
                            </Text>
                            <Text style={styles.bookingDetails}>
                                Hussain Shaikh
                    </Text>
                        </View>

                    </View>
                </View>
                <TouchableOpacity style={{ marginTop: 15 }}>
                    <Text style={styles.redLink}>{I18n.t('bookingTableCancelBookingText')}</Text>
                </TouchableOpacity>
            </View>
        </ScrollView>
    </View>
);
const History = () => (
    <View style={styles.listItemContainer}>
        <ScrollView style={{ height: '100%', backgroundColor: '#fff' }}>
            <View style={[styles.listItem, { height:190}]}>
                <View style={{ width: '100%', height: 80 }}>
                    <View style={{ flex: 1, flexDirection: 'row', }}>
                        <Image source={require('../assets/images/booking-image.png')}
                            style={styles.hotelImage}></Image>
                        <View style={styles.hotelDetails}>
                            <View>
                                <Text style={styles.hotelName}>Jumjoji - The Parsi Diner</Text>
                            </View>
                            <View>
                                <Text style={styles.hotelAddress}>
                                    As Sulimaniyah, Hanifa Valley Street, Jeddah, Riyadh 12214, Saudi Arabia
                            </Text>
                            </View>
                        </View>
                    </View>
                </View>
                <View style={{ width: '100%', height: 50 }}>
                    <View style={styles.bookingDetailsRow}>
                        <View style={{ flex: 5 }}>
                            <Text style={styles.bookingHead}>
                                {I18n.t('bookingTableDateHeadingText')}
                            </Text>
                            <Text style={styles.bookingDetails}>
                                March 07 at 9.30pm
                    </Text>
                        </View>
                        <View style={{ flex: 2 }}>
                            <Text style={styles.bookingHead}>
                                {I18n.t('bookingTableGuestHeadingText')}
                            </Text>
                            <Text style={styles.bookingDetails}>
                                02
                    </Text>
                        </View>
                        <View style={{ flex: 4 }}>
                            <Text style={styles.bookingHead}>
                                {I18n.t('bookingTableNameText')}
                            </Text>
                            <Text style={styles.bookingDetails}>
                                Hussain Shaikh
                    </Text>
                        </View>

                    </View>
                </View>
                <TouchableOpacity style={{ marginTop: 15 }}>
                    <Text style={styles.redLink}>{I18n.t('bookingTableAddReviewText')}</Text>
                </TouchableOpacity>
            </View>
            <View style={[styles.listItem, { height:190 }]}>
                <View style={{ width: '100%', height: 80 }}>
                    <View style={{ flex: 1, flexDirection: 'row', }}>
                        <Image source={require('../assets/images/booking-image.png')}
                            style={styles.hotelImage}></Image>
                        <View style={styles.hotelDetails}>
                            <View>
                                <Text style={styles.hotelName}>Jumjoji - The Parsi Diner</Text>
                            </View>
                            <View>
                                <Text style={styles.hotelAddress}>
                                    As Sulimaniyah, Hanifa Valley Street, Jeddah, Riyadh 12214, Saudi Arabia
                            </Text>
                            </View>
                        </View>
                    </View>
                </View>
                <View style={{ width: '100%', height: 50 }}>
                    <View style={styles.bookingDetailsRow}>
                        <View style={{ flex: 5 }}>
                            <Text style={styles.bookingHead}>
                                {I18n.t('bookingTableDateHeadingText')}
                            </Text>
                            <Text style={styles.bookingDetails}>
                                March 07 at 9.30pm
                    </Text>
                        </View>
                        <View style={{ flex: 2 }}>
                            <Text style={styles.bookingHead}>
                                {I18n.t('bookingTableGuestHeadingText')}
                            </Text>
                            <Text style={styles.bookingDetails}>
                                02
                    </Text>
                        </View>
                        <View style={{ flex: 4 }}>
                            <Text style={styles.bookingHead}>
                                {I18n.t('bookingTableNameText')}
                            </Text>
                            <Text style={styles.bookingDetails}>
                                Hussain Shaikh
                    </Text>
                        </View>

                    </View>
                </View>
                <TouchableOpacity style={{ marginTop: 15 }}>
                    <Text style={styles.redLink}>{I18n.t('bookingTableAddReviewText')}</Text>
                </TouchableOpacity>
            </View>
            <View style={[styles.listItem, { height:190 }]}>
                <View style={{ width: '100%', height: 80 }}>
                    <View style={{ flex: 1, flexDirection: 'row', }}>
                        <Image source={require('../assets/images/booking-image.png')}
                            style={styles.hotelImage}></Image>
                        <View style={styles.hotelDetails}>
                            <View>
                                <Text style={styles.hotelName}>Jumjoji - The Parsi Diner</Text>
                            </View>
                            <View>
                                <Text style={styles.hotelAddress}>
                                    As Sulimaniyah, Hanifa Valley Street, Jeddah, Riyadh 12214, Saudi Arabia
                            </Text>
                            </View>
                        </View>
                    </View>
                </View>
                <View style={{ width: '100%', height: 50 }}>
                    <View style={styles.bookingDetailsRow}>
                        <View style={{ flex: 5 }}>
                            <Text style={styles.bookingHead}>
                                {I18n.t('bookingTableDateHeadingText')}
                            </Text>
                            <Text style={styles.bookingDetails}>
                                March 07 at 9.30pm
                    </Text>
                        </View>
                        <View style={{ flex: 2 }}>
                            <Text style={styles.bookingHead}>
                                {I18n.t('bookingTableGuestHeadingText')}
                            </Text>
                            <Text style={styles.bookingDetails}>
                                02
                    </Text>
                        </View>
                        <View style={{ flex: 4 }}>
                            <Text style={styles.bookingHead}>
                                {I18n.t('bookingTableNameText')}
                            </Text>
                            <Text style={styles.bookingDetails}>
                                Hussain Shaikh
                    </Text>
                        </View>

                    </View>
                </View>
                <TouchableOpacity style={{ marginTop: 15 }}>
                    <Text style={styles.redLink}>{I18n.t('bookingTableAddReviewText')}</Text>
                </TouchableOpacity>
            </View>
        </ScrollView>
    </View>);

type State = NavigationState<{
    key: string,
    title: string,
}>;

export default class SearchLocation extends React.Component {
    static title = 'Scrollable top bar';
    static backgroundColor = themeRed;
    static appbarElevation = 0;

    state = {
        index: 0,
        routes: [
            { key: 'upcoming', title: I18n.t('bookingTableUpcomingText') },
            { key: 'history', title: I18n.t('bookingTableHistoryText') }
        ],
    };

    _handleIndexChange = index =>
        this.setState({
            index,
        });

    _renderTabBar = props => (
        <TabBar
            {...props}
            scrollEnabled
            indicatorStyle={styles.indicator}
            style={styles.tabbar}
            tabStyle={styles.tab}
            labelStyle={styles.label}
        />
    );

    _renderScene = SceneMap({
        upcoming: Upcoming,
        history: History
    });

    componentWillMount() {
        this.IS_RTL = this.props.navigation.getScreenProps().IS_RTL;
    }

    render() {
        return (
            <View style={{ ...styles.mainWrapper }}>
                <View style={styles.header}>
                    <View style={styles.headerTitleWrapper}>
                        <TouchableOpacity style={styles.backBtn} onPress={() => this.props.navigation.goBack()}>
                            <Icon name="arrow-left" color="#ffffff" style={styles.backBtnIcon}></Icon>
                        </TouchableOpacity>
                        <Text style={styles.headerTitle}>{I18n.t('bookingTableHeading')}</Text>
                    </View>
                </View>
                <View style={styles.contentWrapper}>
                    <TabView
                        style={this.props.style}
                        navigationState={this.state}
                        renderScene={this._renderScene}
                        renderTabBar={this._renderTabBar}
                        onIndexChange={this._handleIndexChange}
                    />
                </View>
                <BottomNav navigation={this.props.navigation} viewType={1}></BottomNav>
            </View>
        )
    }
}