import React, { Component } from 'react';
import { Text, View, StyleSheet, Platform, Image, Linking, ScrollView, ImageBackground, TouchableOpacity, SafeAreaView, Dimensions,AsyncStorage } from 'react-native';
import { reusableStyles } from '../../Helpers/CommonStyles.js';
import { TextField } from 'react-native-material-textfield';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import iconmoonConfig from '../../selection.json';
const Icon = createIconSetFromIcoMoon(iconmoonConfig);
import I18n from '../../Helpers/i18n';
import { getLanguages } from 'react-native-i18n';
import ToastUtils from "../../Helpers/ToastUtils";
import constants from "../../Helpers/AppConstants";
import RequestHelper from '../../Helpers/RequestHelper.js';
import NetworkHelper from '../../Helpers/NetworkHelper.js';

var themeRed = "#f01616",
    themeGrey = "#a3a3a3",
    themelightGrey = "#595959",
    fnt1 = 'OpenSans-Regular',
    fnt2 = 'OpenSans-Bold';
fnt3 = 'OpenSans-SemiBold';

const styles = StyleSheet.create({
    wrapper: {
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        alignItems:'center',
        position: 'relative',
        backgroundColor: '#f8f8f8',
        // backgroundColor:'grey'
        // paddingTop:5,
        // paddingBottom:5,
        // shadowRadius: 2,
        // shadowOffset: {
        //   width: 0,
        //   height: -3,
        // },
        // shadowColor: '#000000',
        // elevation: 4,
    },
    navItem: {
        justifyContent:'center',
        textAlign: 'center',
        alignItems: 'center',
    },
    navItemActive:{
        backgroundColor: '#fa3b3d', 
        borderRadius: 30,
        marginBottom:3,
    },
    navItemText: {
        fontSize: 10,
        lineHeight:10,
        fontFamily: fnt1,
        color: themeRed,
    },
    navItemTextActive:{
        fontWeight:'bold',
    },
    navItemIcon: {
        color: themeRed,
        padding:7,
        fontSize:18,
        lineHeight:18,
    },
    navItemIconActive:{
        color: '#fff',
        padding:10,
    },
    searchBtn: {
        position: 'relative',
        backgroundColor: '#e8e8e8',
        borderRadius:50,
        top: -40,
        borderWidth:10,
        borderColor: '#f8f8f8',
        marginBottom: Platform.OS == 'android' ? -20 : 0,
    },
    searchBtnActive: {
        backgroundColor:themeRed,
        borderRadius:50,
    },
    searchBtnIcon: {
        fontSize:38,
        lineHeight:38,
        color: themeRed,
        padding: 10,
    },
    searchIconActive:{
        color: '#fff'
    },
    activeBtn: {
        backgroundColor: themeRed
    },
    activeIcon: {
        color: '#fff'
    }

})

export default class BottomNav extends React.Component {
    constructor() {
        super();

        this.state = {
            viewType: 0//0-Home, 1-MyBookings, 2-Search, 3-Notification, 4-Profile
        }
    }

    goToHome() {
        if (this.props.viewType != 0){
            this.props.navigation.navigate('Home');
        }
    }

    goToBookingTable() {
        if (this.props.viewType != 1){
            AsyncStorage.getItem("userId").then((value) => {
                if(value)
                {
                    this.props.navigation.navigate('BookingTable');
                }
                else
                {
                    this.props.navigation.navigate('Login');
                }
            }).done();
        }
    }

    goToSearchLanding() {
        if (this.props.viewType != 2){
            this.props.navigation.navigate('SearchLanding');
        }
    }

    goToNotifications() {
        if (this.props.viewType != 3){
            this.props.navigation.navigate('Notifications');
        }
    }

    goToProfile() {
        if (this.props.viewType != 4){
            AsyncStorage.getItem("userId").then((value) => {
            console.log("User Id ", value);
            if(value)
            {
                this.props.navigation.navigate('Favourites');
            }
            else
            {
                this.props.navigation.navigate('Login');
            }
            }).done();
        }
    }


    render() {

        const keyboardVisible = this.props.navigation.getScreenProps().keyboardDidShow;

        return (
            <View style={{}}>
                {keyboardVisible ? null :
                    <View style={styles.wrapper}>
                        <TouchableOpacity onPress={() => this.goToHome()}>
                            <View style={styles.navItem}>
                                <View style={[this.props.viewType == 0 ? styles.navItemActive : null]}>
                                    <Icon name="home" color="#333333" style={[this.props.viewType == 0 ? [styles.navItemIcon, styles.navItemIconActive] : styles.navItemIcon]}></Icon>
                                </View>
                                <Text style={[this.props.viewType == 0 ? [styles.navItemText, styles.navItemTextActive] : styles.navItemText]}>HOME</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.goToBookingTable()}>
                            <View style={styles.navItem}>
                                <View style={[this.props.viewType == 1 ? styles.navItemActive : null]}>
                                    <Icon name="bookings" color="#333333" style={[this.props.viewType == 1 ? [styles.navItemIcon, styles.navItemIconActive] : styles.navItemIcon]}></Icon>
                                </View>
                                <Text style={[this.props.viewType == 1 ? [styles.navItemText, styles.navItemTextActive] : styles.navItemText]}>My BOOKING</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.goToSearchLanding()}>
                            <View style={[this.props.viewType == 2 ? [styles.navItem, styles.navItemActive, styles.searchBtn, styles.searchBtnActive] : [styles.navItem, styles.searchBtn]]}>
                            {/* ...styles.activeBtn */}
                                <Icon name="Search" color="#333333" style={[this.props.viewType == 2 ? [styles.navItemIcon, styles.navItemIconActive, styles.searchBtnIcon, styles.searchIconActive] : [styles.navItemIcon, styles.searchBtnIcon]]}></Icon>
                                {/* ...styles.activeIcon  */}
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.goToNotifications()}>
                            <View style={styles.navItem}>
                                <View style={[this.props.viewType == 3 ? styles.navItemActive : null]}>
                                    <Icon name="notification" color="#333333" style={[this.props.viewType == 3 ? [styles.navItemIcon, styles.navItemIconActive] : styles.navItemIcon]}></Icon>
                                </View>
                                <Text style={[this.props.viewType == 3 ? [styles.navItemText, styles.navItemTextActive] : styles.navItemText]}>NOTIFICATION</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.goToProfile()}>
                            <View style={styles.navItem}>
                                <View style={[this.props.viewType == 4 ? styles.navItemActive : null]}>
                                    <Icon name="user-menu" color="#333333" style={[this.props.viewType == 4 ? [styles.navItemIcon, styles.navItemIconActive] : styles.navItemIcon]}></Icon>
                                </View>
                                <Text style={[this.props.viewType == 4 ? [styles.navItemText, styles.navItemTextActive] : styles.navItemText]}>PROFILE</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                }
            </View>
        )
    }
}