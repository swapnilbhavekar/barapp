import React, { Component } from 'react';
import { Text, View, Image, Linking, ScrollView, TouchableOpacity, SafeAreaView, StyleSheet } from 'react-native';
import { reusableStyles } from '../Helpers/CommonStyles.js';
import { TextField } from 'react-native-material-textfield';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import iconmoonConfig from '../selection.json';
const Icon = createIconSetFromIcoMoon(iconmoonConfig);
import I18n from '../Helpers/i18n';
import { getLanguages } from 'react-native-i18n';
import ToastUtils from "../Helpers/ToastUtils";
import constants from "../Helpers/AppConstants";
import RequestHelper from '../Helpers/RequestHelper.js'
import NetworkHelper from '../Helpers/NetworkHelper.js'
import Logo from '../assets/images/book-a-resto-logo-white.png';
import Slide from '../assets/images/slide1.jpg';
import BottomNav from './BottomNav/BottomNav';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import Utils from "../Helpers/Utils.js";
import RestaurantByType from "../Models/RestaurantByType";
import Cuisines from "../Models/Cuisines";
import Areas from "../Models/Areas";
import Spinner from 'react-native-loading-spinner-overlay';




var themeRed = "#f01616",
    themeGrey = "#a3a3a3",
    themelightGrey = "#595959",
    fnt1 = 'OpenSans-Regular',
    fnt2 = 'OpenSans-Bold';
fnt3 = 'OpenSans-SemiBold';

const IS_RTL = false;

const styles = StyleSheet.create({
    mainWrapper: {
        flex:1,
        justifyContent: 'flex-start',
        backgroundColor: '#ffefeb',
    },
    header: {
        backgroundColor: themeRed,
        height:55,
        alignItems:'flex-start',
        justifyContent:'center',
        paddingHorizontal:20
    },
    headerTitleWrapper: {
        width:'100%',
        flexDirection: 'row',
        alignItems: 'center',
    },
    headerTitle: {
        fontFamily: fnt2,
        fontSize: 16,
        lineHeight:16,
        fontWeight: 'bold',
        color: '#fff'
    },
    contentWrapper: {
        paddingTop:20,
        paddingHorizontal: 20,
        paddingBottom:20,
        flex: 1,
        justifyContent: 'flex-start'
    },
    scrollWrapper: {
        backgroundColor:'#ffffff',
        flexDirection:'row',
        justifyContent:'space-evenly',
        flexWrap: 'wrap',
        flex: 1,
        paddingHorizontal:20,
        paddingVertical: 30,
        borderRadius: 5
    },
    slideWrapper:{
        marginBottom:30
    },
    slideItem:{
        width:86,
        height:104,
        marginRight:0,
        borderRadius: 5,
    },
    slideCatTitle:{
        fontFamily: fnt2,
        fontSize: 10,
        marginTop: 10,
        textAlign : IS_RTL ? 'right' : 'left'
    },
    slideCatCount:{
        fontFamily: fnt1,
        fontSize: 10,
        textAlign : IS_RTL ? 'right' : 'left'
    },
    backIcon: {
        fontSize:18,
        lineHeight:18
    },
})

export default class BrowseCuisines extends React.Component {
    state = {
        isRTL: false,
        titleText:'',
        showLoading : true,
        data : []
    }

    constructor(props)
    {
        super(props);
        currentComponentBrowseCuisines = this;
    }

    componentWillMount()
    {
        Utils.logData("props === "+JSON.stringify(this.props));
        var restaurantByType = this.props.navigation.state.params.restaurantByType
        var searchResult = this.props.navigation.state.params.searchResult
        if (RestaurantByType.CUISINES ==restaurantByType) {
            this.setState({
                titleText : 'Browse by Cuisines'
            })
            this.getBrowseByCuisines(searchResult.latitude,searchResult.longitude)
        }
        else if (RestaurantByType.AREAS ==restaurantByType) {
            this.setState({
                titleText : 'Browse by Areas'
            })
            this.getBrowseByAreas(searchResult.latitude,searchResult.longitude)
        }
    }

    goToRestaurantListing(item)
    {
        var searchResult = this.props.navigation.state.params.searchResult
        var otherDetails = {
            "guest_count": "2", "input_search": "","key":"default_search","section":"Single","slot_date":"2019-06-08"
        }
        this.props.navigation.navigate('RestaurantListing',{searchResult : searchResult ,selectedCuisineOrArea : item , otherDetails : otherDetails})
    }

    getBrowseByCuisines(latitude,longitude) 
    {
        var reqHelper = new RequestHelper()
        var requestUrl = reqHelper.getBrowserByCuisines()
        var networkHelper = new NetworkHelper();
        var requestParams = {"lang_code": "en",'lattitude':latitude,'longitude':longitude};
        networkHelper.setUrl(requestUrl);
        networkHelper.setMethod(constants.methodPost);
        networkHelper.setData(JSON.stringify(requestParams));

        networkHelper.execute(function (responseData) 
        {
            Utils.logData("BrowseCuisines getBrowseByCuisines Response "+JSON.stringify(responseData)); 
            if (responseData.IsSuccess == true){
                var cuisinesArray = []
                var cuisines = responseData.Data
                for (var i = 0;i <cuisines.length; i++) {
                    var id = cuisines[i].cuisine_id
                    var cuisine_name = cuisines[i].cuisine_name
                    var image = cuisines[i].image
                    var restaurant_count = cuisines[i].restaurant_count
                    var lattitude = cuisines[i].lattitude
                    var longitude = cuisines[i].longitude
                    var cuisine = new Cuisines(id,cuisine_name,image,lattitude,longitude,restaurant_count)
                    cuisinesArray.push(cuisine)
                }

                currentComponentBrowseCuisines.setState({ 
                    showLoading : false,
                    data:cuisinesArray
                })

            } 
            else{
                ToastUtils.showErrorToast(responseData.Message);
                currentComponentBrowseCuisines.setState({ 
                    showLoading:false
                })
            }
        }, function (errorMessage, statusCode) 
        {
            ToastUtils.showErrorToast(constants.somethingwent_wrong_msg);
            currentComponentBrowseCuisines.setState({ 
                    showLoading:false
                })
        }, function()
            {
                ToastUtils.showErrorToast(constants.no_network_msg);
                currentComponentBrowseCuisines.setState({ 
                    showLoading:false
                })
            });
    }

    getBrowseByAreas(latitude,longitude) 
    {
        var reqHelper = new RequestHelper()
        var requestUrl = reqHelper.getBrowserByAreas()
        var networkHelper = new NetworkHelper();
        var requestParams = {"lang_code": "en",'lattitude':latitude,'longitude':longitude};
        networkHelper.setUrl(requestUrl);
        networkHelper.setMethod(constants.methodPost);
        networkHelper.setData(JSON.stringify(requestParams));

        networkHelper.execute(function (responseData) 
        {
            Utils.logData("BrowseCuisines getBrowseByAreas Response "+JSON.stringify(responseData)); 
            if (responseData.IsSuccess == true){
                Utils.logData("BrowseCuisines getBrowseByAreas Response ")
                var areasArray = []
                var areas = responseData.Data
                for (var j = 0;j <areas.length; j++) {                    
                    var id = areas[j].area_id                   
                    var area_name = areas[j].area_name
                    var area_image = areas[j].image
                    var restaurant_count = areas[j].restaurant_count
                    var latitude = areas[j].lattitude 
                    var longitude = areas[j].longitude
                    var distance_in_km = areas[j].distance_in_km
                    var area = new Areas(id,area_name,area_image,latitude,longitude,distance_in_km,restaurant_count)
                    areasArray.push(area)
                 }
                 currentComponentBrowseCuisines.setState({ 
                    showLoading : false,
                    data:areasArray
                })

            } 
            else{
                ToastUtils.showErrorToast(responseData.Message);
                currentComponentBrowseCuisines.setState({ 
                    showLoading:false
                })
            }
        }, function (errorMessage, statusCode) 
        {
            ToastUtils.showErrorToast(constants.somethingwent_wrong_msg);
            currentComponentBrowseCuisines.setState({ 
                    showLoading:false
                })
        }, function()
            {
                ToastUtils.showErrorToast(constants.no_network_msg);
                currentComponentBrowseCuisines.setState({ 
                    showLoading:false
                })
            });
    }

    CardItem = (item) => {
        Utils.logData("itemmmmm "+JSON.stringify(item))
        item = item.item
        return (<TouchableOpacity onPress={() => this.goToRestaurantListing(item) } style={styles.slideWrapper}>
            <Image style={styles.slideItem} source={{uri : item.image}}></Image>
            <Text style={styles.slideCatTitle}>{item.name}</Text>
            <Text style={styles.slideCatCount}>{item.restaurant_count + ' Restaurants'}</Text>
        </TouchableOpacity>)
    }

    render() {
        const IS_RTL = this.state.isRTL;
        var spinner = null
        if (this.state.showLoading) {
            spinner = <Spinner visible={this.state.showLoading} 
            //textContent={"Loading..."} 
            textStyle={reusableStyles.loaderStyle} />
        } 
        return (
            <View style={{...styles.mainWrapper,direction:IS_RTL?'rtl':'ltr'}}>
            {spinner}
                <View style={styles.header}>
                    <View style={styles.headerTitleWrapper}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={{paddingVertical:10}}>
                            <Icon name="arrow-left" color="#ffffff" 
                            style={[styles.backIcon,{marginRight: 10,marginTop:-3 }]}></Icon>
                        </TouchableOpacity>
                        <Text style={styles.headerTitle}>{this.state.titleText}</Text>
                    </View>
                </View>
                <ScrollView>
                    <View style={styles.contentWrapper}>
                        <View style={styles.scrollWrapper}>
                            {this.state.data.map((item, index) => {
                                Utils.logData('datat '+JSON.stringify(item))
                                return (
                                    <this.CardItem item ={item}></this.CardItem>
                                )
                            })}
                        </View>
                    </View>
                </ScrollView>
                <BottomNav navigation = {this.props.navigation}></BottomNav>
            </View>
        )
    }
}