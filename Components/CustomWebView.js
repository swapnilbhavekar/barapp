import React from 'react';
import {WebView} from 'react-native';
import Utils from '../Helpers/Utils.js'

export default class CustomWebView extends React.Component 
{
	constructor(props) 
	{
		super(props);
	}

	componentWillMount()
	{
  		Utils.logData('url to load in custom web view '+JSON.stringify(this.props))
  	}

	onLoad() {
  		Utils.logData('onLoad custom web view')
	}
	onError() {
  		Utils.logData('onError custom web view')
	}
	onLoadStart() {
  		Utils.logData('onLoadStart custom web view')
	}

	render() 
	{
		var url = this.props.navigation.state.params.url
		Utils.logData('loading url custom web view'+url)
		return(
			<WebView
			source={{uri: url}}
        	onLoad={this.onLoad}
      		onError={this.onError}
      		onLoadStart={this.onLoadStart}
        	/>
        	);
	}
}