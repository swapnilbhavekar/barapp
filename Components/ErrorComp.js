import React, { Component } from 'react';
import { Text, View, Image, Linking, StyleSheet, ImageBackground, TouchableOpacity, SafeAreaView, Dimensions } from 'react-native';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import iconmoonConfig from '../selection.json';
const Icon = createIconSetFromIcoMoon(iconmoonConfig);
import errorIcon from '../assets/images/errorIcon.png';
import Utils from "../Helpers/Utils";


var themeRed = "#f01616",
themeGrey="#a3a3a3",
themelightGrey="#595959",
fnt1='OpenSans-Regular',
fnt2='OpenSans-Bold';
fnt3='OpenSans-SemiBold';

const styles = StyleSheet.create({
    wrapper:{
        backgroundColor:'#ffffff',
        paddingVertical:50,
        paddingHorizontal: 30,
        alignItems: 'center',
        marginBottom: 30,
        borderRadius: 5,
    },
    icon:{
        marginBottom:20,
        marginTop: 20,
    },
    text:{
        textAlign:'center',
        fontFamily: fnt1,
        marginBottom:10
    },
    btnWrapper:{
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center'
    },
    btnIcon:{
        width:12,
        height:12,
        color:themeRed,
        marginRight: 7,
    },
    btnText:{
        fontFamily: fnt2,
        color:themeRed,
        fontSize: 12,
        textTransform:'uppercase'
    }
})

export default class ErrorComp extends React.Component {
    constructor() {
        super();

        this.state = {
            infoText : ''
        }
        this.onLocationDetailsAvailable = this.onLocationDetailsAvailable.bind(this)
    }

    componentWillMount()
    {
        if (this.props.isGPSEnabled == false) {
            this.setState({
                infoText : 'We can not detect your location. Please Enable your GPS to help us Detect.'
            })

        }
        else if (this.props.isRestaurantsAvailble == false) {
            this.setState({
                infoText :'We do not have any restaurant near Mumbai place. Please try another loacation.'
            })
        }
    }

    goToSearchLocation() {
        this.props.navigation.navigate('SearchLocation',{ onLocationDetailsAvailable : this.onLocationDetailsAvailable});
    }

    onLocationDetailsAvailable(searchResult)
    {
        Utils.logData('onLocationDetailsAvailable error comp '+JSON.stringify(searchResult))
        Utils.logData('error comp props '+JSON.stringify(this.props))
        this.props.onLocationDetailsAvailable(searchResult)
    }

    render()
    {
        return(
            <View style={styles.wrapper}>
    <Image style={styles.icon} source={errorIcon} ></Image>
    {/* <Icon name="location-not-found" color="#ff0000" style={styles.btnIcon}></Icon> */}
    <Text style={styles.text}>{this.state.infoText}</Text>
    <TouchableOpacity  style={styles.btnWrapper} onPress={() => this.goToSearchLocation()}>
        <Icon name="map-pointer" color="#ffffff" style={styles.btnIcon}></Icon>
        <Text style={styles.btnText}>Select Your Location</Text>
    </TouchableOpacity>
</View>
            )
    }
}

// const ErrorComp = props => <View style={styles.wrapper}>
//     <Image style={styles.icon} source={errorIcon} ></Image>
//     {/* <Icon name="location-not-found" color="#ff0000" style={styles.btnIcon}></Icon> */}
//     <Text style={styles.text}>We do not have any restaurant near Mumbai place. Please try another loacation.</Text>
//     <TouchableOpacity  style={styles.btnWrapper}>
//         <Icon name="map-pointer" color="#ffffff" style={styles.btnIcon}></Icon>
//         <Text style={styles.btnText}>Select Your Location</Text>
//     </TouchableOpacity>
// </View>

// export default ErrorComp;