import React, { Component } from 'react';
import { Text, View, Image, Linking, ScrollView, TouchableOpacity, SafeAreaView, StyleSheet, Dimensions, ImageBackground,ListView,AsyncStorage } from 'react-native';
import { reusableStyles } from '../Helpers/CommonStyles.js';
import { TextField } from 'react-native-material-textfield';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import iconmoonConfig from '../selection.json';
const Icon = createIconSetFromIcoMoon(iconmoonConfig);
import I18n from '../Helpers/i18n';
import { getLanguages } from 'react-native-i18n';
import ToastUtils from "../Helpers/ToastUtils";
import constants from "../Helpers/AppConstants";
import RequestHelper from '../Helpers/RequestHelper.js'
import NetworkHelper from '../Helpers/NetworkHelper.js'
import BottomNav from './BottomNav/BottomNav';
import Utils from "../Helpers/Utils.js";
import Spinner from 'react-native-loading-spinner-overlay';
import Restaurant from "../Models/Restaurant";
import CommonNetworkCalls from "../Helpers/CommonNetworkCalls.js";



var themeRed = "#f01616",
    themeGrey = "#a3a3a3",
    themelightGrey = "#595959",
    lightRed = "#fdc9c9",
    fnt1 = 'OpenSans-Regular',
    fnt2 = 'OpenSans-Bold';
fnt3 = 'OpenSans-SemiBold';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
    mainWrapper: {
        flex: 1,
        justifyContent: 'flex-start',
    },
    header: {
        backgroundColor: themeRed,
        height:55,
        alignItems:'flex-start',
        justifyContent:'center',
        paddingHorizontal:20
    },
    headerTitleWrapper: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    headerTitle: {
        fontFamily: fnt2,
        fontSize: 16,
        fontWeight: 'bold',
        color: '#fff'
    },
    contentWrapper: {
        backgroundColor: '#ffefeb',
        paddingTop: 20,
        paddingHorizontal: 20,
        paddingBottom: 50,
        flex: 1,
        justifyContent: 'flex-start'
    },
    scrollWrapper: {
        backgroundColor: '#ffffff',
        paddingHorizontal: 20,
        paddingTop: 20,
        paddingBottom: 60,
        borderRadius: 5,
        // height: height - 100
    },

})

export default class Favourites extends React.Component {

    constructor(props)
    {
        super(props);
        currentComponentFavouritesListing = this;
        ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            favouritesData :ds
        }
    }


    componentWillMount() {
        this.IS_RTL = this.props.navigation.getScreenProps().IS_RTL;
        AsyncStorage.getItem("userId").then((userId) => {
            console.log("User Id ", userId);
            if(userId)
            {
               this.getFavouriteRestaurants(userId)
            }
            }).done();
        
    }

    goToSearchTable() {
        this.props.navigation.navigate('SearchTable');
    }

    goToSearchRestaurants() {
        this.props.navigation.navigate('SearchRestaurants');
    }

    goToSearchLocation() {
        this.props.navigation.navigate('SearchLocation');
    }

    goToRestaurantDetails(restaurant){
        this.props.navigation.navigate('RestaurantDetails',{'restaurant':restaurant})
    }

    submitPressed() {

    }

    getFavouriteRestaurants(userId)
{
        currentComponentFavouritesListing.setState({ 
                    showLoading:true
                })
        var reqHelper = new RequestHelper()
        var requestUrl = reqHelper.getFavourites()
        var networkHelper = new NetworkHelper();
        var requestParams = {"lattitude":"19.172421","longitude":"72.796982",
        "lang_code":"en","guest_count":"2","section":"Single",
    "slot_date":"2019-06-08",
"customer_id":userId}

        networkHelper.setUrl(requestUrl);
        networkHelper.setMethod(constants.methodPost);
        networkHelper.setData(JSON.stringify(requestParams));

        networkHelper.execute(function (responseData) 
        {
            Utils.logData("getRestaurants Response"+JSON.stringify(responseData)); 
            if (responseData.IsSuccess == true){
                var favouritesArray = []
                var restaurants = responseData.Data
                for (var i = 0;i <restaurants.length; i++) {

                    var id = restaurants[i].restaurant_id;
                    var lattitude = restaurants[i].lattitude;
                    var longitude = restaurants[i].longitude;
                    var distance_in_km = restaurants[i].distance_in_km;
                    var cover_image = restaurants[i].cover_image;
                    var name = restaurants[i].restaurant_name;
                    var cuisines = restaurants[i].cuisines;
                    var city_name = restaurants[i].city_name;
                    var avg_cost_per_person = restaurants[i].avg_cost_per_person;
                    var turn_time = restaurants[i].turn_time;
                    var booked_today_count = restaurants[i].booked_today_count;

                    var restaurant = new Restaurant(id,lattitude,longitude,distance_in_km,
                                                    cover_image,name,cuisines,city_name,avg_cost_per_person,turn_time,booked_today_count)
                    favouritesArray.push(restaurant)
                }
                 Utils.logData("favouritesArray  "+JSON.stringify(favouritesArray))
                currentComponentFavouritesListing.setState({ 
                    showLoading : false,
                    favouritesData: ds.cloneWithRows(favouritesArray)
                })
            } 
            else{
                ToastUtils.showErrorToast(responseData.Message);
                currentComponentFavouritesListing.setState({
                    favouritesData: ds.cloneWithRows([]), 
                    showLoading:false
                })
            }
        }, function (errorMessage, statusCode) 
        {
            Utils.logData(errorMessage);
            ToastUtils.showErrorToast(constants.somethingwent_wrong_msg);
            currentComponentFavouritesListing.setState({ 
                    showLoading:false
                })
        }, function()
            {
                ToastUtils.showErrorToast(constants.no_network_msg);
                currentComponentFavouritesListing.setState({ 
                    showLoading:false
                })
            });
    }

    removeFromFavourites(restaurant)
    {
        Utils.logData('addRemoveToFavourites restaurantDetails'+JSON.stringify(restaurant))
        AsyncStorage.getItem("userId").then((userId) => {
            console.log("User Id ", userId);
            if(userId)
            {
               var commonNetworkCalls = new CommonNetworkCalls();
               var requestParams = {'restaurant_id' : restaurant.id,"customer_id":userId,"action":'delete'}
               commonNetworkCalls.addRemoveToFavourites(requestParams,function()
                {
                    currentComponentFavouritesListing.getFavouriteRestaurants(userId)                    
                });
            }
            }).done();
    }

    renderFavouritesRow(restaurant)
    {
        Utils.logData(' restaurant rowdata '+JSON.stringify(restaurant))
        return(
            <TouchableOpacity
                            onPress={() => this.goToRestaurantDetails(restaurant)} 
                            style={[reusableStyles.listItem, reusableStyles.hasPoints]}>
                            <ImageBackground style={reusableStyles.restaurantItem}
                                source={{ uri: restaurant.cover_image }} 
                                imageStyle={{ borderRadius: 5 }}>
                                <View style={{ width: 27, height: 12, marginTop: 10 }}>
                                    <ImageBackground source={require('../assets/images/ratings.png')}
                                        style={{ flex: 1, alignItems: 'center', }}>
                                        <Text style={{ color: '#fff', fontSize: 10 }}>4 *</Text>
                                    </ImageBackground>
                                </View>
                                <View style={{ flexDirection: 'row', alignSelf: 'flex-end', marginTop: 5, height: 15 }}>
                                    <TouchableOpacity onPress = {() =>this.removeFromFavourites(restaurant)}>
                                        <Icon name="favourites" color="#ffffff" ></Icon>
                                    </TouchableOpacity>
                                    <Text style={[reusableStyles.ratingText,{paddingLeft:15}]}>
                                        {Utils.roundOfValue(restaurant.distance_in_km)}km
                            </Text>
                                </View>
                            </ImageBackground>
                            <Text style={reusableStyles.restaurantHeading}>
                                {restaurant.name}
                            </Text>
                            <Text style={reusableStyles.restaurantHeading2}>
                                {restaurant.cuisines}
                            </Text>
                            <View style={reusableStyles.details}>
                                <Text style={[reusableStyles.detailsText, reusableStyles.brdrRight]}>
                                    Jeddah
                                {restaurant.city_name}
                                </Text>
                                <Text style={[reusableStyles.detailsText, reusableStyles.brdrRight]}>
                                    {restaurant.avg_cost_per_person}
                                </Text>
                                <Text style={[reusableStyles.detailsText]}>
                                    Booked {" " + restaurant.booked_today_count + " "}times day
                                </Text>
                            </View>
                            <View style={reusableStyles.btnsContainer}>
                                <TouchableOpacity style={reusableStyles.redThumbnailBtn}>
                                    <Text style={reusableStyles.redThumbnailBtnText}>06.00pm</Text>
                                    <Text style={reusableStyles.pointsLink}>+500 pts</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={reusableStyles.redThumbnailBtn}>
                                    <Text style={reusableStyles.redThumbnailBtnText}>06.30pm</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={reusableStyles.redThumbnailBtn}>
                                    <Text style={reusableStyles.redThumbnailBtnText}>07.00pm</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={reusableStyles.redThumbnailBtn}>
                                    <Text style={reusableStyles.redThumbnailBtnText}>More..</Text>
                                </TouchableOpacity>
                            </View>
                        </TouchableOpacity>
            )
    }

    render() {
        var spinner = null
        if (this.state.showLoading) {
            spinner = <Spinner visible={this.state.showLoading} 
            //textContent={"Loading..."} 
            textStyle={reusableStyles.loaderStyle} />
        }
        return (
            <View style={{ ...styles.mainWrapper }}>
            {spinner}
                <View style={styles.header}>
                    <View style={styles.headerTitleWrapper}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={{paddingVertical:10}}>
                            <Icon name="arrow-left" color="#ffffff" style={{ marginRight: 10, marginTop: -3 }}></Icon>
                        </TouchableOpacity>
                        <Text style={styles.headerTitle}>Favourites</Text>
                    </View>
                </View>
                <View style={styles.contentWrapper}>
                    <ListView
                            dataSource={currentComponentFavouritesListing.state.favouritesData}
                            renderRow={(rowData) =>currentComponentFavouritesListing.renderFavouritesRow(rowData)}
                            />
                </View>
                {/* <BottomNav navigation={this.props.navigation} viewType={2}></BottomNav> */}
            </View>
        )
    }
}