import React, { Component } from 'react';
import { Text, View, Dimensions, TouchableOpacity, Image, UIManager, SafeAreaView, Platform, LayoutAnimation, StyleSheet } from 'react-native';
import { reusableStyles } from '../Helpers/CommonStyles.js';
import { TextField } from 'react-native-material-textfield';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import iconmoonConfig from '../selection.json';
const Icon = createIconSetFromIcoMoon(iconmoonConfig);
import I18n from '../Helpers/i18n';
import { getLanguages } from 'react-native-i18n';
import ToastUtils from "../Helpers/ToastUtils";
import constants from "../Helpers/AppConstants";
import RequestHelper from '../Helpers/RequestHelper.js'
import NetworkHelper from '../Helpers/NetworkHelper.js'
import Logo from '../assets/images/book-a-resto-logo-white.png';
import Slide from '../assets/images/slide1.jpg';
import BottomNav from './BottomNav/BottomNav';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { TextInput } from 'react-native-gesture-handler';
import RangeSlider from 'rn-range-slider';
import CheckBox from 'react-native-check-box'
import Accordion from 'react-native-collapsible/Accordion';
// import AccordionExample from './AccordionExample.js';

const SECTIONS = [
    {
      title: 'Heading 1',
      content: '<View>1</View>',
    },
    {
      title: 'Heading 2',
      content: 'Data 2',
    },
  ];



var themeRed = "#f01616",
    themeGrey = "#a3a3a3",
    themelightGrey = "#595959",
    lightRed = "#fdc9c9",
    fnt1 = 'OpenSans-Regular',
    fnt2 = 'OpenSans-Bold',
    fnt3 = 'OpenSans-SemiBold';

const IS_RTL = false;

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
    mainWrapper: {
        flex: 1,
        justifyContent: 'flex-start',
        paddingHorizontal: 20,
    },
    greyLink: {
        textTransform: 'uppercase',
        fontSize: 14,
        lineHeight: 14,
        color: 14
    },
    MainContainer: {
        flex: 1,
        justifyContent: 'center',
        paddingTop: (Platform.OS === 'ios') ? 20 : 0
    },
    accordionItem:{
        width:'100%'
    },
    Panel_text: {
        fontSize: 18,
        color: '#000',
        padding: 10
    },

    Panel_Button_Text: {
        textAlign: 'center',
        color: '#fff',
        fontSize: 21
    },
    accordionHeading: {
        borderBottomColor:'#f8f8f8',
        borderBottomWidth:1,
        paddingVertical:20,
        marginBottom: 15,
        backgroundColor:'#fff',
        flex:1,
        flexDirection:'row',
        justifyContent:'space-between',
    },    
    accordionHeadingText:{
        fontSize: 12,
        color:'#000',
        fontFamily: fnt3,
    },
    accordionIcon:{
        fontSize: 12,
    },
    accordionContent:{
    }
});

export default class FilterModal extends React.Component {
    constructor (props) {
        super(props)
        this.state = {
            isChecked:true,
            activeSections: [],
        }
     }

     _renderSectionTitle = section => {
        return (
          <View style={styles.content}>
            <Text>{section.content}</Text>
          </View>
        );
      };
    
      _renderHeader = section => {
        return (
          <View style={styles.accordionHeading}>
            <Text style={styles.accordionHeadingText}>
                {section.title}
            </Text>
            <Icon name="chevron-thin-left" color="#000" style={styles.accordionIcon}></Icon>
          </View>
        );
      };
    
      _renderContent = section => {
        return (
          <View style={styles.accordionContent}>
            <Text>{section.content}</Text>
          </View>
        );
      };
    
      _updateSections = activeSections => {
        this.setState({ activeSections });
      };
     
    render() {
        return (
            <View style={styles.mainWrapper}>
                <View style={styles.header}>
                    <TouchableOpacity>
                        <Text style={styles.greyLink}>
                            clear all
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Text style={reusableStyles.redLink}>
                            Done
                        </Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.MainContainer}>
                    <View style={styles.accordionItem}>
                    <Accordion
                        sections={SECTIONS}
                        activeSections={this.state.activeSections}
                        //renderSectionTitle={this._renderSectionTitle}
                        renderHeader={this._renderHeader}
                        renderContent={this._renderContent}
                        onChange={this._updateSections}
                    />
                    </View>
                    <View style={styles.accodtionItem}>
                        <Text style={styles.accordionHeading}>
                            Sort By
                        </Text>
                        <View style={styles.accordionItem}>
                            <View style={reusableStyles.timingWrapper}>
                                <View style={reusableStyles.timingToggleWrapper}>
                                    <TouchableOpacity style={[reusableStyles.timingToggle, reusableStyles.activeTimingToggle]}>
                                        <Text style={[reusableStyles.timingText, reusableStyles.activeTimingText]}>Birthday</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={reusableStyles.timingToggleWrapper}>
                                    <TouchableOpacity style={reusableStyles.timingToggle}>
                                        <Text style={reusableStyles.timingText}>Date</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={reusableStyles.timingToggleWrapper}>
                                    <TouchableOpacity style={reusableStyles.timingToggle}>
                                        <Text style={reusableStyles.timingText}>Anniversary</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={reusableStyles.timingToggleWrapper}>
                                    <TouchableOpacity style={reusableStyles.timingToggle}>
                                        <Text style={reusableStyles.timingText}>Business Meeting</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </View>
                    <View style={styles.accodtionItem}>
                        <Text style={styles.accordionHeading}>
                            Distance (in KMS)
                        </Text>
                        <View style={styles.accordionItem}>
                            <View style={{ width: '100%', marginTop: -35, }}>
                                <RangeSlider style={{ width: '100%', }}
                                    gravity={'center'}
                                    min={5} max={60} step={5} selectionColor={themeRed} blankColor={lightRed}
                                    onValueChanged={(low, high, fromUser) => {
                                        this.setState({ rangeLow: low, rangeHigh: high })
                                    }} />
                            </View>
                        </View>
                    </View>
                    <View syle={styles.accodtionItem}>
                        <View style={{ width: '100%', marginTop: 50 }}>
                            <CheckBox
                                style={{ flex: 1, padding: 10 }}
                                onClick={() => {
                                    this.setState({
                                        isChecked: !this.state.isChecked
                                    })
                                }}
                                isChecked={this.state.isChecked}
                                leftText={"CheckBox"}
                                checkedImage={<Image source={require('../assets/images/checkbox.png')}/>}
                                    // style={this.props.theme.styles.tabBarSelectedIcon} 
                                unCheckedImage={<Image source={require('../assets/images/un-checkbox.png')}/>}
                                    // style={this.props.theme.styles.tabBarSelectedIcon} 
                            />
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}