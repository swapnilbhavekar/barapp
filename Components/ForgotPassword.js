import React, { Component } from 'react';
import { Text, View, Image, Platform, ImageBackground, TouchableOpacity, SafeAreaView,Dimensions } from 'react-native';
import { reusableStyles } from '../Helpers/CommonStyles.js'
import { TextField } from 'react-native-material-textfield';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import iconmoonConfig from '../selection.json';
const Icon = createIconSetFromIcoMoon(iconmoonConfig);
import I18n from '../Helpers/i18n';
import { getLanguages } from 'react-native-i18n';
import RequestHelper from '../Helpers/RequestHelper.js';
import NetworkHelper from '../Helpers/NetworkHelper.js';
import ToastUtils from "../Helpers/ToastUtils";
import constants from "../Helpers/AppConstants";
import Validator from '../Helpers/Validator.js';
import Spinner from 'react-native-loading-spinner-overlay';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

export default class ForgotPassword extends React.Component {
    constructor() {
        super();
        this.state = {
            mobileno: '',
            showLoading: false,
            scrollViewHeight:0

        }
        currentComponentForgotPwd = this;
    }

    onPressSendButton() {
        let errors = {};

        if (!Validator.isValidString(currentComponentForgotPwd.state.mobileno)) {
            errors["mobileno"] = constants.empty_mobile_no_error;
            //ToastUtils.showErrorToast(constants.empty_mobile_no_error);
            //return;
        }

        this.setState({ errors });

        if (Object.keys(errors).length === 0){
            this.generateOTP();
        }
    }

    generateOTP() {
        currentComponentForgotPwd.setState({
            showLoading: true,
        })
        console.log('Generate OTP Request')
        var reqHelper = new RequestHelper();
        var requestUrl = reqHelper.generateOTPRequest();
        var networkHelper = new NetworkHelper();
        var requestParams = { "mobile": this.state.mobileno, "source": "login" };
        networkHelper.setUrl(requestUrl);
        networkHelper.setMethod(constants.methodPost);
        networkHelper.setData(JSON.stringify(requestParams));

        networkHelper.execute(function (responseData) {
            currentComponentForgotPwd.setState({
                showLoading: false,
            })
            console.log("Generate OTP Response", responseData);
            if (responseData.IsSuccess == true) {
                ToastUtils.showSuccessToast(responseData.Message);
                currentComponentForgotPwd.props.navigation.navigate('VerificationCode', { 'mobileNo': currentComponentForgotPwd.state.mobileno, 'source': 'login', 'generatedOTP': responseData.OTP, 'userParams': {} });
            }
            else {
                ToastUtils.showErrorToast(responseData.Message);
            }
        }, function (errorMessage, statusCode) {
                currentComponentForgotPwd.setState({
                    showLoading: false,
                })
                console.log(errorMessage);
                ToastUtils.showErrorToast(constants.somethingwent_wrong_msg);
            }, function () {
                currentComponentForgotPwd.setState({
                    showLoading: false,
                })
                ToastUtils.showErrorToast(constants.no_network_msg);
            });
    }

    componentWillMount(){
        this.keyboardOpen = false;        
    }

    onLayout(event) {
        if(!this.keyboardOpen){
            var windowHeight = Dimensions.get('window').height;
            const {x, y, height, width} = event.nativeEvent.layout;
            svHeight = height - (Platform.OS === 'ios' ? 70 : 0);
            this.setState({scrollViewHeight : svHeight})
            console.log(height);
        }
        this.keyboardOpen = true;
    }


    render() {
        let { errors = {} } = this.state;
        let { mobileno } = this.state;
        var spinner = null
        if (this.state.showLoading) {
            spinner = <Spinner visible={this.state.showLoading} 
            //textContent={"Loading..."} 
            textStyle={reusableStyles.loaderStyle} />
        }
        return (
            <ImageBackground source={require('../assets/images/background-table.jpg')}
                style={reusableStyles.imageBackground} >
                {spinner}
                <SafeAreaView style={reusableStyles.droidSafeArea} >
                    <View style={reusableStyles.container} >
                        <View style={reusableStyles.headerBox}>
                            <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={{paddingVertical:10}}>
                                <Icon name="arrow-left" color="#f01616" style={reusableStyles.backBtnIcon}></Icon>
                            </TouchableOpacity>
                            <View style={reusableStyles.logoContainer}>
                                <Image source={require('../assets/images/book-a-resto-logo.png')}
                                    style={reusableStyles.logo}
                                />
                            </View>
                        </View>
                        <View style={reusableStyles.formBox}
                            onLayout={(event) => this.onLayout(event)} >
                            <KeyboardAwareScrollView keyboardShouldPersistTaps='always' contentContainerStyle={{ height: this.state.scrollViewHeight }}>
                                <View style={reusableStyles.scrollingBox}>
                                    <View>
                                        <View style={reusableStyles.infoBox}>
                                            <Text style={reusableStyles.infoHeading}>{I18n.t('forgotHeadingText')}</Text>
                                            <Text style={reusableStyles.infoDetails}>{I18n.t('forgotInfoText')}</Text>
                                            <Text style={reusableStyles.infoHeading}>{I18n.t('forgotOTPVerifiyText')}</Text>
                                        </View>
                                        <TextField 
                                            label={I18n.t('forgotPasswordLabel')} 
                                            value={mobileno}
                                            onChangeText={(mobileno) => this.setState({ mobileno })}
                                            tintColor={inputColor}
                                            keyboardType="number-pad"
                                            error={errors.mobileno}
                                            containerStyle={[reusableStyles.textField, reusableStyles.w100]} />
                                    </View>
                                    <View style={reusableStyles.bottomBtnContainer}>
                                        <TouchableOpacity style={reusableStyles.themeBtn}
                                            onPress={() => this.onPressSendButton()}>
                                            <Text style={reusableStyles.themeBtnTxt} >{I18n.t('forgotPasswordBtn')}</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </KeyboardAwareScrollView>
                        </View>
                    </View>
                </SafeAreaView>
            </ImageBackground>
        )
    }
}