import React, { Component } from 'react';
import { Text, View, Image, Linking, ScrollView, ImageBackground, TouchableOpacity, SafeAreaView, Dimensions, AsyncStorage, PermissionsAndroid,ToastAndroid } from 'react-native';
import { reusableStyles } from '../../Helpers/CommonStyles.js';
import { TextField } from 'react-native-material-textfield';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import iconmoonConfig from '../../selection.json';
const Icon = createIconSetFromIcoMoon(iconmoonConfig);
import I18n from '../../Helpers/i18n';
import { getLanguages } from 'react-native-i18n';
import ToastUtils from "../../Helpers/ToastUtils";
import constants from "../../Helpers/AppConstants";
import RequestHelper from '../../Helpers/RequestHelper.js'
import NetworkHelper from '../../Helpers/NetworkHelper.js'
import Logo from '../../assets/images/book-a-resto-logo-white.png';
import Slide from '../../assets/images/slide1.jpg';
import BottomNav from '../BottomNav/BottomNav';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { HomeStyles } from './HomeStyles';
import SectionCarousel from '../sectionCarousel/sectionCarousel';
import BannerCTA from '../BannerCTA/BannerCTA';
import ErrorComp from '../ErrorComp';
import Utils from "../../Helpers/Utils.js";
import Geolocation from 'react-native-geolocation-service';
import Cuisines from "../../Models/Cuisines";
import Areas from "../../Models/Areas";
import SearchResult from "../../Models/SearchResult";
import RestaurantByType from "../../Models/RestaurantByType";
import Spinner from 'react-native-loading-spinner-overlay';



export default class Home extends React.Component {
    state = {
        showLoading : false,
        availableLocation : {},
        canShowPendingOrderView : false,
        activeDotIndex: 0,
        isGPSEnabled : false,
        isRestaurantsAvailble : false,
        canRenderCuisines : false,
        canRenderAreas : false,
        restauraurantByAreas :[],
        restauraurantByCuisines : []
    }

    constructor(props)
    {
        super(props);
        currentComponentHomeScreen = this;
        this.onLocationDetailsAvailable = this.onLocationDetailsAvailable.bind(this)
    }

    _renderItem({ item, index }) {
        return (
            <View>
                <Image style={{ width: Dimensions.get('window').width }} source={Slide}></Image>
            </View>
        );
    }

    get pagination() {
        return (
            <Pagination
                
                dotsLength={3}
                activeDotIndex={this.state.activeDotIndex}
                containerStyle={HomeStyles.topCarouselPagination}
                dotStyle={HomeStyles.topCarouselDot}
                inactiveDotStyle={{
                    // Define styles for inactive dots here
                }}
                inactiveDotOpacity={0.7} 
                inactiveDotScale={1}
            />
        );
    }

    componentDidMount(){
        // const IS_RTL = this.props.navigation.getScreenProps().IS_RTL;
        // I18nManager.forceRTL(true);
        AsyncStorage.getItem("userId").then((value) => {
            Utils.logData("User Id ", value);
        }).done();

        this.getLocation()
    }

    getLocation = async () => {
    const hasLocationPermission = await this.hasLocationPermission();

    if (!hasLocationPermission) return;

    this.setState({ showLoading: true }, () => {
      Geolocation.getCurrentPosition(
        (position) => {
          this.setState({ location: position});
          Utils.logData('position '+JSON.stringify(position));
          this.getLocationDetails()
        },
        (error) => {
          if (Utils.isIOS()) {//iOS
                    if (error.code == 1){//User denied permission access

                    }
                    else if (error.code == 2){//Location disabled by user in device.

                    }
                }
                else//android
                {
                  if (error.code == 1){//User denied permission access

                    }
                    else if (error.code == 2){//Location disabled by user in device.

                    }
                    else if (error.code == 5){//{ message: 'Location settings are not satisfied.', code: 5 }

                    }  
                }
          this.setState({ location: error, showLoading: false });
          Utils.logData('error hereeeeere '+JSON.stringify(error));
        },
        { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000, distanceFilter: 50 }
      );
    });
  }

  hasLocationPermission = async () => {
    if (Utils.isIOS() ||
        (Utils.isIOS() == false && Utils.platformVersion() < 23)) {
      return true;
    }

    const hasPermission = await PermissionsAndroid.check(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
    );

    if (hasPermission) return true;

    const status = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
    );

    if (status === PermissionsAndroid.RESULTS.GRANTED) return true;

    if (status === PermissionsAndroid.RESULTS.DENIED) {
      ToastAndroid.show('Location permission denied by user.', ToastAndroid.LONG);
    } else if (status === PermissionsAndroid.RESULTS.NEVER_ASK_AGAIN) {
      ToastAndroid.show('Location permission revoked by user.', ToastAndroid.LONG);
    }

    return false;
  }


    getLocationDetails() {
        var longitude = this.state.location.coords.longitude
        var latitude = this.state.location.coords.latitude
        var googleApiKey = constants.googleApiKey
        //url='https://maps.googleapis.com/maps/api/geocode/json?address='+ latitude + ',' +longitude + '&key=' +  googleApiKey
        
        var reqHelper = new RequestHelper()
        var requestUrl = reqHelper.getLocationDetails(latitude,longitude,googleApiKey)
        

        fetch(requestUrl)
        .then((response) => response.json())
        .then((responseJson) => {
            Utils.logData('responseJson '+JSON.stringify(responseJson))
            var users_formatted_address = Utils.getDisplayAddressFromGoogleApiResponse(responseJson)
            Utils.logData('users_formatted_address from func '+users_formatted_address)

            //var users_formatted_address1 = Utils.getDisplayAddressFromGoogleApiResponses(responseJson)
            //Utils.logData('users_formatted_address from func 1 '+users_formatted_address1)
            var place_id = responseJson.results[constants.indexOfComponentToGet].place_id
            var availableSearchResult = new SearchResult(users_formatted_address,place_id,latitude,longitude)
            Utils.logData('formatted address availableSearchResult '+JSON.stringify(availableSearchResult))
            this.onLocationDetailsAvailable(availableSearchResult)            
        });
    }

    getHomePageAreaAndCuisines(latitude,longitude) 
    {
        Utils.logData('LOCATION FOUND latitude'+latitude)
        Utils.logData('LOCATION FOUND longitude'+longitude)
        currentComponentHomeScreen.setState({ 
                    isGPSEnabled : true,
                    showLoading:true
                })
        var reqHelper = new RequestHelper()
        var requestUrl = reqHelper.getHomePageAreaAndCuisines()
        var networkHelper = new NetworkHelper();
        var requestParams = {"lang_code": "en",'lattitude':latitude,'longitude':longitude};
        networkHelper.setUrl(requestUrl);
        networkHelper.setMethod(constants.methodPost);
        networkHelper.setData(JSON.stringify(requestParams));

        networkHelper.execute(function (responseData) 
        {
            Utils.logData("homepage getHomePageAreaAndCuisines Response"+JSON.stringify(responseData)); 
            if (responseData.IsSuccess == true){
                Utils.logData("homepage getHomePageAreaAndCuisines Response");
                var cuisinesArray = []
                var cuisines = responseData.Data.cuisines
                for (var i = 0;i <cuisines.length; i++) {
                    var id = cuisines[i].cuisine_id
                    var cuisine_name = cuisines[i].cuisine_name
                    var image = cuisines[i].image
                    var restaurant_count = cuisines[i].restaurant_count
                    var lattitude = cuisines[i].lattitude
                    var longitude = cuisines[i].longitude
                    var cuisine = new Cuisines(id,cuisine_name,image,lattitude,longitude,restaurant_count)
                    cuisinesArray.push(cuisine)
                }
                var areasArray = []
                var areas = responseData.Data.area
                for (var j = 0;j <areas.length; j++) { 
                    var id = areas[j].area_id                   
                    var area_name = areas[j].area_name
                    var area_image = areas[j].image
                    var restaurant_count = areas[j].restaurant_count
                    var latitude = areas[j].lattitude 
                    var longitude = areas[j].longitude
                    var distance_in_km = areas[j].distance_in_km
                    var area = new Areas(id,area_name,area_image,latitude,longitude,distance_in_km,restaurant_count)
                    areasArray.push(area)
                 }
                 Utils.logData("cuisine array  "+JSON.stringify(cuisinesArray))
                 Utils.logData("areas array  "+JSON.stringify(areasArray))
                currentComponentHomeScreen.setState({ 
                    showLoading : false,
                    isRestaurantsAvailble : true,
                    restauraurantByCuisines: cuisinesArray,
                    restauraurantByAreas : areasArray,
                    canRenderCuisines : true,
                    canRenderAreas : true
                })
                
                /*Utils.logData("cuisine_name "+cuisine_name)
                Utils.logData("restaurant_count "+restaurant_count)
                Utils.logData("image "+image)
                Utils.logData("area_name "+area_name)
                Utils.logData("area_wise_restaurant_count "+area_wise_restaurant_count)
                Utils.logData("area_image "+area_image)
                Utils.logData("homepage getHomePageAreaAndCuisines Response");*/
            } 
            else{
                ToastUtils.showErrorToast(responseData.Message);
                currentComponentHomeScreen.setState({ 
                    showLoading:false,
                    restauraurantByCuisines: [],
                    restauraurantByAreas : [],
                    canRenderCuisines : false,
                    canRenderAreas : false,
                    isRestaurantsAvailble : false,
                })
            }
        }, function (errorMessage, statusCode) 
        {
            Utils.logData(errorMessage);
            ToastUtils.showErrorToast(constants.somethingwent_wrong_msg);
            currentComponentHomeScreen.setState({ 
                    showLoading:false
                })
        }, function()
            {
                ToastUtils.showErrorToast(constants.no_network_msg);
                currentComponentHomeScreen.setState({ 
                    showLoading:false
                })
            });
    }

    goToSearchLocation()
    {
        Utils.logData('going to search location')
        this.props.navigation.navigate('SearchLocation',{ onLocationDetailsAvailable : this.onLocationDetailsAvailable});
    }


    onLocationDetailsAvailable(searchResult)
    {
        Utils.logData('onLocationDetailsAvailable Homescreen '+JSON.stringify(searchResult))
        this.setState({
                showLoading:true,
                availableLocation : searchResult
            },this.getHomePageAreaAndCuisines(searchResult.latitude,searchResult.longitude))
    }

    render() {

        var spinner = null
        if (this.state.showLoading) {
            spinner = <Spinner visible={this.state.showLoading} 
            // textContent={"Loading..."} 
            textStyle={reusableStyles.loaderStyle} />
        }        
        Utils.logData('description on home screen '+this.state.availableLocation.description)       
        return (
            <SafeAreaView style = { reusableStyles.droidSafeArea } >
            {spinner}
            <View style={{...HomeStyles.mainWrapper}}>
            {this.state.canShowPendingOrderView ? <TouchableOpacity style={HomeStyles.waitngRow}>
                    <View style={{flex:7,flexDirection:'row',alignItems: 'center'}}>
                        <Image source={require('../../assets/images/waiting-image.png')} 
                            style={{width:31,height:27}} ></Image>
                        <Text style={[HomeStyles.redText,{marginLeft:10,marginTop:5}]}>
                            Waiting for confirmation from{"\n"}Jumjoji - The Parsi Diner
                        </Text>
                    </View>
                    <Text style={[HomeStyles.redText,HomeStyles.viewText,{flex:1}]}>VIEW</Text>
                </TouchableOpacity> : null}
                
                <View style={{flex:5}}>
                <ScrollView  style={{width:'100%', height:'100%'}}>
                    <View style={HomeStyles.homeHeader}>
                        <View style={HomeStyles.headerText}>
                            <Image style={HomeStyles.logo} source={Logo} ></Image>
                            <View>
                                <Text style={{...HomeStyles.restoLocation,textAlign:'left'}}>{I18n.t('homeBannerText')}</Text>
                                <View style={{ marginTop: 5, flexDirection: 'row', alignItems: 'center', marginRight:15}}>
                                    <TouchableOpacity style={{width:'100%',flexDirection:'row'}} 
                                    onPress = {() => this.goToSearchLocation()}>
                                        <Icon name="map-pin" color="#ffffff" style={[HomeStyles.backIcon,{marginTop:2}]}></Icon>
                                        <Text style={HomeStyles.restoName}  
                                        numberOfLines = { 1 }>  {this.state.availableLocation.description}</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                        <Carousel
                            style={HomeStyles.topCarousel}
                            ref={(c) => { this._carousel = c; }}
                            data={[1, 2, 3]}  
                            autoplayInterval={3000} 
                            enableSnap={true}
                            renderItem={this._renderItem} startAutoplay={true}
                            sliderWidth={Dimensions.get('window').width}
                            itemWidth={Dimensions.get('window').width}
                            showsHorizontalScrollIndicator={true}
                            loop={false} 
                            autoplay={true}
                            showsHorizontalScrollIndicator={true} 
                            onSnapToItem={(index) => this.setState({ activeDotIndex: index })}
                        />
                        {this.pagination}
                    </View>
                    <View style={HomeStyles.contentWrapper}>
                        <View style={HomeStyles.scrollWrapper}>
                            {this.state.isGPSEnabled ? null : <ErrorComp navigation = {this.props.navigation} isGPSEnabled = {false} onLocationDetailsAvailable = {this.onLocationDetailsAvailable}></ErrorComp> }
                            {
                                (this.state.isGPSEnabled &&  !this.state.isRestaurantsAvailble) && 
                                <ErrorComp navigation = {this.props.navigation} isRestaurantsAvailble ={false} onLocationDetailsAvailable = {this.onLocationDetailsAvailable}></ErrorComp>}
                            {this.state.canRenderCuisines ?<SectionCarousel  navigation = {this.props.navigation} data ={this.state.restauraurantByCuisines} restaurantByType = {RestaurantByType.CUISINES} searchResult= {this.state.availableLocation}></SectionCarousel> : null}
                            {this.state.canRenderAreas ? <SectionCarousel  navigation = {this.props.navigation} data ={this.state.restauraurantByAreas} restaurantByType = {RestaurantByType.AREAS} searchResult= {this.state.availableLocation}></SectionCarousel>: null}
                            <BannerCTA navigation = {this.props.navigation}></BannerCTA>
                        </View>
                    </View>
                </ScrollView>
                </View>
                <BottomNav navigation = {this.props.navigation} viewType={0}></BottomNav>
            </View>
            </SafeAreaView>
        )
    }

}