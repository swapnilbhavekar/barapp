import { StyleSheet, Dimensions } from 'react-native';
import { bold } from 'ansi-colors';

var themeRed = "#f01616",
themeGrey="#a3a3a3",
themelightGrey="#595959",
fnt1='OpenSans-Regular',
fnt2='OpenSans-Bold';
fnt3='OpenSans-SemiBold';

const { width , height} = Dimensions.get('window');

export const HomeStyles = StyleSheet.create({
    mainWrapper: {
        flex:1,
        justifyContent: 'flex-start'
    },
    homeHeader : {
        position: 'relative',
        width: width,
        padding: 0,
        margin:0,
    },
    headerText: {
        position: 'absolute',
        top:40,
        left:30,
        zIndex: 1,
    },
    logo: {
        marginBottom:20,
    },
    restoName : {
        color: '#fff',
        fontFamily:fnt2,
        fontSize:16,
        lineHeight:16,
        fontWeight:'bold',
        marginTop:5,
        marginLeft:0,
        width:'85%'
},
    restoLocation : {
        color: '#fff',
        fontFamily:fnt2,
        fontSize:12,
        margin: 0,
    },
    topCarousel: {
        
    },
    topCarouselPagination:{
        position: 'absolute',
        bottom: 30,
        zIndex: 2,
        width: width,
    },
    topCarouselDot:{
        width:7,
        height:7,
        backgroundColor: '#ffffff',
        borderRadius: 7,
        margin:0,
        padding:0
    },
    contentWrapper: {
        backgroundColor: '#ffefeb',
        paddingHorizontal:20,
        flex:1,
        justifyContent: 'flex-start'
    },
    scrollWrapper: {
        position: 'relative',
        flex: 1,
        top: - 50
    },
    waitngRow:{
        flex:.4,
        flexDirection:'row',
        alignItems: 'center',
        backgroundColor:'#fff0f0',
        width:'100%',
        paddingRight:15,
        paddingLeft:15,
        paddingTop:5,
        paddingBottom:5,
    },
    redText:{
        color:themeRed,
        fontSize:14,
        lineHeight:14,
        fontFamily:fnt3
    },
    viewText:{
        color:themeRed,
        fontSize:14,
        lineHeight:14,
        fontFamily:fnt2
    },
    backIcon: {
        fontSize:18,
        lineHeight:18
    },
})