import React, { Component } from 'react';
import { Text, View, Image, TextInput, TouchableOpacity, ImageBackground,Dimensions } from 'react-native';
import constants from "../Helpers/AppConstants";
import Utils from "../Helpers/Utils";
import Swiper from 'react-native-swiper'
import ToastUtils from "../Helpers/ToastUtils";
import ImageModel from "../Models/ImageModel";

export default class ImageSlider extends React.Component {


  constructor(props) {
    super(props);    
    this.state = {
      selectedImageDataSource: [],
    };
      currentComponentImageSlider = this;
  }

  componentWillMount()
  {
    this.setState(
    {
      selectedImageDataSource: currentComponentImageSlider.props.navigation.state.params.images,
      })
  }

  renderRow(data,index)
  {
    Utils.logData('renderRow data '+data.path)
    const SCREEN_WIDTH = Dimensions.get("window").width;
    const SCREEN_HEIGHT = Dimensions.get("window").height;
    return(
               <Image resizeMode="contain" source={{uri: data.path}} 
                style={{width: SCREEN_WIDTH,
        height: SCREEN_HEIGHT,}}
               />
      )
  }

  renderPagination (index, total, context){
  return (
    <View style={{
    justifyContent: 'center',
    alignItems: 'center',
    bottom: 10,
    right: 10}}>
      <Text style={{ color: 'grey' }}>
        <Text style={{ color: 'black',
    fontSize: 20}}>{index + 1}</Text>/{total}
      </Text>
    </View>
  )
}

  render() {
    return (
         <Swiper index = {0} showsButtons ={true} renderPagination={this.renderPagination} 
         loop = {false} style ={{}} showsPagination = {true}
         >
         {
          this.state.selectedImageDataSource.map((data, index) =>
          this.renderRow(data,index)
          )          
         }
         </Swiper>
    );
  }
}
