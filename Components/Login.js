import React, { Component } from 'react';
import { Text, View, Image, ScrollView, ImageBackground, TouchableOpacity, SafeAreaView, AsyncStorage, Dimensions, Platform } from 'react-native';
import { reusableStyles } from '../Helpers/CommonStyles.js';
import { reusableArStyles } from '../Helpers/CommonArStyles.js';
import { TextField } from 'react-native-material-textfield';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import iconmoonConfig from '../selection.json';
import OAuthManager from 'react-native-oauth';
const Icon = createIconSetFromIcoMoon(iconmoonConfig);
import I18n from '../Helpers/i18n';
import { getLanguages } from 'react-native-i18n';
const manager = new OAuthManager('firestackexample')
import ToastUtils from "../Helpers/ToastUtils";
import constants from "../Helpers/AppConstants";
import RequestHelper from '../Helpers/RequestHelper.js';
import NetworkHelper from '../Helpers/NetworkHelper.js';
import Utils from "../Helpers/Utils.js";
import Validator from '../Helpers/Validator.js';
import Spinner from 'react-native-loading-spinner-overlay';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

export default class Login extends React.Component {
    constructor() {
        super();
        this.state = {
            email: '',
            password: '',
            switch1Value: false,
            isArabic: false,
            showLoading: false,
            scrollViewHeight:0

        }
        currentComponentLogin = this;
    }

    toggleSwitch1 = (value) => {
        this.setState({ switch1Value: value })
        console.log('Switch 1 is: ' + value)
    }

    onPressFacebookButton() {
        console.log("Facebook button clicked");
        manager.configure({
            facebook: {
                client_id: '220228342213439',
                client_secret: 'eb794871e613b5b29c1766530a54bae8'
            }
        });
        //manager.deauthorize('facebook');
        manager.authorize('facebook', { scopes: 'email' })
            .then(resp => {
                console.log('Your users ID', resp);
                manager
                    .makeRequest('facebook', '/me?fields=id,name,email,gender')
                    .then(resp => {
                        console.log('Data ->', resp.data);
                    });
            })
            .catch(err => console.log('There was an error'));
    }

    onPressGoogleButton() {
        console.log("Google button clicked");
        if (Utils.isIOS()) {
            manager.configure({
                google: {
                    callback_url: `com.googleusercontent.apps.811824796543-imgn1uhhuovam51k1gvtkt8df5juco03:/oauth2redirect/google`,
                    client_id: '811824796543-imgn1uhhuovam51k1gvtkt8df5juco03.apps.googleusercontent.com',
                }
            });
        }
        else {
            manager.configure({
                google: {
                    callback_url: `com.googleusercontent.apps.811824796543-m4n18ls139b1qumjv5v6neq5b8r2qsq5:/oauth2redirect/google`,
                    client_id: '811824796543-m4n18ls139b1qumjv5v6neq5b8r2qsq5.apps.googleusercontent.com',
                    client_secret: 'OaKA5QMvjeznRH46DHD7iTcG'
                }
            });
        }

        //manager.deauthorize('google');
        manager.authorize('google', { scopes: 'email' })
            .then(resp => {
                console.log('Your users ID', resp);
                manager
                    .makeRequest('google', 'https://www.googleapis.com/plus/v1/people/me')
                    .then(resp => {
                        console.log('Data ->', resp.data);
                    });
            })
            .catch(err => console.log('There was an error'));
    }

    onPressContinueButton() {
        let errors = {};

        if (!Validator.isValidString(currentComponentLogin.state.email)) {
            errors["email"] = constants.empty_email_id_error;
            //ToastUtils.showErrorToast(constants.invalid_credential_error);
            //return;
        }

        if (!Validator.isValidString(currentComponentLogin.state.password)) {
            errors["password"] = constants.empty_password_error;
            //ToastUtils.showErrorToast(constants.invalid_credential_error);
            //return;
        }

        this.setState({ errors });

        if (Object.keys(errors).length === 0){
            this.verifyUser();
        }
    }

    componentWillMount() {
        // if(!I18nManager.isRTL) {
        //     I18nManager.forceRTL(true);
        //     RNRestart.Restart();
        // }

        // if (this.state.isArabic) {
        //     reusableStyles.themeBtn = reusableArStyles.themeBtn;
        //     reusableStyles.textField = reusableArStyles.textField;
        //     reusableStyles.redLinkSmall = reusableArStyles.redLinkSmall;
        //     reusableStyles.socialLoginBox = reusableArStyles.socialLoginBox;
        //     reusableStyles.socialLoginElementText = reusableArStyles.socialLoginElementText;
        // }

        /*getLanguages().then(languages => {
            console.log(languages + "Current Language"); // ['en-US', 'en']
            if(languages[0] == "en-US"){
                console.log("English" + reusableStyles.redLinkSmall);
            }
            else if(languages[0] != "en-US"){
                console.log("Its Arabic" + reusableArStyles.themeBtn);
                reusableStyles.themeBtn = reusableArStyles.themeBtn;
                reusableStyles.textField = reusableArStyles.textField;
                reusableStyles.redLinkSmall = reusableArStyles.redLinkSmall;
                reusableStyles.socialLoginElementText = reusableArStyles.socialLoginElementText;  
            }
          });*/
    }

    verifyUser() {
        currentComponentLogin.setState({
            showLoading: true,
        })
        console.log('logging sagar')
        var reqHelper = new RequestHelper()
        var requestUrl = reqHelper.loginUserRequest()
        var networkHelper = new NetworkHelper();
        var requestParams = { "EmailId": this.state.email, "Password": this.state.password };
        networkHelper.setUrl(requestUrl);
        networkHelper.setMethod(constants.methodPost);
        networkHelper.setData(JSON.stringify(requestParams));

        networkHelper.execute(function (responseData) {
            currentComponentLogin.setState({
                showLoading: false,
            })

            console.log("Login Response", responseData);   
            if (responseData.IsSuccess == true){
                if (responseData.Data.length > 0){
                    AsyncStorage.setItem('userId', responseData.Data[0].customer_id);
                    AsyncStorage.setItem('emailId', responseData.Data[0].email);
                    AsyncStorage.setItem('mobileNo', responseData.Data[0].contact_number);
                    AsyncStorage.setItem('name', responseData.Data[0].customer_name);
                }

                ToastUtils.showSuccessToast(responseData.Message);
                currentComponentLogin.props.navigation.navigate('Home');
            }
            else {
                ToastUtils.showErrorToast(responseData.Message);
            }
        }, function (errorMessage, statusCode) {
            currentComponentLogin.setState({
                showLoading: false,
            })
            console.log(errorMessage);
            ToastUtils.showErrorToast(constants.somethingwent_wrong_msg);
        }, function () {
            currentComponentLogin.setState({
                showLoading: false,
            })
            ToastUtils.showErrorToast(constants.no_network_msg);
        });
    }

    componentWillMount() {
        this.keyboardOpen = false;
    }

    onLayout(event) {
        if (!this.keyboardOpen) {
            var windowHeight = Dimensions.get('window').height;
            const { x, y, height, width } = event.nativeEvent.layout;
            svHeight = height - (Platform.OS === 'ios' ? 70 : 0);
            this.setState({ scrollViewHeight: svHeight })
            //console.log(height, svHeight);
        }
        this.keyboardOpen = true;
    }

    render() {

        var spinner = null
        if (this.state.showLoading) {
            spinner = <Spinner visible={this.state.showLoading} 
            //textContent={"Loading..."} 
            textStyle={reusableStyles.loaderStyle} />
        }

        let { email } = this.state;
        let { password } = this.state;
        let { errors = {} } = this.state;

        const IS_RTL = this.props.navigation.getScreenProps().IS_RTL;

        return (
            <ImageBackground source={require('../assets/images/background-table.jpg')}
                style={{ ...reusableStyles.imageBackground, direction: IS_RTL ? 'rtl' : 'ltr' }} >
                {spinner}
                <SafeAreaView style={reusableStyles.droidSafeArea} >
                    <View style={reusableStyles.container} >
                        <View style={reusableStyles.headerBox}>
                            <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={{paddingVertical:10}}>
                                <Icon name="arrow-left" color="#f01616" style={reusableStyles.backBtnIcon}></Icon>
                            </TouchableOpacity>
                            <View style={reusableStyles.logoContainer}>
                                <Image source={require('../assets/images/book-a-resto-logo.png')}
                                    style={reusableStyles.logo}
                                />
                            </View>
                        </View>
                        <View style={reusableStyles.formBox}  onLayout={(event) => this.onLayout(event)}>
                            <KeyboardAwareScrollView keyboardShouldPersistTaps='always' contentContainerStyle={{ height: this.state.scrollViewHeight }}>
                                <View style={reusableStyles.scrollingBox}>
                                    <View>
                                        <TextField 
                                            label={I18n.t('userEmailId')} 
                                            value={email}
                                            onChangeText={(email) => this.setState({ email })}
                                            tintColor={inputColor}
                                            error={errors.email}
                                            containerStyle={[reusableStyles.textField, reusableStyles.w100]} />
                                        <TextField 
                                            label={I18n.t('userPassword')} 
                                            value={password}
                                            secureTextEntry={true}
                                            onChangeText={(password) => this.setState({ password })}
                                            tintColor={inputColor}
                                            error={errors.password}
                                            containerStyle={[reusableStyles.textField, reusableStyles.w100]} />
                                        <TouchableOpacity style={[reusableStyles.redLinkSmall, reusableStyles.mB10]} onPress={() => this.props.navigation.navigate('ForgotPassword')}>
                                            <Text style={reusableStyles.redLinkSmallTxt} >
                                                {I18n.t('forgotPasswordText')}
                                            </Text>
                                        </TouchableOpacity>
                                        {/*<View style={reusableStyles.separatorBox}>
                                        <View style={{ alignSelf: 'center', position: 'absolute', borderBottomColor: '#e8e8e8', borderBottomWidth: 1, height: '50%', width: '10%', left: '30%' }} />
                                        <Text style={reusableStyles.separatorTextColor}>{I18n.t('orText')}</Text>
                                        <View style={{ alignSelf: 'center', position: 'absolute', borderBottomColor: '#e8e8e8', borderBottomWidth: 1, height: '50%', width: '10%', left: '60%' }} />
                                    </View>
                                    <View style={reusableStyles.socialLoginBox}>
                                        <TouchableOpacity onPress={() => this.onPressGoogleButton()}>
                                            <View style={reusableStyles.socialLoginElement}>
                                                <Icon style={reusableStyles.socialIcon}
                                                    name="google" color="#ed3722" />
                                                <Text style={reusableStyles.socialLoginElementText}>
                                                    {I18n.t('googleSignInText')}
                                                </Text>
                                            </View>
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={() => this.onPressFacebookButton()}>
                                            <View style={reusableStyles.socialLoginElement}>
                                                <Icon style={reusableStyles.socialIcon}
                                                    name="facebook" color="#39579c" />
                                                <Text style={reusableStyles.socialLoginElementText}>
                                                    {I18n.t('facebookSignInText')}
                                                </Text>
                                            </View>
                                        </TouchableOpacity>
                                    </View> */}
                                    </View>
                                    <View style={[reusableStyles.bottomBtnContainer]}>
                                        <TouchableOpacity style={[reusableStyles.themeBtn, reusableStyles.mB10]} onPress={() => this.onPressContinueButton()}>
                                            <Text style={reusableStyles.themeBtnTxt} > {I18n.t('loginBtnText')} </Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity style={[reusableStyles.themeLink]} onPress={() => this.props.navigation.navigate('Signup')}>
                                            <Text style={reusableStyles.themeLinkTxt} > {I18n.t('loginSignUpBtnText')} </Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </KeyboardAwareScrollView>
                        </View>
                    </View>
                </SafeAreaView>
            </ImageBackground>
        )
    }
} 