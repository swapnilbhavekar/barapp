import React, { Component } from 'react';
import { Text, View, TouchableOpacity, Dimensions, StyleSheet, ListView, ImageBackground,  FlatList, SafeAreaView} from 'react-native';
import { reusableStyles } from '../Helpers/CommonStyles.js';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import iconmoonConfig from '../selection.json';
const Icon = createIconSetFromIcoMoon(iconmoonConfig);
import I18n from '../Helpers/i18n';
import { getLanguages } from 'react-native-i18n';
import Utils from "../Helpers/Utils";
import ToastUtils from "../Helpers/ToastUtils";
import constants from "../Helpers/AppConstants";
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import { Marker } from 'react-native-maps';
import Restaurant from "../Models/Restaurant";
import Icon1 from 'react-native-vector-icons/FontAwesome5';

const IS_RTL = false;
const screen = Dimensions.get('window');
const ASPECT_RATIO = screen.width / screen.height;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

const styles = StyleSheet.create({
    mapContainer:{
        ...StyleSheet.absoluteFillObject,
        flex:1,
        position:'relative',
    },
    mapObject:{
        ...StyleSheet.absoluteFillObject,
        position:'absolute',
        top:0,
        left:0,
        height:'100%',
        width:'100%',
        // backgroundColor:'blue'
    },
    listView:{
        position:'absolute',
        bottom:0,
        left:0,
        width:'100%',
        height:220,
        backgroundColor:'#ddd'
    },
    listImageItem :{
        height:50,
        flexDirection: 'column', 
        alignItems: 'flex-end', 
        justifyContent: 'space-between',
        borderRadius:10,
        backgroundColor:'blue'
    },
    listItem :{
        marginTop:15,
        marginBottom:20,
        marginHorizontal:10,
        paddingHorizontal:15,
        paddingVertical:20,
        borderBottomColor:'#dfdfdf',
        borderBottomWidth:1,
        borderRadius:5,
        backgroundColor:'#fff',
    },
    overlay: {
        position: 'absolute',
        left: 0,
        top:0,
        right:0,
        bottom:0,
        backgroundColor:'#000000',
        opacity: 0.6,
        flex:1,
        zIndex:0,
        borderRadius:5
    },
    backIcon: {
        fontSize:18,
        lineHeight:18
    },
});

const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

export default class MapViewComponent extends React.Component {
    state = {
        isRTL: false,

    }

    constructor(props) {
        super(props);
        this.mapRef = null;
        currentComponentMapView = this;
        mapPinData = [];
        restaurantsList = this.props.navigation.state.params.restaurantsList;

        this.generateMapPinsData();

        this.state = {
            dataSource: ds.cloneWithRows(restaurantsList),
            markerIndex: 0,
            scrollViewHeight:0,

        };
    }

    generateMapPinsData(){
        for (i=0; i<restaurantsList.length; i++)
        {
            let mapData = {
                title: '' + (i+1),
                id: i,
                key: i,
                description: '',
                address: '',
                latlng:{
                    latitude: parseFloat(restaurantsList[i].lattitude),
                    longitude: parseFloat(restaurantsList[i].longitude),
                    latitudeDelta: LATITUDE_DELTA,
                    longitudeDelta: LONGITUDE_DELTA
                }

            };
            mapPinData.push(mapData);
        }
        console.log("Pins Data ", mapPinData);
    }

    onLayout(event) {
        const { x, y, height, width } = event.nativeEvent.layout;
        console.log(width + " Width");
        this.setState({ scrollViewHeight:(width-50)});
    }

    goToRestaurantDetails(restaurant)
    {
        this.props.navigation.navigate('RestaurantDetails',{'restaurant':restaurant})
    }

    // renderRestaurantsListView = ({item}) => (
        
    //     //var restaurant = item;
    //     <View style={[styles.listItem]}>
    //         <ImageBackground style={styles.listImageItem} source={{uri : item.cover_image}} imageStyle={{borderRadius:5}}>
    //             <View style={{ width: 27, height: 12, marginTop:10 }}>
    //                 <ImageBackground source={require('../assets/images/ratings.png')}
    //                     style={{ flex: 1, alignItems: 'center', }}>
    //                     <Text style={{ color: '#fff', fontSize: 10 }}>4 *</Text>
    //                 </ImageBackground>
    //             </View>
    //             <View style={{ flexDirection: 'row', alignSelf:'flex-end', marginTop: 5, height:15}}>
    //                 <TouchableOpacity >
    //                     <Icon name="favourites" color="#ffffff" style={reusableStyles.ratingHeart}></Icon>
    //                 </TouchableOpacity>
    //                 <View style={reusableStyles.ratingTextContainer}>
    //                     <Text style={reusableStyles.ratingText}>
    //                         {item.distance_in_km}km
    //                     </Text>
    //                 </View>
    //             </View>
    //         </ImageBackground>
    //         <Text style={reusableStyles.restaurantHeading}>{item.name}</Text>
    //         <Text style={reusableStyles.restaurantHeading2}>{item.cuisines}</Text>
    //         <View style={reusableStyles.details}>
    //             <Text style={[reusableStyles.detailsText, reusableStyles.brdrRight]}>{item.city_name}</Text>
    //             <Text style={[reusableStyles.detailsText, reusableStyles.brdrRight]}>{item.avg_cost_per_person}</Text>
    //             <Text style={[reusableStyles.detailsText]}>Booked {" "+item.booked_today_count+" "}times day</Text>
    //         </View>
    //         <View style={reusableStyles.btnsContainer}>
    //             <TouchableOpacity style={[reusableStyles.redThumbnailBtn]}>
    //                 <Text style={reusableStyles.redThumbnailBtnText}>06.00pm</Text>
    //             </TouchableOpacity>
    //             <TouchableOpacity style={reusableStyles.redThumbnailBtn}>
    //                 <Text style={reusableStyles.redThumbnailBtnText}>06.30pm</Text>
    //             </TouchableOpacity>
    //             <TouchableOpacity style={reusableStyles.redThumbnailBtn}>
    //                 <Text style={reusableStyles.redThumbnailBtnText}>07.00pm</Text>
    //             </TouchableOpacity>
    //             <TouchableOpacity style={reusableStyles.redThumbnailBtn}>
    //                 <Text style={reusableStyles.redThumbnailBtnText}>more..</Text>
    //             </TouchableOpacity>
    //         </View>
    //     </View>
    // );

    markerClicked(e){
        console.log("Marker Clicked ", e);
        this.refs.ListView_Reference.scrollToIndex({animated: true, index: "" + e.id});
    }

    onViewableItemsChanged = ({ viewableItems, changed }) => {
        console.log("Visible items are", viewableItems);
        console.log("changed ", changed);
        if (viewableItems.length > 0){
            if (viewableItems[0].index != this.state.markerIndex){
                this.setState({
                    markerIndex: viewableItems[0].index
                }, () => {
                    this.mapRef.fitToElements(true);
                });
            }
        }
    } 

    render() {
        const IS_RTL = this.state.isRTL;
        return (
            <SafeAreaView style = {[reusableStyles.droidSafeArea]} >
            <View style={{ ...reusableStyles.mainWrapper, direction: IS_RTL ? 'rtl' : 'ltr',width:'100%',height:'100%'}}>
                <View style={reusableStyles.header}>
                    <View style={reusableStyles.headerTitleWrapper}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                            <Icon name="arrow-left" color="#ffffff" style={[styles.backIcon,{ marginRight: 10 }]}></Icon>
                        </TouchableOpacity>
                        <Text style={reusableStyles.headerTitle}>All Restaurants</Text>
                    </View>
                </View>
                <View style={styles.mapContainer}>
                    {
                        (Utils.isIOS()) ? (<MapView
                        style={reusableStyles.map}
                        minZoomLevel={2}  // default => 0
                        maxZoomLevel={15} // default => 20
                        showsUserLocation={false}
                        ref={ref => { this.mapRef = ref }}
                        onLayout={() => this.mapRef.fitToElements(true)}
                    >
                    {
                        mapPinData.map((marker) =>(
                            <MapView.Marker key = {marker.key} coordinate={marker.latlng} title={marker.title} onPress={e => this.markerClicked(marker)}>
                                <View style={{flex:1, justifyContent:'center',alignItems:'center',jusbackgroundColor:'blue',position:'relative'}}>
                                    {
                                        this.state.markerIndex == marker.id ? (<Icon1 name="map-marker" color="#6D2426" size={40}></Icon1>) : (<Icon1 name="map-marker" color="#CA4549" size={35}></Icon1>)
                                    }
                                    <Text style={{color: "white", fontWeight: 'bold', fontSize:12, position:'absolute'}}>{marker.title}</Text>
                                </View>
                                <MapView.Callout tooltip={true} />
                            </MapView.Marker>
                        ))
                    }
                    </MapView>) : (<MapView
                        provider={PROVIDER_GOOGLE} // remove if not using Google Maps
                        style={reusableStyles.map}
                        minZoomLevel={2}  // default => 0
                        maxZoomLevel={15} // default => 20
                        showsUserLocation={false}
                        ref={ref => { this.mapRef = ref }}
                        onLayout={() => this.mapRef.fitToElements(true)}
                    >
                    {
                        mapPinData.map((marker) =>(
                            <MapView.Marker key = {marker.key} coordinate={marker.latlng} title={marker.title} onPress={e => this.markerClicked(marker)}>
                                <View style={{flex:1, justifyContent:'center',alignItems:'center',jusbackgroundColor:'blue',position:'relative'}}>
                                    {
                                        this.state.markerIndex == marker.id ? (<Icon1 name="map-marker" color="#6D2426" size={40}></Icon1>) : (<Icon1 name="map-marker" color="#CA4549" size={35}></Icon1>)
                                    }
                                    <Text style={{color: "white", fontWeight: 'bold', fontSize:12, position:'absolute'}}>{marker.title}</Text>
                                </View>
                                <MapView.Callout tooltip={true} />
                            </MapView.Marker>
                        ))
                    }
                    </MapView>)
                    }
                    
                    <View style={[styles.listView]} 
                    onLayout={(event) => this.onLayout(event)}>
                        <FlatList
                            horizontal={true}
                            removeClippedSubviews={false}
                            style={{flex:1}}
                            data={restaurantsList}
                            ref='ListView_Reference'
                            onViewableItemsChanged={this.onViewableItemsChanged }
                            viewabilityConfig={{
                                itemVisiblePercentThreshold: 70
                            }}
                            renderItem={({item}) =>
                                <TouchableOpacity 
                                onPress = {() => this.goToRestaurantDetails(item)} 
                                style={[styles.listItem,{width: this.state.scrollViewHeight}]}>
                                    <ImageBackground style={styles.listImageItem} source={{uri : item.cover_image}} imageStyle={{borderRadius:5}}>
                                        <View style={styles.overlay}></View>
                                        <View style={{ width: 27, height: 12, marginTop:10 }}>
                                            <ImageBackground source={require('../assets/images/ratings.png')} style={{ flex: 1, alignItems: 'center', }}>
                                                <Text style={{ color: '#fff', fontSize: 10 }}>4 *</Text>
                                            </ImageBackground>
                                        </View>
                                        <View style={{ flexDirection: 'row', alignSelf:'flex-end', marginTop: 5, height:15}}>
                                            <Text style={reusableStyles.ratingText}>
                                                    {Utils.roundOfValue(item.distance_in_km)}km
                                                </Text>
                                        </View>
                                    </ImageBackground>
                                    <Text style={reusableStyles.restaurantHeading}>{item.name}</Text>
                                    <Text style={reusableStyles.restaurantHeading2} numberOfLines = { 1 } >{item.cuisines}</Text>
                                    <View style={reusableStyles.details}>
                                        <Text style={[reusableStyles.detailsText, reusableStyles.brdrRight, {height:12}]}>{item.city_name}</Text>
                                        <Text style={[reusableStyles.detailsText, reusableStyles.brdrRight, {height:12}]}>{item.avg_cost_per_person}</Text>
                                        <Text style={[reusableStyles.detailsText]}>Booked {" "+item.booked_today_count+" "}times day</Text>
                                    </View>
                                    <View style={reusableStyles.btnsContainer}>
                                        <TouchableOpacity style={[reusableStyles.redThumbnailBtn, {marginTop:-5}]}>
                                            <Text style={reusableStyles.redThumbnailBtnText}>06.00pm </Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity style={[reusableStyles.redThumbnailBtn, {marginTop:-5}]}>
                                            <Text style={reusableStyles.redThumbnailBtnText}>06.30pm</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity style={[reusableStyles.redThumbnailBtn, {marginTop:-5}]}>
                                            <Text style={reusableStyles.redThumbnailBtnText}>07.00pm</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity style={[reusableStyles.redThumbnailBtn, {marginTop:-5}]}>
                                            <Text style={reusableStyles.redThumbnailBtnText}>More..</Text>
                                        </TouchableOpacity>
                                    </View>
                                </TouchableOpacity>
                            } 
                        />
                    </View>
                </View>
            </View>
            </SafeAreaView>
        )
    }
}