import React, { Component } from 'react';
import {Modal,Dimensions, View, Text, Image, TouchableOpacity,StyleSheet} from 'react-native';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import iconmoonConfig from '../selection.json';
import Carousel, { Pagination } from 'react-native-snap-carousel';
const Icon = createIconSetFromIcoMoon(iconmoonConfig);

var themeRed = "#f01616",
    themeGrey = "#a3a3a3",
    themelightGrey = "#595959",
    fnt1 = 'OpenSans-Regular',
    fnt2 = 'OpenSans-Bold';
fnt3 = 'OpenSans-SemiBold';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
    modalWrapper:{
        flex:1,
        backgroundColor:'#ffffff',
    },
    modalContentWrapper:{
        flex:1
    },
    modalHeader:{
        padding: 20,
        backgroundColor: themeRed,
        flexDirection: 'row',
        alignItems:'center',
        
    },
    modalHeaderIcon:{
        width:18,
        height:18,
        color:'#ffffff'
    },
    modalHeaderText:{
        fontFamily:fnt3,
        color:'#ffffff',
        marginLeft:10,
        fontSize:16
    },
    modalContent:{
        backgroundColor:'#060402',
        flex:1,
    },
    carousel:{
        flex:1,
        // padding: 
    },
    carouselItem:{
        flex:1,
        alignItems:'center',
        justifyContent:'center'
    },
    carouselItemImage:{
        width:width - 40,
        height: height - 200
    },
    modalFooter:{
        backgroundColor:'#4f0f12',
        padding: 10,
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center'
    },
    paginationBtnIcon:{
        width:16,
        height:16,
        color:'#ffffff'
    },
    paginationBtnText:{
        fontFamily:fnt3,
        color:'#ffffff',
        fontSize:16,
        marginHorizontal:65
    },

});

export default class MenuGalleryModal extends React.Component {
    constructor(props) {
    super(props);    
    this.state = {
      selectedImagesDataSource: [],
      activeSlide:1
    };
      currentComponentMenuGalleryModal = this;
      arrayOfImages = this.props.images
    }


    _renderItem({ item, index }) {
        return (
            <View style={styles.carouselItem}>
                <Image style={styles.carouselItemImage} source={{uri: item.path}}></Image>
            </View>
        );
    }

    componentDidMount(){
        this.setState({
            activeSlide:this.props.activeSlide
        });
    }
    

    render() {
        return (
            <Modal
                animationType="slide"
                transparent={true}
                visible={this.props.modalVisible}
                presentationStyle={'fullScreen'}>
                <View style={styles.modalWrapper}>
                    <View style={styles.modalContentWrapper}>
                        <View style={styles.modalHeader}>
                            <TouchableOpacity onPress={() => {
                            this.props.setModalVisible(this.props.modalName,false);
                            }}>
                                <Icon name="dropdown" color="#ffffff" style={styles.modalHeaderIcon}></Icon>
                            </TouchableOpacity>
                            <Text style={styles.modalHeaderText}>Food Menu</Text>
                        </View>
                        <View style={styles.modalContent}>
                            <Carousel
                                style={styles.carousel}
                                ref={(c) => { this._carousel = c; }}
                                data={arrayOfImages} 
                                enableSnap={true}
                                renderItem={this._renderItem} 
                                activeSlideAlignment={'center'}
                                sliderWidth={Dimensions.get('window').width}
                                itemWidth={Dimensions.get('window').width}
                                showsHorizontalScrollIndicator={false}
                                loop={false} 
                                autoplay={false}
                                onSnapToItem={(index) => this.setState({ activeSlide: index+1 })}
                            />
                        </View>
                        <View style={styles.modalFooter}>
                            <TouchableOpacity onPress={() => { this._carousel.snapToPrev(); }} style={styles.paginationBtn}>
                                <Icon name="dropdown" color="#ffffff" style={styles.paginationBtnIcon}></Icon>
                            </TouchableOpacity>
                            <Text style={styles.paginationBtnText}>{this.state.activeSlide}/{arrayOfImages.length}</Text>
                            <TouchableOpacity onPress={() => { this._carousel.snapToNext(); }} style={styles.paginationBtn}>
                                <Icon name="dropdown" color="#ffffff" style={styles.paginationBtnIcon}></Icon>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </Modal>
        )
    }
}