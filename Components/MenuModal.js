import React, { Component } from 'react';
import { Text, View, Image, Linking, ScrollView, Dimensions, TouchableOpacity, ImageBackground, SafeAreaView, StyleSheet } from 'react-native';
import { reusableStyles } from '../Helpers/CommonStyles.js';
import { TextField } from 'react-native-material-textfield';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import iconmoonConfig from '../selection.json';
const Icon = createIconSetFromIcoMoon(iconmoonConfig);
import I18n from '../Helpers/i18n';
import { getLanguages } from 'react-native-i18n';
import ToastUtils from "../Helpers/ToastUtils";
import constants from "../Helpers/AppConstants";
import RequestHelper from '../Helpers/RequestHelper.js'
import NetworkHelper from '../Helpers/NetworkHelper.js'
import Logo from '../assets/images/book-a-resto-logo-white.png';
import Slide from '../assets/images/slide1.jpg';
import BottomNav from './BottomNav/BottomNav';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { TextInput } from 'react-native-gesture-handler';

var themeRed = "#f01616",
    themeGrey = "#a3a3a3",
    themelightGrey = "#595959",
    fnt1 = 'OpenSans-Regular',
    fnt2 = 'OpenSans-Bold';
fnt3 = 'OpenSans-SemiBold';

const IS_RTL = false;

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
    mainWrapper: {
        flex:1,
        justifyContent: 'flex-start',
        paddingHorizontal: 20,
    },
    menuBlock:{
        marginTop: 20,
        marginBottom: 30
    },
    menuLabel:{
        fontFamily: fnt2,
        fontSize:12,
        color:'#000000',
        marginBottom: 15,
    },
    menuItemWrapper:{
        flexDirection:'row',
        alignItems: 'center',
    },
    menuItemBtn:{
        marginRight: 7
    },
    menuItem:{
        width:75,
        height:75,
        borderRadius: 3,
    },
    menuMoreBtn:{
        position: 'relative',
    },
    menuMoreBg:{
        position:'absolute',
        borderRadius:5,
        width:75,
        height:75,
        backgroundColor:'rgba(237,28,36,0.5)',
        alignItems:'center',
        justifyContent:'center'
    },
    menuMoreText:{
        fontFamily: fnt2,
        fontSize:12,
        color:'#000000',
    },
    separator: {
        width: width - 40,
        height: 1,
        backgroundColor: '#e8e8e8',
    },
});

export default class MenuModal extends React.Component {
    render() {
        return(
        <View style={styles.mainWrapper}>
            <ScrollView>
                <View style={styles.menuBlock}>
                    <Text style={styles.menuLabel}>Lunch</Text>
                    <View style={styles.menuItemWrapper}>
                        <TouchableOpacity style={styles.menuItemBtn}>
                            <Image style={styles.menuItem} source={require('../assets/images/italian.png')} />
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.menuItemBtn}>
                            <Image style={styles.menuItem} source={require('../assets/images/italian.png')} />
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.menuItemBtn}>
                            <Image style={styles.menuItem} source={require('../assets/images/italian.png')} />
                        </TouchableOpacity>
                        <TouchableOpacity style={{...styles.menuItemBtn,...styles.menuMoreBtn}}>
                            <Image style={styles.menuItem} source={require('../assets/images/italian.png')} />
                            <TouchableOpacity style={styles.menuMoreBg}>
                                <Text style={styles.menuMoreText}>More 10</Text>
                            </TouchableOpacity>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={styles.separator}></View>
                <View style={styles.menuBlock}>
                    <Text style={styles.menuLabel}>Food Menu</Text>
                    <View style={styles.menuItemWrapper}>
                        <TouchableOpacity style={styles.menuItemBtn}>
                            <Image style={styles.menuItem} source={require('../assets/images/italian.png')} />
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.menuItemBtn}>
                            <Image style={styles.menuItem} source={require('../assets/images/italian.png')} />
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.menuItemBtn}>
                            <Image style={styles.menuItem} source={require('../assets/images/italian.png')} />
                        </TouchableOpacity>
                        <TouchableOpacity style={{...styles.menuItemBtn,...styles.menuMoreBtn}}>
                            <Image style={styles.menuItem} source={require('../assets/images/italian.png')} />
                            <TouchableOpacity style={styles.menuMoreBg}>
                                <Text style={styles.menuMoreText}>More 10</Text>
                            </TouchableOpacity>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={styles.separator}></View>
                <View style={styles.menuBlock}>
                    <Text style={styles.menuLabel}>Bar Menu</Text>
                    <View style={styles.menuItemWrapper}>
                        <TouchableOpacity style={styles.menuItemBtn}>
                            <Image style={styles.menuItem} source={require('../assets/images/italian.png')} />
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.menuItemBtn}>
                            <Image style={styles.menuItem} source={require('../assets/images/italian.png')} />
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        </View>
        )
    }
}