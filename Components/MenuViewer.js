import React, { Component } from 'react';
import { Text, View, Image, TextInput, TouchableOpacity, ImageBackground,ListView,Alert } from 'react-native';
import { reusableStyles } from '../Helpers/CommonStyles.js'
import constants from "../Helpers/AppConstants";
import Utils from "../Helpers/Utils";
import ImageCropPicker from 'react-native-image-crop-picker';
import Tiles from "react-native-tiles";
import ToastUtils from "../Helpers/ToastUtils";
import ImageModel from "../Models/ImageModel";

export default class MenuViewer extends React.Component {

  constructor(props) {
    super(props);
    ds1 = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
    this.state = {
      selectedImageDataSource: ds1,
    };
    var arrayOfImages = []
    currentComponentMenuViewer = this;
  } 
  componentWillMount()
  {
    currentComponentMenuViewer.askOptionToPicImage();
  }


  askOptionToPicImage(){
      Alert.alert(
      'Select Option',
      'Select Option to take photo',
        [
        {
          text: 'Camera',
          onPress: () => this.openCaeraToCaptureImages(),
          style: 'cancel',
        },
        {text: 'Gallery', onPress: () => this.selectPhotoAndUpload()},
        ],
        {cancelable: false},
      );
  }

  selectPhotoAndUpload() {

    ImageCropPicker.openPicker({
      multiple: true,
      waitAnimationEnd: false,
      maxFiles: 20,
      mediaType: 'photo',
      includeBase64: false,
      compressImageMaxWidth: constants.compressImageMaxWidth,
      compressImageMaxHeight: constants.compressImageMaxHeight,
      compressImageQuality: constants.compressImageQuality
    }).then(images => 
    {
      if (images.length > 0)
        {
          arrayOfImages = []
          for (var i = 0; i < images.length; i++) 
          {
            Utils.logData("single image retreived==>> " + JSON.stringify(images[i]));
          var tempObject = new ImageModel(images[i].path,images[i].mime,images[i].size,images[i].data)
          arrayOfImages.push(tempObject)
          }
          currentComponentMenuViewer.setState(
        {
          selectedImageDataSource: ds1.cloneWithRows(arrayOfImages),
          showLoading: false,
        });
        }
    })
  }

  openCaeraToCaptureImages(){
      ImageCropPicker.openCamera({
      multiple: true,
      waitAnimationEnd: false,
      maxFiles: 20,
      mediaType: 'photo',
      includeBase64: false,
      compressImageMaxWidth: constants.compressImageMaxWidth,
      compressImageMaxHeight: constants.compressImageMaxHeight,
      compressImageQuality: constants.compressImageQuality,
      cropping: true
}).then(image => {
  console.log(image);
          arrayOfImages = []
          var tempObject = new ImageModel(image.path,image.mime,image.size,image.data)
          arrayOfImages.push(tempObject)
          currentComponentMenuViewer.setState(
        {
          selectedImageDataSource: ds1.cloneWithRows(arrayOfImages),
          showLoading: false,
        });
});
  }

  viewImage(rowData) {
    currentComponentMenuViewer.props.navigation.navigate('ImageSlider', { images: arrayOfImages })
  }

  render() {
    return (
      <View style={{flex: 1,flexWrap: 'wrap', flexDirection: 'row', justifyContent:'space-evenly', width:'100%'}}>
              <Tiles
                dataSource={this.state.selectedImageDataSource}
                style={{ marginTop: 10 }}
                tilesPerRow={2}
                renderItem={(rowData) => this.renderPreviewImages(rowData)}
              />
      </View>
    );
  }

  renderPreviewImages(rowData) {
    return (
      <View style={{position:'relative', backgroundColor:'#1d1d1d', marginBottom:30}}>
        <TouchableOpacity onPress={() => this.viewImage(rowData)}  style={{height:'100%', width:'100%'}}>
          <Image source={{uri: rowData.path}}
            style={{position: 'absolute', top: 0, left: 0, bottom: 0, right: 0}}
            resizeMode="contain" />
        </TouchableOpacity>
      </View>
    );

  }

}
