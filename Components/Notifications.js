import React, { Component } from 'react';
import { Text, View, Image, Linking, ScrollView, TouchableOpacity, SafeAreaView, StyleSheet } from 'react-native';
import { reusableStyles } from '../Helpers/CommonStyles.js';
import { TextField } from 'react-native-material-textfield';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import iconmoonConfig from '../selection.json';
const Icon = createIconSetFromIcoMoon(iconmoonConfig);
import I18n from '../Helpers/i18n';
import { getLanguages } from 'react-native-i18n';
import ToastUtils from "../Helpers/ToastUtils";
import constants from "../Helpers/AppConstants";
import RequestHelper from '../Helpers/RequestHelper.js'
import NetworkHelper from '../Helpers/NetworkHelper.js'
import Logo from '../assets/images/book-a-resto-logo-white.png';
import Slide from '../assets/images/slide1.jpg';
import BottomNav from './BottomNav/BottomNav';
import Carousel, { Pagination } from 'react-native-snap-carousel';

var themeRed = "#f01616",
    themeGrey = "#a3a3a3",
    themelightGrey = "#595959",
    fnt1 = 'OpenSans-Regular',
    fnt2 = 'OpenSans-Bold';
fnt3 = 'OpenSans-SemiBold';

const IS_RTL = false;

const styles = StyleSheet.create({
    mainWrapper: {
        flex:1,
        justifyContent: 'flex-start'
    },
    header: {
        backgroundColor: themeRed,
        height:55,
        alignItems:'flex-start',
        justifyContent:'center',
        paddingHorizontal:20
    },
    headerTitleWrapper: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    headerTitle: {
        fontFamily: fnt2,
        fontSize: 16,
        fontWeight: 'bold',
        color: '#fff'
    },
    contentWrapper: {
        backgroundColor: '#ffefeb',
        paddingTop:20,
        paddingHorizontal: 20,
        paddingBottom: 50,
        flex: 1,
        justifyContent: 'flex-start'
    },
    scrollWrapper: {
        backgroundColor:'#ffffff',
        flexDirection:'row',
        justifyContent:'space-between',
        flexWrap: 'wrap',
        flex: 1,
        paddingHorizontal:20,
        paddingVertical: 30,
        borderRadius: 5
    },
    slideWrapper:{
        marginBottom:30
    },
    slideItem:{
        width:86,
        height:104,
        marginRight:0,
        borderRadius: 5,
    },
    slideCatTitle:{
        fontFamily: fnt2,
        fontSize: 10,
        marginTop: 10,
        textAlign : IS_RTL ? 'right' : 'left'
    },
    slideCatCount:{
        fontFamily: fnt1,
        fontSize: 10,
        textAlign : IS_RTL ? 'right' : 'left'
    },
    backIcon: {
        fontSize:18,
        lineHeight:18
    },
})

export default class Notifications extends React.Component {
    state = {
        isRTL: false
    }


    render() {
        const IS_RTL = this.state.isRTL;
        return (
            <View style={{...styles.mainWrapper,direction:IS_RTL?'rtl':'ltr'}}>
                <View style={styles.header}>
                    <View style={styles.headerTitleWrapper}>
                        <TouchableOpacity 
                        onPress={() => this.props.navigation.goBack()} 
                        style={{paddingVertical:10}}>
                            <Icon name="arrow-left" color="#ffffff" 
                            style={[styles.backIcon,{marginRight:10}]}></Icon>
                        </TouchableOpacity>
                        <Text style={styles.headerTitle}>Notifications</Text>
                    </View>
                </View>
                <ScrollView>
                   
                </ScrollView>
                <BottomNav navigation = {this.props.navigation} viewType={3}></BottomNav>
            </View>
        )
    }
}