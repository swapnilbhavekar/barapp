import React, { Component } from 'react';
import { Text, View, Image, ImageBackground, TouchableOpacity, SafeAreaView,Dimensions, Platform } from 'react-native';
import { reusableStyles } from '../Helpers/CommonStyles.js'
import { TextField } from 'react-native-material-textfield';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import iconmoonConfig from '../selection.json';
const Icon = createIconSetFromIcoMoon(iconmoonConfig);
import I18n from '../Helpers/i18n';
import { getLanguages } from 'react-native-i18n';
import RequestHelper from '../Helpers/RequestHelper.js';
import NetworkHelper from '../Helpers/NetworkHelper.js';
import ToastUtils from "../Helpers/ToastUtils";
import constants from "../Helpers/AppConstants";
import Validator from '../Helpers/Validator.js';
import Spinner from 'react-native-loading-spinner-overlay';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';


export default class ResetPassword extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            newPassword: '',
            confirmPassword: '',
            scrollViewHeight: 0

        };

        mobileNo = this.props.navigation.state.params.mobileNo;
        customer_id = this.props.navigation.state.params.custId;
        currentComponentResetPwd = this;
    }

    onPressSubmitButton() {
        let errors = {};

        if (!Validator.isValidString(currentComponentResetPwd.state.newPassword)) {
            errors["newpassword"] = constants.empty_new_password_error;
            //ToastUtils.showErrorToast(constants.empty_new_password_error);
            //return;
        }

        this.setState({ errors });

        if (Object.keys(errors).length === 0){
            currentComponentResetPwd.resetPassword();
        }
    }

    resetPassword() {
        currentComponentResetPwd.setState({
            showLoading: true,
        })
        console.log('Reset Password Request')
        var reqHelper = new RequestHelper();
        var requestUrl = reqHelper.restPasswordRequest();
        var networkHelper = new NetworkHelper();
        var requestParams = { "mobile": mobileNo, "password": currentComponentResetPwd.state.newPassword, "customer_id": customer_id};
        networkHelper.setUrl(requestUrl);
        networkHelper.setMethod(constants.methodPost);
        networkHelper.setData(JSON.stringify(requestParams));

        networkHelper.execute(function (responseData) {
            currentComponentResetPwd.setState({
                showLoading: false,
            })
            console.log("Verify OTP Response", responseData);
            if (responseData.IsSuccess == true) {
                ToastUtils.showSuccessToast(responseData.Message);
                currentComponentResetPwd.props.navigation.navigate('Login');
            }
            else {
                ToastUtils.showErrorToast(responseData.Message);
            }
        }, function (errorMessage, statusCode) {
            currentComponentResetPwd.setState({
                showLoading: false,
            })
            console.log(errorMessage);
            ToastUtils.showErrorToast(constants.somethingwent_wrong_msg);
        }, function () {
            currentComponentResetPwd.setState({
                showLoading: false,
            })
            ToastUtils.showErrorToast(constants.no_network_msg);
        });
    }


    componentWillMount() {
        this.keyboardOpen = false;
    }

    onLayout(event) {
        if (!this.keyboardOpen) {
            var windowHeight = Dimensions.get('window').height;
            const { x, y, height, width } = event.nativeEvent.layout;
            svHeight = height - (Platform.OS === 'ios' ? 70 : 0);
            this.setState({ scrollViewHeight: svHeight })
            console.log(height);
        }
        this.keyboardOpen = true;
    }


    render() {
        let { newPassword } = this.state;
        let { confirmPassword } = this.state;
        let { errors = {} } = this.state;

        var spinner = null
        if (this.state.showLoading) {
            spinner = <Spinner visible={this.state.showLoading} 
            //textContent={"Loading..."} 
            textStyle={reusableStyles.loaderStyle} />
        }

        return (
            <ImageBackground source={require('../assets/images/background-table.jpg')}
                style={reusableStyles.imageBackground} >
                {spinner}
                <SafeAreaView style={reusableStyles.droidSafeArea} >
                    <View style={reusableStyles.container} >
                        <View style={reusableStyles.headerBox}>
                            <TouchableOpacity onPress={() => this.props.navigation.goBack()}  style={{paddingVertical:10}}>
                                <Icon name="arrow-left" color="#f01616" style={reusableStyles.backBtnIcon}></Icon>
                            </TouchableOpacity>
                            <View style={reusableStyles.logoContainer}>
                                <Image source={require('../assets/images/book-a-resto-logo.png')}
                                    style={reusableStyles.logo}
                                />
                            </View>
                        </View>
                        <View style={reusableStyles.formBox}    
                            onLayout={(event) => this.onLayout(event)} >
                            <KeyboardAwareScrollView keyboardShouldPersistTaps='always' contentContainerStyle={{ height: this.state.scrollViewHeight }}>
                                <View style={reusableStyles.scrollingBox}>
                                    <View>
                                        <View style={reusableStyles.infoBox}>
                                            <Text style={reusableStyles.infoHeading}>{I18n.t('resetPasswordHeadingText')}</Text>
                                            <Text style={reusableStyles.infoDetails}>{I18n.t('resetPasswordInfoText')}</Text>
                                        </View>
                                        <TextField 
                                            label={I18n.t('resetPasswordNewPasswordLabel')} 
                                            value={newPassword}
                                            onChangeText={(newPassword) => this.setState({ newPassword })}
                                            tintColor={inputColor} secureTextEntry={true}
                                            error={errors.newpassword}
                                            containerStyle={[reusableStyles.textField, reusableStyles.w100]} />
                                        <TextField 
                                            label={I18n.t('resetPasswordConfirmPasswordLabel')} 
                                            value={confirmPassword}
                                            onChangeText={(confirmPassword) => this.setState({ confirmPassword })}
                                            tintColor={inputColor} secureTextEntry={true}
                                            containerStyle={[reusableStyles.textField, reusableStyles.w100]} />
                                    </View>
                                    <View style={reusableStyles.bottomBtnContainer}>
                                        <TouchableOpacity style={reusableStyles.themeBtn}
                                            onPress={() => this.onPressSubmitButton()}>
                                            <Text style={reusableStyles.themeBtnTxt} >{I18n.t('resetPasswordSubmitBtn')}</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </KeyboardAwareScrollView>
                        </View>
                    </View>
                </SafeAreaView>
            </ImageBackground>
        )
    }
}