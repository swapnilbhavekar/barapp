import React, { Component } from 'react';
import { Text, View, Image, Linking, ScrollView, TouchableOpacity, SafeAreaView, Animated, StyleSheet, ImageBackground, Dimensions, Share, AsyncStorage, FlatList } from 'react-native';
import { reusableStyles } from '../Helpers/CommonStyles.js';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import iconmoonConfig from '../selection.json';
const Icon = createIconSetFromIcoMoon(iconmoonConfig);
import I18n from '../Helpers/i18n';
import { getLanguages } from 'react-native-i18n';
import BottomNav from './BottomNav/BottomNav';
import ReviewsModal from './ReviewsModal';
import AddReviewModal from './AddReviewModal';
import MenuModal from './MenuModal';
import CommonNetworkCalls from "../Helpers/CommonNetworkCalls.js";
import Utils from "../Helpers/Utils.js";
import { createOpenLink } from './MapsNavigation.js';
import RestoBarModal from './RestoBarModal.js';
import MenuGalleryModal from './MenuGalleryModal.js';
import ToastUtils from "../Helpers/ToastUtils";
import constants from "../Helpers/AppConstants";
import RequestHelper from '../Helpers/RequestHelper.js';
import NetworkHelper from '../Helpers/NetworkHelper.js';
import Restaurant from "../Models/Restaurant";
import ImageModel from "../Models/ImageModel";
import Spinner from 'react-native-loading-spinner-overlay';



var themeRed = "#f01616",
    themeGrey = "#a3a3a3",
    themelightGrey = "#595959",
    fnt1 = 'OpenSans-Regular',
    fnt2 = 'OpenSans-Bold';
fnt3 = 'OpenSans-SemiBold';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
    header: {
        paddingTop:20,
        paddingBottom:10,
    },
    headerTitleWrapper: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    actionRow: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingLeft: 20
    },
    filter: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    filterIconWrapper: {
        paddingHorizontal: 15,
    },
    filterIcon: {
        //color: '#fff',
    },
    ratingWrapper: {
        width: 35,
        height: 16,
        flexDirection: 'row',
        alignItems: 'flex-start',
        paddingLeft:10
    },
    ratingIcon: {
        width: 10,
        height: 10
    },
    ratingText: {
        color: '#fff',
        fontFamily: fnt1,
        fontSize: 12,
        marginRight: 5
    },
    HeaderRestoDetails: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-end',
        paddingTop: 70,
        paddingLeft: 20,
        paddingBottom: 10
    },
    restoAddressWrapper: {
    },
    restoAddressName: {
        color: '#fff',
        fontFamily: fnt2,
        fontSize: 16,
        marginBottom: 10,
    },
    restoTags: {
        color: '#fff',
        fontFamily: fnt1,
        fontSize: 12,
        fontStyle: 'italic',
        width:'100%'
    },
    textBlock: {
        marginBottom: 20
    },
    blockTitle: {
        fontFamily: fnt2,
        fontSize: 12,
        marginBottom: 10,
    },
    blockTitlePadding: {
        marginBottom: 0,
    },
    blockSubTitle: {
        fontFamily: fnt2,
        fontSize: 10,
    },
    blockText: {
        fontFamily: fnt1,
        fontSize: 12,
        marginBottom: 5
    },
    restoActions: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: 15
    },
    restoActionItem: {
        borderRightWidth: 1,
        borderRightColor: '#e8e8e8',
        paddingRight: 15,
        paddingLeft: 15,
    },
    restoActionItemBtn: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    restoActionItemIcon: {
        fontSize:14,
        lineHeight:14,
        margin: 5
    },
    restoActionItemText: {
        color: themeRed,
        fontFamily: fnt2,
        fontSize: 10,
        textTransform: 'uppercase'
    },
    separator: {
        width: width - 40,
        height: 2,
        backgroundColor: '#e8e8e8',
        marginBottom: 20
    },
    accordion: {
        marginBottom: 20,
    },
    accordionTitle: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingBottom: 5
    },
    accordionTitleText: {
        color: themeRed,
        fontFamily: fnt2,
        fontSize: 12,
        marginRight: 10
    },
    accordionTitleicon: {
        width: 10,
        height: 10
    },
    accordionBody: {
        paddingTop: 20,
        overflow: 'hidden'
    },
    timingWrapper: {
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    timingToggleWrapper: {
        marginRight: 10,
        marginBottom: 10
    },
    timingToggle: {
        paddingVertical: 7,
        paddingHorizontal: 10,
        backgroundColor: themeRed,
        borderRadius: 5
    },
    timingText: {
        color: '#fff',
        fontFamily: fnt1,
        fontSize: 10
    },
    timingOfferText: {
        color: themeRed,
        fontSize: 10,
        marginTop: 3,
        paddingLeft: 7
    },
    albumBlock: {
        marginBottom: 20,
        width: width - 40
    },
    albumWrapper: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    albumRight: {
        marginLeft: 10,
    },
    albumBottom: {
        marginTop: 10,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    albumItem: {
        flex: 1,
        position: 'relative',
        borderRadius: 5,
        overflow: 'hidden'
    },
    albumItemCategory: {
        position: 'absolute',
        flex: 1,
        left: 0,
        bottom: 0,
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
        paddingVertical: 3,

    },
    albumItemCategoryText: {
        color: '#ffffff',
        fontFamily: fnt1,
        fontSize: 10,
        textAlign: 'center'
    },
    albumItemFoods: {
        width: width - 225,
        height: 100,
    },
    albumItemAmbience: {
        width: width - 200,
        height: 50,
    },
    albumItemAll: {
        width: width - 260,
        height: 40,
    },
    albumUpload: {
        width: width - 325,
        height: 40,
        backgroundColor: themeRed,
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
        borderRadius: 5
    },
    albumUploadIcon: {
        color: '#fff'
    },
    cusineItem: {
        paddingHorizontal: 15,
        paddingVertical: 3,
        backgroundColor: '#f4f4f4',
        borderRadius: 10,
        marginRight: 10,
        marginTop:15
    },
    cusineItemWrapper: {
        flexDirection: 'row',
        marginBottom: 20
    },
    cusineItemText: {
        fontFamily: fnt1,
        fontSize: 10,
        color: themeRed
    },
    infoItemsWrapper: {
        flexDirection: 'row',
        flexWrap: 'wrap'
    },
    infoItem: {
        flexDirection: 'row',
        alignItems: 'center',
        width: width / 2 - 20,
        marginBottom: 10
    },
    infoItemIcon: {
        width: 4,
        height: 8,
        marginRight: 10
    },
    infoItemText: {
        fontFamily: fnt1,
        fontSize: 12
    },  
    overlay: {
        position: 'absolute',
        left: 0,
        top:0,
        right:0,
        bottom:0,
        backgroundColor:'#000000',
        opacity: 0.6,
        flex:1,
        zIndex:0,
    },
    backIcon: {
        fontSize:18,
        lineHeight:18
    },
});


export default class RestaurantDetails extends React.Component {
    state = {
        //accordHeight: new Animated.Value(),
        accordMaxHeight: 0,
        accordionExpanded: true,
        heightFlag: false,
        reviewsModal: false,
        addReviewModal: false,
        menuModal: false,
        menuGalleryModal: false,
        isFavourite : false,
        restaurant : {},
        showLoading: false,
        restaurantDetails:{}
    }
    constructor(props)
    {
        super(props);

        this.setState({
            restaurant : this.props.navigation.state.params.restaurant
        })

        restaurantId = this.props.navigation.state.params.restaurant.id;
        currentComponentRestaurantDetails = this;
        arrayOfImages = []
    }

    componentWillMount()
    {
        Utils.logData('props on restaurantDetails '+JSON.stringify(this.props))
        this.getRestaurantDetails();
    }

    setModalVisible(modalName,visible) {
        this.setState({[modalName]: visible});
    }

    getRestaurantDetails(){
        currentComponentRestaurantDetails.setState({
            showLoading: true
        })

        var reqHelper = new RequestHelper()
        var requestUrl = reqHelper.getRestaurantDetails()
        var networkHelper = new NetworkHelper();
        var requestParams = {
            "lang_code": "en",
            "restaurant_id": restaurantId,            
            "guest_count":"2",
            "section":"Single",
            "slot_date":"2019-06-04"
        }
        networkHelper.setUrl(requestUrl);
        networkHelper.setMethod(constants.methodPost);
        networkHelper.setData(JSON.stringify(requestParams));

        networkHelper.execute(function (responseData) {
            Utils.logData("getRestaurant Details Response" + JSON.stringify(responseData));
            if (responseData.IsSuccess == true) {
                var restaurantDet = {};
                if (responseData.Data.length > 0){
                    var restaurants = responseData.Data;
                    var id = restaurants[0].restaurant_id;
                    var lattitude = restaurants[0].lattitude;
                    var longitude = restaurants[0].longitude;
                    var distance_in_km = restaurants[0].distance_in_km;
                    var cover_image = restaurants[0].cover_image;
                    var name = restaurants[0].restaurant_name;
                    var cuisines = restaurants[0].cuisines;
                    var city_name = restaurants[0].city_name;
                    var avg_cost_per_person = restaurants[0].avg_cost_per_person;
                    var turn_time = restaurants[0].turn_time;
                    var booked_today_count = restaurants[0].booked_today_count;
                    var ratings = restaurants[0].ratings;
                    var restaurant_address = restaurants[0].restaurant_address;
                    var restaurant_info = restaurants[0].restaurant_info;
                    var reviews_count = restaurants[0].reviews_count;
                    var offer_title = restaurants[0].offer_title;
                    var offer_description = restaurants[0].offer_description;
                    var restaurant_type = restaurants[0].restaurant_type;
                    var more_information = [];
                    if (restaurants[0].more_information != undefined){
                        more_information = Utils.getArrayFromString(restaurants[0].more_information);
                    }
                    var food_image_count = restaurants[0].food_image_count;
                    var food_image = restaurants[0].food_image;
                    var ambience_image_count = restaurants[0].ambience_image_count;
                    var ambience_image = restaurants[0].ambience_image;
                    var all_image_count = restaurants[0].all_image_count;
                    var all_image = restaurants[0].all_image;
                    var top_dishes = restaurants[0].top_dishes_people_order;
                    var people_liked = restaurants[0].people_liked;
                    var cuisinesArr = [];
                    if (cuisines != undefined){
                        cuisinesArr = Utils.getArrayFromString(cuisines);
                    }
                    var payment_type = restaurants[0].payment_type;
                    var payment_type_description = restaurants[0].payment_type_description;

                    var restaurant = new Restaurant(id, lattitude, longitude, distance_in_km,
                        cover_image, name, cuisines, city_name, avg_cost_per_person, turn_time, booked_today_count, ratings, restaurant_address, restaurant_info,
                        reviews_count, offer_title, offer_description, restaurant_type, more_information, food_image_count, food_image, ambience_image_count,
                        ambience_image, all_image_count, all_image, top_dishes, people_liked, cuisinesArr, payment_type, payment_type_description);
                    restaurantDet = restaurant;
                }
                currentComponentRestaurantDetails.setState({
                    showLoading: false,
                    restaurantDetails: restaurantDet
                })
                //storing this restaurant to show in recent while searching restaurant
                Utils.saveSelectedRestaurant(restaurantDet)
            }
            else {
                ToastUtils.showErrorToast(responseData.Message);
                currentComponentRestaurantDetails.setState({
                    showLoading: false
                })
            }
        }, function (errorMessage, statusCode) {
                Utils.logData(errorMessage);
                ToastUtils.showErrorToast(constants.somethingwent_wrong_msg);
                currentComponentRestaurantDetails.setState({
                    showLoading: false
                })
            }, function () {
                ToastUtils.showErrorToast(constants.no_network_msg);
                currentComponentRestaurantDetails.setState({
                    showLoading: false
                })
            });
    }

    // _setmaxHeight(event) {
    //     if (!this.state.heightFlag) {
    //         this.setState({
    //             heightFlag: true,
    //             accordMaxHeight: event.nativeEvent.layout.height
    //         });
    //     }
    // }

    // toggleAccordion() {

    //     let height = this.state.accordionExpanded ? 0 : this.state.accordMaxHeight;

    //     this.setState({
    //         accordionExpanded: !this.state.accordionExpanded
    //     });

    //     this.state.accordHeight.setValue(height);
    //     Animated.spring(     //Step 4
    //         this.state.accordHeight,
    //         {
    //             toValue: height
    //         }
    //     ).start();
    // }

    addRemoveToFavourites()
    {
        Utils.logData('addRemoveToFavourites restaurantDetails'+JSON.stringify(this.state.restaurant))
        AsyncStorage.getItem("userId").then((userId) => {
            console.log("User Id ", userId);
            if(userId)
            {
               var commonNetworkCalls = new CommonNetworkCalls();
               var action = 'add'
               Utils.logData('isFavourite '+currentComponentRestaurantDetails.state.isFavourite)
               if (currentComponentRestaurantDetails.state.isFavourite) {
                action = 'delete'
               }
               var requestParams = {'restaurant_id' : this.state.restaurant.id,"customer_id":userId,"action":action}
               commonNetworkCalls.addRemoveToFavourites(requestParams,function()
            {
                currentComponentRestaurantDetails.setState({
                    isFavourite : !currentComponentRestaurantDetails.state.isFavourite
                })
            });
            }
            else
            {
                this.props.navigation.navigate('Login');
            }
            }).done();        
    }

    sharePressed(){
        Share.share({
            message: 'I think you\'d like this restaurant I found on Book A Rest:',
            url: 'http://bam.tech',
            title: 'Share with'
        }, {
            // Android only:
            dialogTitle: 'Share with',
            // iOS only:
            // excludedActivityTypes: [
            //     'com.apple.UIKit.activity.PostToTwitter'
            // ]
        })
    }

    captureImage(){
        this.props.navigation.navigate('MenuViewer');
    }
    goToImageSlider(){
        this.props.navigation.navigate('ImageSlider', { images: arrayOfImages })
    }

    getImages(type){
        currentComponentRestaurantDetails.setState({
            showLoading: true
        })

        var reqHelper = new RequestHelper()
        var requestUrl = reqHelper.getRestaurantPhotosByType()
        var networkHelper = new NetworkHelper();
        var requestParams = {
            "restaurant_id":currentComponentRestaurantDetails.state.restaurantDetails.id,
            "photo_type":type
        }
        networkHelper.setUrl(requestUrl);
        networkHelper.setMethod(constants.methodPost);
        networkHelper.setData(JSON.stringify(requestParams));

        networkHelper.execute(function (responseData) {
            Utils.logData("getImages Response" + JSON.stringify(responseData));
            if (responseData.IsSuccess == true) {
                arrayOfImages = []
                var restaurantDet = {};
                if (responseData.Data.length > 0){
                    var imagesFromServer = responseData.Data   
          for (var i = 0; i < imagesFromServer.length; i++) 
          {
            Utils.logData("single image retreived==>> " + JSON.stringify(imagesFromServer[i]));
          var tempObject = new ImageModel(imagesFromServer[i].image_url,imagesFromServer[i].mime,imagesFromServer[i].size,imagesFromServer[i].data)
          arrayOfImages.push(tempObject)
          }
                }
                currentComponentRestaurantDetails.setState({
                    showLoading: false,
                })
                currentComponentRestaurantDetails.setModalVisible('menuGalleryModal',true)
                //currentComponentRestaurantDetails.goToImageSlider() 
            }
            else {
                ToastUtils.showErrorToast(responseData.Message);
                currentComponentRestaurantDetails.setState({
                    showLoading: false
                })
            }
        }, function (errorMessage, statusCode) {
                Utils.logData(errorMessage);
                ToastUtils.showErrorToast(constants.somethingwent_wrong_msg);
                currentComponentRestaurantDetails.setState({
                    showLoading: false
                })
            }, function () {
                ToastUtils.showErrorToast(constants.no_network_msg);
                currentComponentRestaurantDetails.setState({
                    showLoading: false
                })
            });
    }

    render() {
        // let accordHeight = this.state.accordHeight;
        // const start = 'SOHO, New York City, NY';
        const end = this.state.restaurantDetails.restaurant_address;//'Times Square Building, Andheri - Kurla Rd, Gamdevi, Marol, Andheri East, Mumbai, Maharashtra 400059';
        const travelType = 'drive';

        var slotsArray = ["6.00pm", "6.30pm", "7.00pm", "7.30pm", "8.00pm", "8.30pm", "9.00pm", "9.30pm", "10.00pm", "11.00pm"];

        var spinner = null
        if (this.state.showLoading) {
            spinner = <Spinner visible={this.state.showLoading} 
            // textContent={"Loading..."} 
            textStyle={reusableStyles.loaderStyle} />
        }

        return (
            <View style={{ ...reusableStyles.mainWrapper }}>
            {spinner}
                <RestoBarModal modalVisible={this.state.reviewsModal} modalTitle={'Reviews ( 123 )'} modalName={'reviewsModal'} setModalVisible={this.setModalVisible.bind(this)}>
                    <ReviewsModal></ReviewsModal>
                </RestoBarModal>
                <RestoBarModal modalVisible={this.state.addReviewModal}modalTitle={'Add Review'} modalName={'addReviewModal'} setModalVisible={this.setModalVisible.bind(this)}>
                    <AddReviewModal></AddReviewModal>
                </RestoBarModal>
                <RestoBarModal modalVisible={this.state.menuModal}modalTitle={'Menu'} modalName={'menuModal'} setModalVisible={this.setModalVisible.bind(this)}>
                    <MenuModal></MenuModal>
                </RestoBarModal>
                <MenuGalleryModal activeSlide={1} modalVisible={this.state.menuGalleryModal} modalName={'menuGalleryModal'} setModalVisible={this.setModalVisible.bind(this)} images ={arrayOfImages}>
                </MenuGalleryModal>
                <ScrollView>
                    <ImageBackground source={{uri : this.state.restaurantDetails.cover_image}} style={styles.header}>
                        <View style={styles.overlay}></View>
                        <View>
                            <View style={styles.actionRow}>
                                <View style={styles.headerTitleWrapper}>
                                    <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                                        <Icon name="arrow-left" color="#ffffff" 
                                        style={[styles.backIcon, { marginRight: 10 }]}></Icon>
                                    </TouchableOpacity>
                                </View>
                                <View style={styles.filter}>
                                    <TouchableOpacity onPress = {() => this.addRemoveToFavourites()} style={{ ...styles.filterIconWrapper}}>
                                       {this.state.isFavourite ? <Icon name="favourites-fill" color={themeRed} ></Icon> : <Icon name="favourites" color="#ffffff" ></Icon>}
                                        
                                    </TouchableOpacity>
                                    <TouchableOpacity style={{...styles.filterIconWrapper, borderLeftColor: '#fff', borderLeftWidth: 1 }} onPress = {() => this.sharePressed()}>
                                        <Icon name="share" color="#ffffff" style={styles.filterIcon}></Icon>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View style={styles.HeaderRestoDetails}>
                                <View style={styles.restoAddressWrapper}>
                                    <Text style={styles.restoAddressName}>{this.state.restaurantDetails.name}</Text>
                                    <Text style={styles.restoTags} numberOfLines = { 1 }  >{this.state.restaurantDetails.cuisines}</Text>
                                </View>
                                <ImageBackground source={require('../assets/images/ratingRibbon.png')} style={styles.ratingWrapper} >
                                    <Text style={styles.ratingText}>{this.state.restaurantDetails.ratings}</Text>
                                    <Icon name="Rating-fill" color="#ffffff" style={styles.ratingIcon} ></Icon>
                                </ImageBackground>
                            </View>
                        </View>
                    </ImageBackground>
                    <View style={reusableStyles.scrollWrapper}>
                        <View style={styles.textBlock}>
                            <Text style={styles.blockTitle}>Address</Text>
                            <Text style={styles.blockText}>{this.state.restaurantDetails.restaurant_address}</Text>
                        </View>
                        <View style={styles.restoActions}>
                            <View style={{ ...styles.restoActionItem, paddingLeft: 0 }}>
                                <TouchableOpacity style={styles.restoActionItemBtn} onPress = {createOpenLink({ travelType, end })}>
                                    <Icon name="map-pin" color={themeRed} style={styles.restoActionItemIcon} ></Icon>
                                    <Text style={styles.restoActionItemText}>GET DIRECTIONS</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={styles.restoActionItem}>
                                <TouchableOpacity onPress={() => {
                  this.setModalVisible('menuModal',true);
                }} style={styles.restoActionItemBtn}>
                                    <Icon name="ticket" color={themeRed} style={styles.restoActionItemIcon} ></Icon>
                                    <Text style={styles.restoActionItemText}>MENU</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{ ...styles.restoActionItem, borderRightWidth: 0 }}>
                                <TouchableOpacity onPress={() => {
                  this.setModalVisible('reviewsModal',true);
                }} style={styles.restoActionItemBtn}>
                                    <Icon name="rating" color={themeRed} style={styles.restoActionItemIcon} ></Icon>
                                    <Text style={styles.restoActionItemText}>REVIEWS ({this.state.restaurantDetails.reviews_count})</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={styles.separator}></View>
                        <View style={styles.accordion}>
                            <TouchableOpacity 
                                //onPress={this.toggleAccordion.bind(this)} 
                            style={styles.accordionTitle}>
                                <Text style={styles.accordionTitleText}>Table for 03, 25 Jan 2019</Text>
                                <Icon name="dropdown" style={styles.accordionTitleIcon} ></Icon>
                            </TouchableOpacity>
                            <View style={styles.accordionBody}>
                                <FlatList
                                    contentContainerStyle={{paddingBottom: 15, justifyContent:'space-between'}}
                                    horizontal={false}
                                    numColumns={4}
                                    columnWrapperStyle={{flexWrap: 'wrap', flex: 1, paddingTop: 5 }}
                                    data={slotsArray}
                                    renderItem={(item) =>                                
                                        <View style={styles.timingToggleWrapper}>
                                            <TouchableOpacity style={styles.timingToggle}>
                                                <Text style={styles.timingText}>{item.item}</Text>
                                            </TouchableOpacity>
                                        </View>
                                    }
                                />
                            </View>
                        </View>
                        <View style={styles.separator}></View>
                        <View style={styles.textBlock}>
                            <Text style={styles.blockTitle}>Special Offer</Text>
                            <Text style={styles.blockSubTitle}>{this.state.restaurantDetails.offer_title}</Text>
                            <Text style={styles.blockText}>{this.state.restaurantDetails.offer_description} </Text>
                        </View>
                        <View style={styles.separator}></View>
                        <View style={styles.albumBlock}>
                            <Text style={styles.blockTitle}>Albums</Text>
                            <View style={styles.albumWrapper}>
                                <TouchableOpacity style={styles.albumItem} onPress ={()=>this.getImages(constants.Foods)}>
                                    <ImageBackground 
                                    imageStyle={{ borderRadius: 5 }} 
                                    style={styles.albumItemFoods} 
                                    source={{uri : this.state.restaurantDetails.food_image}} >
                                        <View style={{ ...styles.albumItemCategory, width: width - 225 }}>
                                            <Text style={styles.albumItemCategoryText}>Foods ({this.state.restaurantDetails.food_image_count})</Text>
                                        </View>
                                    </ImageBackground>
                                </TouchableOpacity>
                                <View style={styles.albumRight}>
                                    <TouchableOpacity style={styles.albumItem} onPress ={()=>this.getImages(constants.Ambience)}>
                                        <ImageBackground imageStyle={{ borderRadius: 5 }} style={styles.albumItemAmbience} source={{uri: this.state.restaurantDetails.ambience_image}}>
                                            <View style={{ ...styles.albumItemCategory, width: width - 200 }}>
                                                <Text style={styles.albumItemCategoryText}>Ambience ({this.state.restaurantDetails.ambience_image_count})</Text>
                                            </View>
                                        </ImageBackground>
                                    </TouchableOpacity>
                                    <View style={styles.albumBottom}>
                                        <TouchableOpacity style={styles.albumItem} onPress ={()=>this.getImages(constants.All)}>
                                            <ImageBackground imageStyle={{ borderRadius: 5 }} style={styles.albumItemAll} source={{uri: this.state.restaurantDetails.all_image}} >
                                                <View style={{ ...styles.albumItemCategory, width: width - 260 }}>
                                                    <Text style={styles.albumItemCategoryText}>All ({this.state.restaurantDetails.all_image_count})</Text>
                                                </View>
                                            </ImageBackground>
                                        </TouchableOpacity>
                                        <TouchableOpacity style={styles.albumUpload} onPress={() => this.captureImage()}>
                                            <Icon name="camera" style={styles.albumUploadIcon} ></Icon>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        </View>
                        <View style={styles.separator}></View>
                        <View style={styles.cuisineBlock}>
                            <Text style={[styles.blockTitle, styles.blockTitlePadding]}>Cuisines</Text>
                            <FlatList
                                contentContainerStyle={{paddingBottom: 15, justifyContent:'space-between'}}
                                horizontal={false}
                                numColumns={4}
                                columnWrapperStyle={{flexWrap: 'wrap', flex: 1, paddingTop: 5 }}
                                data={this.state.restaurantDetails.cuisinesArray}
                                renderItem={(item) =>                                
                                    <View style={styles.cusineItem}>
                                        <Text style={styles.cusineItemText}>{item.item}</Text>
                                    </View>
                                }
                            />
                        </View>
                        <View style={styles.separator}></View>
                        {
                            (this.state.restaurantDetails.top_dishes_people_order != undefined) ? (
                                <View>
                                    <View style={styles.textBlock}>
                                        <Text style={styles.blockTitle}>Top Dishes People Order</Text>
                                        <Text style={styles.blockText}>Biryani, Tandoori Chicken, Crispie Chicken, Brownie, Khepsa, Pasta, Chinese</Text>
                                    </View>
                                    <View style={styles.separator}></View> 
                                </View>
                            )
                            : null
                        }
                        {
                            (this.state.restaurantDetails.people_liked != undefined) ? (
                                <View>
                                    <View style={styles.textBlock}>
                                        <Text style={styles.blockTitle}>People Liked</Text>
                                        <Text style={styles.blockText}>Friendly Staff, Courteous Staff, Prompt Service, Decor,Dance Floor</Text>
                                    </View>
                                    <View style={styles.separator}></View>
                                </View>
                            ) 
                            : null
                        }
                        <View style={styles.textBlock}>
                            <Text style={styles.blockTitle}>Type</Text>
                            <Text style={styles.blockText}>{this.state.restaurantDetails.restaurant_type}</Text>
                        </View>
                        <View style={styles.separator}></View>
                        <View style={{flexDirection:'column'}}>
                        <View style={styles.textBlock}>
                            <Text style={styles.blockTitle}>Average Cost</Text>
                            <Text style={styles.blockSubTitle}>{this.state.restaurantDetails.avg_cost_per_person}</Text>
                            <Text style={styles.blockText}>Exclusive all taxes</Text>
                        </View>
                        {
                            (this.state.restaurantDetails.payment_type != undefined) ? (
                                <View style={styles.textBlock}>
                                    <Text style={styles.blockSubTitle}>{this.state.restaurantDetails.payment_type}</Text>
                                    {
                                        (this.state.restaurantDetails.payment_type_description != undefined) ? (
                                            <Text style={styles.blockText}>{this.state.restaurantDetails.payment_type_description}</Text>
                                        )
                                        : null
                                    }
                                </View>
                            )
                            : null
                        }
                        </View>
                        
                        
                        <View style={styles.separator}></View>
                        <View style={styles.moreInfoBlock}>
                            <Text style={styles.blockTitle}>More Information</Text>
                            <FlatList
                                contentContainerStyle={{margin:2}}
                                horizontal={false}
                                numColumns={2}
                                data={this.state.restaurantDetails.more_information}
                                renderItem={(item) =>                                
                                    <View style={styles.infoItem}>
                                        <Image style={styles.infoItemIcon} source={require('../assets/images/greenArrow.png')}></Image>
                                        <Text style={styles.infoItemText}>{item.item}</Text>
                                    </View>
                                }
                            />
                        </View>
                    </View>
                </ScrollView>
                <BottomNav navigation={this.props.navigation}></BottomNav>
            </View>
        )
    }
}