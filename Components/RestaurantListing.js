import React, { Component } from 'react';
import { Text, View, Image, Linking, ScrollView, TouchableOpacity, SafeAreaView, StyleSheet, ImageBackground } from 'react-native';
import { reusableStyles } from '../Helpers/CommonStyles.js';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import iconmoonConfig from '../selection.json';
const Icon = createIconSetFromIcoMoon(iconmoonConfig);
import I18n from '../Helpers/i18n';
import { getLanguages } from 'react-native-i18n';
import BottomNav from './BottomNav/BottomNav';
import ToastUtils from "../Helpers/ToastUtils";
import constants from "../Helpers/AppConstants";
import RequestHelper from '../Helpers/RequestHelper.js';
import NetworkHelper from '../Helpers/NetworkHelper.js';
import Utils from "../Helpers/Utils.js";
import Spinner from 'react-native-loading-spinner-overlay';
import Restaurant from "../Models/Restaurant";
import FilterModal from "./FilterModal";
import RestoBarModal from './RestoBarModal.js';
import CommonNetworkCalls from "../Helpers/CommonNetworkCalls.js";


var themeRed = "#f01616",
    themeGrey = "#a3a3a3",
    themelightGrey = "#595959",
    fnt1 = 'OpenSans-Regular',
    fnt2 = 'OpenSans-Bold';
fnt3 = 'OpenSans-SemiBold';

const styles = StyleSheet.create({
    header: {
        paddingHorizontal: 20,
        //paddingVertical:30,
        paddingTop: 30,
        paddingBottom: 10,
    },
    headerTitleWrapper: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    headerTitle: {
        fontFamily: fnt2,
        fontSize: 16,
        lineHeight:16,
        fontWeight: 'bold',
        color: '#fff',
    },
    actionRow: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    headerAddress: {
        color: '#fff',
        fontFamily: fnt1,
        fontSize: 12,
        marginLeft:30
    },
    filter: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    filterIconWrapper: {
        paddingHorizontal:15,
    },
    filterIcon: {
        fontSize:18,
        lineHeight:18
        },
    listingIcon:{
        paddingHorizontal:5
    },
    tableDD: {
        marginTop: 25,
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft:30
    },
    tableDDText: {
        color: '#fff',
        fontFamily: fnt2,
        fontSize: 12,
    },
    tableDDIcon: {
        padding: 10,
        marginLeft: 5
    },
    fullView: {
        backgroundColor: '#ffefeb'
    },
    overlay: {
        position: 'absolute',
        left: 0,
        top:0,
        right:0,
        bottom:0,
        backgroundColor:'#000000',
        opacity: 0.3,
        flex:1,
        zIndex:0,
        borderRadius:5
    },
    backBtnIcon:{
        fontSize:20,
        lineHeight:20,
        color: '#fff',
    },
    yellowLabel:{
        flex: 1,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
    }
});


export default class RestaurantListing extends React.Component {
    state = {
        isRTL: false,
        showLoading: false,
        restaurantData: [],
        filterModal:false,
        locationAddressTxt : '',
        restaurantSearchKeyType : '',

    }
    constructor(props) {
        super(props);
        currentComponentRestaurantListing = this;
    }

    componentWillMount() {
        Utils.logData('props on restaurant listing '+JSON.stringify(this.props))
        var selectedCuisineOrArea = this.props.navigation.state.params.selectedCuisineOrArea
        var searchResult = this.props.navigation.state.params.searchResult
        var otherDetails = this.props.navigation.state.params.otherDetails
        var restSearchKeyType = ''
        if (otherDetails.key == constants.favourites) {
            restSearchKeyType = 'My Favourites'
        }else{
            restSearchKeyType = 'All Restaurants'
        }
        this.setState({
            restaurantSearchKeyType : restSearchKeyType,
            locationAddressTxt : searchResult.description
        })
        var dataToSearch = {
            "lattitude": selectedCuisineOrArea.latitude, "longitude": selectedCuisineOrArea.longitude,
            "lang_code": "en", "guest_count": otherDetails.guest_count, "input_search": otherDetails.input_search,"key":otherDetails.key,
            "customer_id":"1","section":otherDetails.section,"slot_date":otherDetails.slot_date
        }
        this.getRestaurants(dataToSearch)
    }

    goToHome() {
        this.props.navigation.push('Home')
    }

    goToMapView() {
        this.props.navigation.navigate('MapViewComponent', { 'restaurantsList': this.state.restaurantData });
    }

    goToRestaurantDetails(restaurant) {
        this.props.navigation.navigate('RestaurantDetails', { 'restaurant': restaurant })
    }

    setModalVisible(modalName,visible) {
        this.setState({[modalName]: visible});
    }

    getRestaurants(requestParams) {
        currentComponentRestaurantListing.setState({
            showLoading: true
        })
        var commonNetworkCalls = new CommonNetworkCalls();
        commonNetworkCalls.getRestaurants(requestParams,function(responseData)
            {
            if (responseData.IsSuccess == true) {
                var restaurantsArray = []
                var restaurants = responseData.Data
                for (var i = 0; i < restaurants.length; i++) {

                    var id = restaurants[i].restaurant_id;
                    var lattitude = restaurants[i].lattitude;
                    var longitude = restaurants[i].longitude;
                    var distance_in_km = restaurants[i].distance_in_km;
                    var cover_image = restaurants[i].cover_image;
                    var name = restaurants[i].restaurant_name;
                    var cuisines = restaurants[i].cuisines;
                    var city_name = restaurants[i].city_name;
                    var avg_cost_per_person = restaurants[i].avg_cost_per_person;
                    var turn_time = restaurants[i].turn_time;
                    var booked_today_count = restaurants[i].booked_today_count;

                    var restaurant = new Restaurant(id, lattitude, longitude, distance_in_km,
                        cover_image, name, cuisines, city_name, avg_cost_per_person, turn_time, booked_today_count)
                    restaurantsArray.push(restaurant)
                }
                Utils.logData("restaurantsArray  " + JSON.stringify(restaurantsArray))
                currentComponentRestaurantListing.setState({
                    showLoading: false,
                    restaurantData: restaurantsArray
                })
            }
            else {
                ToastUtils.showErrorToast(responseData.Message);
                currentComponentRestaurantListing.setState({
                    showLoading: false
                })
            }
            },function (errorMessage, statusCode) {
                Utils.logData(errorMessage);
                ToastUtils.showErrorToast(constants.somethingwent_wrong_msg);
                currentComponentRestaurantListing.setState({
                    showLoading: false
                })
            }, function () {
                ToastUtils.showErrorToast(constants.no_network_msg);
                currentComponentRestaurantListing.setState({
                    showLoading: false
                })
            });
    }

    CardItem = (item) => {
        var restaurant = item.item
        var restaurantId = restaurant.id
        var lastRestaurantId = this.state.restaurantData[this.state.restaurantData.length - 1].id
        Utils.logData("restaurant  " + JSON.stringify(restaurant))
        return (<TouchableOpacity onPress={() => this.goToRestaurantDetails(restaurant)} style={[(restaurantId == lastRestaurantId) ? reusableStyles.lastListItem : reusableStyles.listItem]}>
            <ImageBackground style={reusableStyles.restaurantItem} 
            source={{ uri: restaurant.cover_image }} 
            imageStyle={{ borderRadius: 5}}>
                <View style={styles.overlay}></View>
                <View style={{ width: 27, height: 12, marginTop: 10,flexDirection: 'row', alignSelf: 'flex-end'}}>
                    <ImageBackground source={require('../assets/images/ratings.png')}
                        style={styles.yellowLabel}>
                        <Text style={{color:'#fff',fontSize:7,marginTop:-1}}>4</Text>
                        <Icon name="Rating-fill" color="#fff" style={{fontSize:7,marginLeft:1,marginTop:-1}}></Icon>
                    </ImageBackground>
                </View>
                <View style={{ flexDirection: 'row', alignSelf: 'flex-end', marginTop: 5, height: 15 }}>
                    {/* <TouchableOpacity style={styles.listingIcon}>
                        <Icon name="favourites" color="#ffffff" ></Icon>
                    </TouchableOpacity> */}
                    <Text style={[reusableStyles.ratingText, styles.listingIcon 
                    // ,{borderLeftColor: '#fff', borderLeftWidth: 1,paddingTop:3}
                    ]}>
                        {Utils.roundOfValue(restaurant.distance_in_km)}km
                    </Text>
                </View>
            </ImageBackground>
            <Text style={reusableStyles.restaurantHeading}>{restaurant.name}</Text>
            <Text style={reusableStyles.restaurantHeading2} numberOfLines={1} >{restaurant.cuisines}</Text>
            <View style={reusableStyles.details}>
                <Text style={[reusableStyles.detailsText, reusableStyles.brdrRight]}>{restaurant.city_name}</Text>
                <Text style={[reusableStyles.detailsText, reusableStyles.brdrRight]}>{restaurant.avg_cost_per_person}</Text>
                <Text style={[reusableStyles.detailsText]}>Booked {" " + restaurant.booked_today_count + " "}times day</Text>
            </View>
            <View style={reusableStyles.btnsContainer}>
                <TouchableOpacity style={reusableStyles.redThumbnailBtn}>
                    <Text style={reusableStyles.redThumbnailBtnText}>06.00pm</Text>
                </TouchableOpacity>
                <TouchableOpacity style={reusableStyles.redThumbnailBtn}>
                    <Text style={reusableStyles.redThumbnailBtnText}>06.30pm</Text>
                </TouchableOpacity>
                <TouchableOpacity style={reusableStyles.redThumbnailBtn}>
                    <Text style={reusableStyles.redThumbnailBtnText}>07.00pm</Text>
                </TouchableOpacity>
                <TouchableOpacity style={reusableStyles.redThumbnailBtn}>
                    <Text style={reusableStyles.redThumbnailBtnText}>More..</Text>
                </TouchableOpacity>
            </View>
        </TouchableOpacity>)
    }

    render() {
        const IS_RTL = this.state.isRTL;
        var spinner = null
        if (this.state.showLoading) {
            spinner = <Spinner visible={this.state.showLoading}
                //textContent={"Loading..."} 
                textStyle={reusableStyles.loaderStyle} />
        }
        return (
            <View style={{ ...reusableStyles.mainWrapper, ...styles.fullView }}>
                {spinner}
                <RestoBarModal modalVisible={this.state.filterModal}    
                modalName={'filterModal'} 
                setModalVisible={this.setModalVisible.bind(this)}>
                    <FilterModal></FilterModal>
                </RestoBarModal>
                <ImageBackground source={require('../assets/images/restaurant-details.png')} style={styles.header}>
                    <View>
                        <View style={styles.actionRow}>
                            <View style={styles.headerTitleWrapper}>
                                <TouchableOpacity onPress={() => this.goToHome()}
                                    style={{ paddingVertical: 10 }}>
                                    <Icon name="arrow-left" color="#ffffff" 
                                    style={[styles.backBtnIcon,{marginRight: 10}]}></Icon>
                                </TouchableOpacity>
                                <Text style={styles.headerTitle}>{this.state.restaurantSearchKeyType}</Text>
                            </View>
                            <View style={styles.filter}>
                                <TouchableOpacity style={{ ...styles.filterIconWrapper, borderRightColor: '#fff', borderRightWidth: 1 }} 
                                onPress={() => {this.setModalVisible('filterModal',true)}}>
                                    <Icon name="filter" color="#ffffff" style={styles.filterIcon}></Icon>
                                </TouchableOpacity>
                                <TouchableOpacity style={[styles.filterIconWrapper]} onPress={() => this.goToMapView()} >
                                    <Icon name="location-big" color="#ffffff" style={styles.filterIcon}></Icon>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <Text style={styles.headerAddress}>{this.state.locationAddressTxt}</Text>
                        <TouchableOpacity style={styles.tableDD}>
                            <Text style={styles.tableDDText}>Table for 2, tonight at 7:00 pm</Text>
                            <Icon style={styles.tableDDIcon} name="dropdown" color="#ffffff" ></Icon>
                        </TouchableOpacity>
                    </View>
                </ImageBackground>
                <ScrollView>
                    <View style={reusableStyles.contentWrapper}>
                        <View style={reusableStyles.scrollWrapper}>
                            {this.state.restaurantData.map((item, index) => {
                                return (
                                    <this.CardItem item={item}></this.CardItem>
                                )
                            })}
                        </View>
                    </View>
                </ScrollView>
                <BottomNav navigation={this.props.navigation}></BottomNav>
            </View>
        )
    }
}