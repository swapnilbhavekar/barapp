import React, { Component } from 'react';
import {Modal,Dimensions, View, Text, TouchableOpacity,StyleSheet} from 'react-native';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import iconmoonConfig from '../selection.json';
const Icon = createIconSetFromIcoMoon(iconmoonConfig);

var themeRed = "#f01616",
    themeGrey = "#a3a3a3",
    themelightGrey = "#595959",
    fnt1 = 'OpenSans-Regular',
    fnt2 = 'OpenSans-Bold';
fnt3 = 'OpenSans-SemiBold';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
    modalWrapper:{
        flex:1,
        backgroundColor:'rgba(0,0,0,0.7)',
    },
    modalCloseArea:{
        flex:0.3,
    },
    modalContent:{
        backgroundColor:'#fff',
        flex:1,
        borderTopRightRadius: 10,
        borderTopLeftRadius: 10,
        paddingVertical: 30,
    },
    modalCloseTitle:{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center'
    },
    modalTitleText:{
        fontFamily:fnt2,
        fontSize:16,
        paddingLeft:20
    },
    modalTitleIcon:{
        width:16,
        height:16,
        marginRight:20,
        color:'#000000'
    }
});

export default class RestoBarModal extends React.Component {
    state = {
        modalVisible: false
    }

    render() {
        return (
            <Modal
                animationType="slide"
                transparent={true}
                visible={this.props.modalVisible}
                onRequestClose={() => {
                    Alert.alert('Modal has been closed.');
                }}>
                <View style={styles.modalWrapper}>
                    <TouchableOpacity onPress={() => {
                            this.props.setModalVisible(this.props.modalName,false);
                            }} style={styles.modalCloseArea}></TouchableOpacity>
                    <View style={styles.modalContent}>
                        <TouchableOpacity onPress={() => {
                            this.props.setModalVisible(this.props.modalName,false);
                            }} style={styles.modalCloseTitle}>
                            <Text style={styles.modalTitleText}>{this.props.modalTitle}</Text>
                            <Icon name="dropdown" color="#ffffff" style={styles.modalTitleIcon}></Icon>
                        </TouchableOpacity>
                        {this.props.children}
                    </View>
                </View>
            </Modal>
        )
    }
}