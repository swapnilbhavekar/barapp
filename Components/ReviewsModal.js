import React, { Component } from 'react';
import { Text, View, Image, Linking, ScrollView, TouchableOpacity, ImageBackground, SafeAreaView, StyleSheet } from 'react-native';
import { reusableStyles } from '../Helpers/CommonStyles.js';
import { TextField } from 'react-native-material-textfield';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import iconmoonConfig from '../selection.json';
const Icon = createIconSetFromIcoMoon(iconmoonConfig);
import I18n from '../Helpers/i18n';
import { getLanguages } from 'react-native-i18n';
import ToastUtils from "../Helpers/ToastUtils";
import constants from "../Helpers/AppConstants";
import RequestHelper from '../Helpers/RequestHelper.js'
import NetworkHelper from '../Helpers/NetworkHelper.js'
import Logo from '../assets/images/book-a-resto-logo-white.png';
import Slide from '../assets/images/slide1.jpg';
import BottomNav from './BottomNav/BottomNav';
import Carousel, { Pagination } from 'react-native-snap-carousel';

var themeRed = "#f01616",
    themeGrey = "#a3a3a3",
    themelightGrey = "#595959",
    fnt1 = 'OpenSans-Regular',
    fnt2 = 'OpenSans-Bold';
fnt3 = 'OpenSans-SemiBold';

const IS_RTL = false;

const styles = StyleSheet.create({
    mainWrapper: {
        flex:1,
        justifyContent: 'flex-start'
    },
    tabBarScroller:{
        
    }, 
    tabBarWrapper:{
        flex:1,
        flexDirection:'row',
        marginTop:20,
        paddingHorizontal:20,
        borderBottomWidth: 1,
        borderBottomColor: '#fee5e5',
        alignSelf: 'flex-start'
    },
    tabBarItem:{
        marginRight:20,
        paddingHorizontal:3,
        paddingBottom: 7,
        opacity:0.7
    },
    activeTabBarItem:{
        opacity:1,
        borderBottomWidth: 1,
        borderBottomColor: themeRed,
    },
    tabBarItemText:{
        fontFamily:fnt1,
        fontSize:14,
        color:themeRed
    },
    activeTabBarItemText:{
        fontWeight: "600"
    },
    reviewsList:{
        paddingHorizontal:20,
    },
    reviewsItem:{
        paddingBottom:30,
        borderBottomColor:'#f8f8f8',
        borderBottomWidth:1,
        marginTop:30
    },
    reviewsItemHeader:{
        flexDirection:'row',
        
    },
    reviewUserImage:{
        borderRadius:50,
        width:53,
        height:53,
        marginRight:10
    },
    reviewUserDetails:{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'flex-start',
        borderBottomColor:'#f8f8f8',
        borderBottomWidth:1,
        paddingBottom:5,
    },
    reviewUserName_Name:{
        fontFamily:fnt1,
        fontSize:14
    },
    reviewUserName_Count:{
        marginTop:2,
        fontFamily:fnt1,
        fontSize:12
    },
    reviewUserFollowBtn:{
        paddingHorizontal:15,
        paddingVertical:5,
        borderRadius:5,
        backgroundColor:themeRed,
    },
    reviewUserFollowBtn_Text:{
        fontFamily:fnt3,
        color:'#fff',
        fontSize:10
    },
    reviewUserMeta:{
        flexDirection:'row',
        marginBottom:20
    },
    reviewUserMetaItem:{
        flexDirection:'row',
        alignItems:'center',
        marginTop:10,
        paddingHorizontal:15,
        borderRightColor: '#fee5e5',
        borderRightWidth:1
    },
    reviewUserMetaItem_RatingText:{
        fontFamily:fnt3,
        color:'#0ed30e',
        fontSize:14,
        marginRight:2
    },
    reviewUserMetaItem_RatingIcon:{
        width:12,
        height:12,
        color:'#0ed30e',
    },
    reviewUserMetaItem_PhotosIcon:{
        width:12,
        height:12,
        color:themeRed,
    },
    reviewUserMetaItem_PhotosText:{
        fontFamily:fnt3,
        color:themeRed,
        fontSize:12,
        marginLeft:5
    },
    reviewUserMetaItem_Days:{
        fontFamily:fnt1,
        fontSize:12,
        color:'#000000'
    },
    reviewsItemContent:{
        
    },
    reviewsItemContent_Text:{
        fontFamily:fnt1,
        fontSize:12,
        color:'#000000'
    },
    reviewsItemContent_ReadMoreText:{
        fontFamily:fnt2,
        color:themeRed,
        fontSize:10,
    },
    reviewsItemImagesList:{
        marginTop:20,
        flexDirection:'row'
    },
    reviewsItemImage:{
        width:50,
        height:50,
        borderRadius:5,
        marginRight:10
    },
    reviewsItemImage_add:{
        width:50,
        height:50,
        borderRadius:5,
        position:'relative'
    },
    reviewsItemImage_addBtn:{
        position:'absolute',
        borderRadius:5,
        width:'100%',
        height:'100%',
        backgroundColor:'rgba(237,28,36,0.7)',
        alignItems:'center',
        justifyContent:'center'
    },
    reviewsItemImage_addBtnText:{
        fontFamily:fnt2,
        fontSize:10,
        color:'#fff'
    }
});

export default class ReviewsModal extends React.Component {
    render() {
        return(
        <View style={styles.mainWrapper}>
            <ScrollView horizontal={true} style={{flexDirection:'row', height:70,alignSelf:'flex-start'}} contentContainerStyle={styles.tabBarScroller}>
                <View style={styles.tabBarWrapper}>
                    <TouchableOpacity style={{...styles.tabBarItem,...styles.activeTabBarItem}}>
                        <Text style={{...styles.tabBarItemText,...styles.activeTabBarItemText}}>Popular(10)</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.tabBarItem}>
                        <Text style={styles.tabBarItemText}>My Reviews( 10 )</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.tabBarItem}>
                        <Text style={styles.tabBarItemText}>All Reviews( 123 )</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
            <ScrollView contentContainerStyle={styles.reviewsList}>
                {[1,2,3,4,5].map(element => {
                    return(
                    <View style={styles.reviewsItem}>
                        <View style={styles.reviewsItemHeader}>
                            <Image style={styles.reviewUserImage} source={require('../assets/images/italian.png')}></Image>
                            <View style={{flex:1}}>
                                <View style={styles.reviewUserDetails}>
                                    <View style={styles.reviewUserName}>
                                        <Text style={styles.reviewUserName_Name}>Cheryl Anandas</Text>
                                        <Text style={styles.reviewUserName_Count}>29 Reviews, 19 Followers</Text>
                                    </View>
                                    <TouchableOpacity style={styles.reviewUserFollowBtn}>
                                        <Text style={styles.reviewUserFollowBtn_Text}>Follow</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={styles.reviewUserMeta}>
                                    <View style={{...styles.reviewUserMetaItem,paddingLeft:0}}>
                                        <Text style={styles.reviewUserMetaItem_RatingText}>4.3</Text>
                                        <Icon style={styles.reviewUserMetaItem_RatingIcon} name="star" color="#ffffff"></Icon>
                                    </View>
                                    <View style={styles.reviewUserMetaItem}>
                                        <Icon style={styles.reviewUserMetaItem_PhotosIcon} name="share" color="#ffffff"></Icon>
                                        <Text style={styles.reviewUserMetaItem_PhotosText}>09</Text>
                                    </View>
                                    <View style={{...styles.reviewUserMetaItem,borderRightWidth:0}}>
                                        <Text style={styles.reviewUserMetaItem_Days}>2 weeks ago</Text>
                                    </View>
                                </View>
                            </View>
                        </View>
                        <View style={styles.reviewsItemContent}>
                            <Text style={styles.reviewsItemContent_Text}>
                                Lorem ipsum dolor sit amet, consectetur Lorem ipsum adipiscing sed do eiusmod tempor incididuntLorem ut labore et dolore magna aliqua...
                            </Text>
                            <TouchableOpacity style={styles.reviewsItemContent_ReadMore}>
                                <Text style={styles.reviewsItemContent_ReadMoreText}>READ MORE</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.reviewsItemImagesList}>
                            <Image style={styles.reviewsItemImage} source={require('../assets/images/italian.png')}></Image>
                            <Image style={styles.reviewsItemImage} source={require('../assets/images/italian.png')}></Image>
                            <Image style={styles.reviewsItemImage} source={require('../assets/images/italian.png')}></Image>
                            <ImageBackground imageStyle={{borderRadius:5}} style={styles.reviewsItemImage_add} source={require('../assets/images/italian.png')}>
                                <TouchableOpacity style={styles.reviewsItemImage_addBtn}>
                                    <Text style={styles.reviewsItemImage_addBtnText}>+ 06</Text>
                                </TouchableOpacity>
                            </ImageBackground>
                        </View>
                    </View>
                    )
                })}
            </ScrollView>
        </View>
        )
    }
}