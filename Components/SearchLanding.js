import React, { Component } from 'react';
import { Text, View, Image, Linking, ScrollView, TouchableOpacity, SafeAreaView, StyleSheet, Dimensions } from 'react-native';
import { reusableStyles } from '../Helpers/CommonStyles.js';
import { TextField } from 'react-native-material-textfield';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import iconmoonConfig from '../selection.json';
const Icon = createIconSetFromIcoMoon(iconmoonConfig);
import I18n from '../Helpers/i18n';
import { getLanguages } from 'react-native-i18n';
import ToastUtils from "../Helpers/ToastUtils";
import constants from "../Helpers/AppConstants";
import RequestHelper from '../Helpers/RequestHelper.js'
import NetworkHelper from '../Helpers/NetworkHelper.js'
import Utils from '../Helpers/Utils.js'
import Logo from '../assets/images/book-a-resto-logo-white.png';
import Slide from '../assets/images/slide1.jpg';
import BottomNav from './BottomNav/BottomNav';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { TextInput } from 'react-native-gesture-handler';

var themeRed = "#f01616",
    themeGrey = "#a3a3a3",
    themelightGrey = "#595959",
    lightRed = "#fdc9c9",
    fnt1 = 'OpenSans-Regular',
    fnt2 = 'OpenSans-Bold';
fnt3 = 'OpenSans-SemiBold';

const { width, height} = Dimensions.get('window');

const styles = StyleSheet.create({
    mainWrapper: {
        flex:1,
        justifyContent: 'flex-start',
    },
    header: {
        backgroundColor: themeRed,
        height:55,
        alignItems:'flex-start',
        justifyContent:'center',
        paddingHorizontal:20
    },
    headerTitleWrapper: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    headerTitle: {
        fontFamily: fnt2,
        fontSize: 16,
        fontWeight: 'bold',
        color: '#fff'
    },
    contentWrapper: {
        backgroundColor: '#ffefeb',
        paddingTop:20,
        paddingHorizontal: 20,
        paddingBottom: 50,
        flex: 1,
        justifyContent: 'flex-start'
    },
    scrollWrapper: {
        backgroundColor:'#ffffff',
        paddingHorizontal:20,
        paddingTop: 40,
        paddingBottom:80,
        borderRadius: 5,
        height:height - 100
    },
    fieldset:{
        marginBottom:30,
    },
    inputLabel:{
        fontFamily:fnt1,
        fontSize:12,
        marginBottom:2,
        textAlign:'left',
        color:'#000'
    },
    inputWrapper:{
        position:'relative',
        height:40,
    },
    inputIcon:{
        position:'absolute',
        zIndex:1,
        left:5,
        top:9,
        width:20,
        height:20,
        color:themeRed,
        fontSize:16
    },
    inputClear:{
        position:'absolute',
        zIndex:1,
        left:null,
        right:5,
        top:12,
        width:15,
        height:15,
        color:'#333333',
        fontSize:8,
        borderColor:'#333333',
        borderWidth:1,
        borderRadius:10,
        textAlign:'center',
        lineHeight:15
    },
    textInput:{
        borderBottomWidth:1,
        borderBottomColor:lightRed,
        width: width - 80,
        height:35,
        paddingLeft:25,
        paddingBottom:7,
    },
    searchText:{
        marginTop:8
    },
    fieldsetBtn:{
        // paddingBottom:80,
    },
    submitBtn:{
        width: width - 80,
        height:35,
        backgroundColor:themeRed,
        borderRadius:5,
        marginTop:30
    },
    submitBtnText:{
        color:'#fff',
        fontFamily: fnt2,
        textTransform:'uppercase',
        textAlign:'center',
        fontSize:12,
        lineHeight:35
    }
})
//Please make text inputs to Text
export default class SearchLanding extends React.Component {
    constructor() {
    super();
    this.state = {
        canShowCloseEdit : false,
        availableLocation : {},
        selectedRestaurantSearchKeyType : constants.all_restaurants
    };
    currentComponentSearchLanding = this
  }

    componentWillMount(){
        this.IS_RTL = this.props.navigation.getScreenProps().IS_RTL;
    }

    goToSearchTable(){
        this.props.navigation.navigate('SearchTable');
    }

    goToSearchRestaurants(){
        this.props.navigation.navigate('SearchRestaurants',{onRestaurantSearchTypeAvailable : this.onRestaurantSearchTypeAvailable});
    }

    goToSearchLocation(){
        this.props.navigation.navigate('SearchLocation',{ onLocationDetailsAvailable : this.onLocationDetailsAvailable});
    }

    submitPressed(){
        var otherDetails = {
            "guest_count": "2", "input_search": "","key":this.state.selectedRestaurantSearchKeyType,"section":"Single","slot_date":"2019-06-08"
        }
        this.props.navigation.navigate('RestaurantListing',{searchResult : this.state.availableLocation ,selectedCuisineOrArea : this.state.availableLocation , otherDetails : otherDetails})        
    }

    onRestaurantSearchTypeAvailable(type){
        currentComponentSearchLanding.setState({
            selectedRestaurantSearchKeyType : type
        })
    }

    onLocationDetailsAvailable(searchResult)
    {
        Utils.logData('onLocationDetailsAvailable SearchLanding '+JSON.stringify(searchResult))
        currentComponentSearchLanding.setState({
                availableLocation : searchResult,
                canShowCloseEdit : true
            })
    }

    render() {


        return (
            <View style={{...styles.mainWrapper}}>
                <View style={styles.header}>
                    <View style={styles.headerTitleWrapper}>
                        <Text style={styles.headerTitle}>Search</Text>
                    </View>
                </View>
                <View style={styles.contentWrapper}>
                    <ScrollView style={styles.scrollWrapper}>
                        <View style={styles.fieldset}>
                            <Text style={styles.inputLabel}>Restaurants or Cuisine</Text>
                            <TouchableOpacity style={styles.inputWrapper} 
                            onPress = {() => this.goToSearchRestaurants()} >
                                <View pointerEvents='none'>
                                    <Icon name="Search" color="#ffffff" style={[{...styles.inputIcon,left:this.IS_RTL ? -7 : 0}, {fontSize:26,marginTop:-5,marginLeft:-5}]}></Icon>
                                    {this.state.selectedRestaurantSearchKeyType == constants.all_restaurants ? 
                                    <View style={{...styles.textInput}}>
                                    <Text style={[styles.searchText,{textAlign:this.IS_RTL ? 'right' : 'left'}]}  numberOfLines = { 1 } >
                                        All Restaurants</Text></View> : <View style={{...styles.textInput}}>
                                    <Text style={[{textAlign:this.IS_RTL ? 'right' : 'left'}]}>My Favourites</Text></View>}
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.fieldset}>
                            <Text style={styles.inputLabel}>Location</Text>
                            <TouchableOpacity style={styles.inputWrapper} onPress = {() => this.goToSearchLocation()}>
                                <View pointerEvents='none'>
                                    <Icon name="map-pin" color="#ffffff" style={[{...styles.inputIcon,left:this.IS_RTL ? -7 : 0}, {fontSize:14,marginTop:2}]}></Icon>
                                    {this.state.availableLocation.description == undefined ? <View style={{...styles.textInput}}><Text style={[styles.searchText,{textAlign:this.IS_RTL ? 'right' : 'left'}]}  numberOfLines = { 1 } >Current location</Text></View> : <View style={{...styles.textInput}}><Text style={[styles.searchText,{textAlign:this.IS_RTL ? 'right' : 'left', width:'90%'}]}  numberOfLines = { 1 } >{this.state.availableLocation.description}</Text></View> }                                    
                                    {this.state.canShowCloseEdit ? <Icon name="close" color="#333333" style={{...styles.inputClear}}></Icon> : null}
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.fieldset}>
                            <Text style={styles.inputLabel}>People, Date and Time</Text>
                            <TouchableOpacity style={styles.inputWrapper} onPress = {() => this.goToSearchTable()}>
                                <View pointerEvents='none'>
                                    <Icon name="bookings" color="#ffffff" style={[{...styles.inputIcon,left:this.IS_RTL ? -7 : 0}, {fontSize:14,marginTop:2}]}></Icon>
                                    <View style={{...styles.textInput}}><Text style={[styles.searchText,{textAlign:this.IS_RTL ? 'right' : 'left'}]}  numberOfLines = { 1 } >Table for 2</Text></View>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.fieldsetBtn}>
                            <TouchableOpacity style={styles.submitBtn} onPress={() => this.submitPressed()}>
                                <Text style={styles.submitBtnText}>Submit</Text>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                </View>
                <BottomNav navigation = {this.props.navigation} viewType={2}></BottomNav>
            </View>
        )
    }
}