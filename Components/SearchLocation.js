import React, { Component } from 'react';
import { Text, View, Image, Linking, ScrollView, TouchableOpacity, SafeAreaView, StyleSheet, Dimensions,ListView,AsyncStorage,PermissionsAndroid,ToastAndroid } from 'react-native';
import { reusableStyles } from '../Helpers/CommonStyles.js';
import { TextField } from 'react-native-material-textfield';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import iconmoonConfig from '../selection.json';
const Icon = createIconSetFromIcoMoon(iconmoonConfig);
import I18n from '../Helpers/i18n';
import { getLanguages } from 'react-native-i18n';
import ToastUtils from "../Helpers/ToastUtils";
import Utils from "../Helpers/Utils";
import constants from "../Helpers/AppConstants";
import RequestHelper from '../Helpers/RequestHelper.js'
import NetworkHelper from '../Helpers/NetworkHelper.js'
import Logo from '../assets/images/book-a-resto-logo-white.png';
import Slide from '../assets/images/slide1.jpg';
import BottomNav from './BottomNav/BottomNav';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { TextInput } from 'react-native-gesture-handler';
import SearchResult from "../Models/SearchResult";
import Spinner from 'react-native-loading-spinner-overlay';
import Geolocation from 'react-native-geolocation-service';

var themeRed = "#f01616",
    themeGrey = "#a3a3a3",
    themelightGrey = "#595959",
    lightRed = "#f8a4a7",
    fnt1 = 'OpenSans-Regular',
    fnt2 = 'OpenSans-Bold';
fnt3 = 'OpenSans-SemiBold';

const { width, height} = Dimensions.get('window');

const styles = StyleSheet.create({
    mainWrapper: {
        flex:1,
        justifyContent: 'flex-start'
    },
    header: {
        backgroundColor: themeRed,
        height:55,
        alignItems:'flex-start',
        justifyContent:'center',
        paddingHorizontal:20
    },
    headerTitleWrapper: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    backBtn:{
        width: 20,
        height:20,
        marginTop: 10,
        marginRight: 10
    },
    backBtnIcon:{
        fontSize:20,
        lineHeight:20,
        color: '#fff',
    },
    inputWrapper:{
        position:'relative',
        marginLeft:15,
    },
    inputIcon:{
        position:'absolute',
        zIndex:1,
        left:5,
        top:8,
        fontSize:18,
        lineHeight:18,
        color:'#fff'
    },
    textInput:{
        borderBottomWidth:1,
        color:lightRed,
        borderBottomColor:lightRed,
        width: width - 80,
        height:35,
        paddingLeft:25,
        paddingBottom:7
    },
    contentWrapper: {
        backgroundColor: '#fefefe',
        paddingBottom: 50,
        flex: 1,
        justifyContent: 'flex-start'
    },
    currentLocationBtn:{
        flexDirection:'row',
        alignItems:'center',
        backgroundColor: '#ffffff',
        borderBottomWidth:1,
        borderBottomColor:'#f4f4f4',
        paddingHorizontal: 15,
        paddingVertical: 10,
    },
    currentLocationBtnIcon:{
        color:themeRed,
        width:20,
        height:20,
        marginRight: 10,
        marginTop:8
    },
    currentLocationBtnText:{
        fontFamily:fnt2,
        color:themeRed,
        textTransform:'uppercase',
        fontSize:10
    },
    previousSearchWrapper:{
        paddingVertical:20,
        paddingHorizontal: 20,
    },
    previousSearchLabel:{
        fontFamily:fnt1,
        fontSize:12,
        color:'#000'
    },
    previousSearchItem:{
        borderBottomWidth:1,
        borderBottomColor:'#f4f4f4',
        paddingVertical:12
    },
    previousSearchItemWrapper:{
        flexDirection:'row',
        alignItems:'center',
    },
    previousSearchItemIcon:{
        color:themeRed,
        width:20,
        height:20,
        marginRight: 10,
        marginTop:8
    },
    previousSearchItemText:{
        fontFamily:fnt1,
        fontSize:14,    
        color:'#000',
        width:'90%'
    }
})

export default class SearchLocation extends React.Component {
    
    constructor() {
    super();
    ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state = {
        showLoading : false,
        searchResultsdataSource: ds,
        location :{},
        selectedLocation : {}
    };
    currentComponentSearchLocation = this
  }

    componentWillMount(){
        this.IS_RTL = this.props.navigation.getScreenProps().IS_RTL;
        this.retrieveRecentSearchResultsIfAny()
    }

    getLocation = async () => {
    const hasLocationPermission = await currentComponentSearchLocation.hasLocationPermission();

    if (!hasLocationPermission) return;

    currentComponentSearchLocation.setState({ showLoading: true }, () => {
      Geolocation.getCurrentPosition(
        (position) => {


          Utils.logData('position.coords.latitude '+position.coords.latitude)
          Utils.logData('position.coords.longitude '+position.coords.longitude)
          currentComponentSearchLocation.setState({ showLoading: false
                      },currentComponentSearchLocation.getLocationDetails(position.coords.latitude,position.coords.longitude));
          Utils.logData('position '+JSON.stringify(position));
        },
        (error) => {
          if (Utils.isIOS()) {//iOS
                    if (error.code == 1){//User denied permission access

                    }
                    else if (error.code == 2){//Location disabled by user in device.

                    }
                }
                else//android
                {
                  if (error.code == 1){//User denied permission access

                    }
                    else if (error.code == 2){//Location disabled by user in device.

                    }
                    else if (error.code == 5){//{ message: 'Location settings are not satisfied.', code: 5 }

                    }  
                }
          currentComponentSearchLocation.setState({ location: error, showLoading: false });
          Utils.logData('error hereeeeere '+JSON.stringify(error));
        },
        { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000, distanceFilter: 50 }
      );
    });
  }

  hasLocationPermission = async () => {
    if (Utils.isIOS() ||
        (Utils.isIOS() == false && Utils.platformVersion() < 23)) {
      return true;
    }

    const hasPermission = await PermissionsAndroid.check(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
    );

    if (hasPermission) return true;

    const status = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
    );

    if (status === PermissionsAndroid.RESULTS.GRANTED) return true;

    if (status === PermissionsAndroid.RESULTS.DENIED) {
      ToastAndroid.show('Location permission denied by user.', ToastAndroid.LONG);
    } else if (status === PermissionsAndroid.RESULTS.NEVER_ASK_AGAIN) {
      ToastAndroid.show('Location permission revoked by user.', ToastAndroid.LONG);
    }

    return false;
  }

  getLocationDetails(latitude,longitude) {
    Utils.logData('this.latitude  '+latitude)
    Utils.logData('currentComponentSearchLocation latitude  '+longitude)

        var googleApiKey = constants.googleApiKey
        //url='https://maps.googleapis.com/maps/api/geocode/json?address='+ latitude + ',' +longitude + '&key=' +  googleApiKey
        
        var reqHelper = new RequestHelper()
        var requestUrl = reqHelper.getLocationDetails(latitude,longitude,googleApiKey)
        Utils.logData('requestUrl '+requestUrl)

        fetch(requestUrl)
        .then((response) => response.json())
        .then((responseJson) => {
            Utils.logData('responseJson '+JSON.stringify(responseJson))
            var users_formatted_address = Utils.getDisplayAddressFromGoogleApiResponse(responseJson)
            var place_id = responseJson.results[constants.indexOfComponentToGet].place_id
            Utils.logData('formatted address '+users_formatted_address)
            var selectedSearchResult = new SearchResult(users_formatted_address,place_id,latitude,longitude)
            currentComponentSearchLocation.setState({
                selectedLocation : selectedSearchResult
            },currentComponentSearchLocation.saveSelectedSearchResult(selectedSearchResult))
        });
    }

    searchLocation(text)
    {
        if (text == '') {
            this.retrieveRecentSearchResultsIfAny();
        }
        else{
            this.getAutoCompleteAddress(text)
        }
    }

    getAutoCompleteAddress(searchText) {
        var googleApiKey = constants.googleApiKey

        var reqHelper = new RequestHelper()
        var requestUrl = reqHelper.getAutoCompleteAddress(searchText,googleApiKey)
        
        Utils.logData('requestUrl '+requestUrl)
        fetch(requestUrl)
        .then((response) => response.json())
        .then((responseJson) => {
            Utils.logData('getAutoCompleteAddress responseJson '+JSON.stringify(responseJson))
            var searchArray = []
            for (var i = 0; i <responseJson.predictions.length ; i++) {
                var description = responseJson.predictions[i].description
                var place_id = responseJson.predictions[i].place_id
                var searchResult = new SearchResult(description,place_id,19,20)
                searchArray.push(searchResult)
            }
            currentComponentSearchLocation.setState({
                searchResultsdataSource : ds.cloneWithRows(searchArray)
            })            
        });
    }

    getLatLongFromPlaceId(placeId) {
        var googleApiKey = constants.googleApiKey

        var reqHelper = new RequestHelper()
        var requestUrl = reqHelper.getLatLongFromPlaceId(placeId,googleApiKey)
        
        Utils.logData('requestUrl '+requestUrl)
        fetch(requestUrl)
        .then((response) => response.json())
        .then((responseJson) => {
            Utils.logData('getLatLongFromPlaceId responseJson '+JSON.stringify(responseJson))
            var location = responseJson.result.geometry.location
            var latitude = location.lat
            var longitude = location.lng
            var strDescription = currentComponentSearchLocation.state.selectedLocation.description
            var searchData = new SearchResult(strDescription,placeId,latitude,longitude)
            Utils.logData('formatted address searchData '+JSON.stringify(searchData))
            currentComponentSearchLocation.setState({
                showLoading :false,
                selectedLocation : searchData
            },currentComponentSearchLocation.saveSelectedSearchResult(searchData))            
        });
    }

    saveSelectedSearchResult(searchResult){
        var searchResultsArray = []
        AsyncStorage.getItem('searchResults', (err, data) => {
        Utils.logData('data   '+JSON.stringify(data))   
        Utils.logData('err   '+JSON.stringify(err))
        if (data != null && data != undefined) {
            Utils.logData(0)
            var searchJSONObject = JSON.parse(data)
            if (searchJSONObject != null && searchJSONObject!= undefined) {
                Utils.logData(1)

                    //if already searched same address so no need to store it.
                    var canStoreSearchResult = true;
                    for (var i = 0; i < searchJSONObject.length; i++) 
                    {
                        if (searchJSONObject[i].place_id == searchResult.place_id) {
                            canStoreSearchResult = false;
                            break;
                        }
                    }
                    if (canStoreSearchResult == false){
                        return                        
                    } 



                if (searchJSONObject.length>=5) {
                    //remove first index search and add current search result at last
                    for (var i = 1; i < searchJSONObject.length; i++) 
                    {
                        searchResultsArray.push(searchJSONObject[i])
                    }
                    searchResultsArray.push(searchResult)
                    Utils.logData(2)
                }
                else
                {
                    //add current search result in last
                    Utils.logData(3)
                    for (var i = 0; i < searchJSONObject.length; i++) 
                    {
                        searchResultsArray.push(searchJSONObject[i])
                    }
                    searchResultsArray.push(searchResult)
                }
            }
            else
            {
                //create new search object and store
                Utils.logData(4)
                searchResultsArray.push(searchResult)
            }
        }
        else
        {
            //create new search object and store
            Utils.logData(5)
            searchResultsArray.push(searchResult)
            Utils.logData('searchResult searchResultsArray 5   '+JSON.stringify(searchResultsArray))  
        }
        Utils.logData('data before storing   '+JSON.stringify(searchResultsArray))   
        AsyncStorage.setItem('searchResults',JSON.stringify(searchResultsArray))
        })
        this.props.navigation.goBack()
        this.onLocationDetailsAvailable(searchResult)
    }

    setSelectedSearchResult(searchResult){  
        if (searchResult == null || searchResult == undefined || searchResult == '') {
            Utils.logData('searchResult not saved   '+JSON.stringify(searchResult)) 
            return
        } 
        Utils.logData('searchResult   '+JSON.stringify(searchResult)) 
        currentComponentSearchLocation.setState({
            showLoading : true,
            selectedLocation : searchResult
        },currentComponentSearchLocation.getLatLongFromPlaceId(searchResult.place_id))
    }

    retrieveRecentSearchResultsIfAny()
    {
        AsyncStorage.getItem('searchResults', (err, result) => {
            Utils.logData('data retrieved   '+JSON.stringify(result))   
            if (result != null && result != undefined) {
                var searchResultsArray = JSON.parse(result)
                currentComponentSearchLocation.setState({
                    searchResultsdataSource :ds.cloneWithRows(searchResultsArray)
                })
            }            
    })}

    onLocationDetailsAvailable(searchResult)
    {
        Utils.logData('SearchLocation error comp '+JSON.stringify(searchResult))
        Utils.logData('SearchLocation props '+JSON.stringify(this.props))
        this.props.navigation.state.params.onLocationDetailsAvailable(searchResult)
    }

    renderLocationRow(rowData)
    {
        return(
            <View style={styles.previousSearchItem}>
                            <TouchableOpacity onPress = {() => this.setSelectedSearchResult(rowData)} style={styles.previousSearchItemWrapper}>
                                <Icon name="map-pin" color="#ffffff" style={{...styles.previousSearchItemIcon}}></Icon>
                                <Text style={styles.previousSearchItemText} numberOfLines = { 1 }>{rowData.description}</Text>
                            </TouchableOpacity>
            </View>

            )
    }

    render() {
        var spinner = null
        if (this.state.showLoading) {
            spinner = <Spinner visible={this.state.showLoading} 
            //textContent={"Loading..."} 
            textStyle={reusableStyles.loaderStyle} />
        }
        return (
            <View style={{...styles.mainWrapper}}>
            {spinner}
                <View style={styles.header}>
                    <View style={styles.headerTitleWrapper}>
                        <TouchableOpacity style={styles.backBtn} onPress={() => this.props.navigation.goBack()}  style={{paddingVertical:10}}>
                            <Icon name="arrow-left" color="#ffffff" style={styles.backBtnIcon}></Icon>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.inputWrapper}>
                            <Icon name="map-pin" color={lightRed} style={{...styles.inputIcon,left:this.IS_RTL ? -7 : 0}}></Icon>
                            <TextInput value ={this.state.selectedLocation.description} placeholderTextColor={lightRed} placeholder={'Search Location'} style={{...styles.textInput,textAlign:this.IS_RTL ? 'right' : 'left'}}
                            onChangeText = {(text) => this.searchLocation(text)}></TextInput>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={styles.contentWrapper}>
                    <TouchableOpacity style={styles.currentLocationBtn} onPress = {() => this.getLocation()}>
                        <Icon name="map-pointer" style={{...styles.currentLocationBtnIcon}}></Icon>
                        <Text style={styles.currentLocationBtnText}>Current Location</Text>
                    </TouchableOpacity>
                    <View style={styles.previousSearchWrapper}>
                        <Text style={styles.previousSearchLabel}>Last Search</Text>
                        <ListView
                        removeClippedSubviews={false}
                        dataSource={currentComponentSearchLocation.state.searchResultsdataSource}
                        renderRow={(rowData) => this.renderLocationRow(rowData)}
                        />
                    </View>
                </View>
                {/* <BottomNav navigation = {this.props.navigation}></BottomNav> */}
            </View>
        )
    }
}