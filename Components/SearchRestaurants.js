import React, { Component } from 'react';
import { Text, View, Image, Linking, ScrollView, TouchableOpacity, SafeAreaView, StyleSheet, Dimensions,AsyncStorage,ListView } from 'react-native';
import { reusableStyles } from '../Helpers/CommonStyles.js';
import { TextField } from 'react-native-material-textfield';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import iconmoonConfig from '../selection.json';
const Icon = createIconSetFromIcoMoon(iconmoonConfig);
import I18n from '../Helpers/i18n';
import { getLanguages } from 'react-native-i18n';
import ToastUtils from "../Helpers/ToastUtils";
import constants from "../Helpers/AppConstants";
import RequestHelper from '../Helpers/RequestHelper.js'
import NetworkHelper from '../Helpers/NetworkHelper.js'
import Utils from '../Helpers/Utils.js'
import Logo from '../assets/images/book-a-resto-logo-white.png';
import Slide from '../assets/images/slide1.jpg';
import BottomNav from './BottomNav/BottomNav';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { TextInput } from 'react-native-gesture-handler';

var themeRed = "#f01616",
    themeGrey = "#a3a3a3",
    themelightGrey = "#595959",
    fnt1 = 'OpenSans-Regular',
    fnt2 = 'OpenSans-Bold',
    fnt3 = 'OpenSans-SemiBold',
    fnt4 = 'OpenSans-Light';

const { width, height} = Dimensions.get('window');

const styles = StyleSheet.create({
    mainWrapper: {
        flex:1,
        justifyContent: 'flex-start'
    },
    header: {
        backgroundColor: themeRed,
        height:55,
        alignItems:'flex-start',
        justifyContent:'center',
        paddingHorizontal:20
    },
    headerTitleWrapper: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    backBtn:{
        width: 20,
        height:20,
        marginTop: 10,
        marginRight: 10
    },
    backBtnIcon:{
        width: 18,
        height: 18,
        color: '#fff'
    },
    inputWrapper:{
        position:'relative'
    },
    inputIcon:{
        position:'absolute',
        zIndex:1,
        left:5,
        top:10,
        width:20,
        height:20,
        color:'#fff'
    },
    textInput:{
        borderBottomWidth:1,
        color:'#fff',
        borderBottomColor:'#fff',
        width: width - 80,
        height:35,
        paddingLeft:25,
        paddingBottom:7
    },
    contentWrapper: {
        backgroundColor: '#fefefe',
        paddingBottom: 50,
        flex: 1,
        justifyContent: 'flex-start'
    },
    topBar:{
        paddingTop:20,
        paddingHorizontal:20,
        backgroundColor:'#fff'
    },
    topBarItem:{
        flexDirection:'row',
        alignItems:'center',
        backgroundColor: '#ffffff',
        borderBottomWidth:1,
        borderBottomColor:'#f4f4f4',
        paddingVertical: 10,
    },
    topBarItemText:{
        fontFamily:fnt3,
        fontSize:14
    },
    previousSearchWrapper:{
        paddingVertical: 30,
        paddingHorizontal: 20,
    },
    previousSearchLabel:{
        fontFamily:fnt1,
        fontSize:12,
    },
    previousSearchItem:{
        borderBottomWidth:1,
        borderBottomColor:'#f4f4f4',
        paddingVertical:12
    },
    previousSearchItemWrapper:{
        flexDirection:'row',
        alignItems:'center',
    },
    previousSearchItemImage:{
        width:28,
        height:28,
        marginRight:7,
        borderRadius: 5,
    },
    previousSearchItemDetails:{

    },
    previousSearchItemName:{
        fontFamily:fnt3,
        fontSize:14,
        marginBottom: 3,
    },
    previousSearchItemAddress:{
        fontFamily:fnt1,
        fontWeight: '300',
        fontSize:12,
        marginBottom: 3,
    }
})

export default class SearchRestaurants extends React.Component {
    
    constructor() {
    super();
    ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state = {
        recentRestaurantsDataSource: ds,
        canShowMyFavourites : false
    };
    currentComponentSearchRestaurants = this
  }


    componentWillMount(){
        this.IS_RTL = this.props.navigation.getScreenProps().IS_RTL;
        this.canShowMyFavourites()
        this.retrieveRecentRestaurantsIfAny()
    }

    selectAndGoBack(selectedValue){
        Utils.logData('selectedValue '+selectedValue)
        this.props.navigation.state.params.onRestaurantSearchTypeAvailable(selectedValue)
        this.props.navigation.goBack()
    }
    selectRestaurantAndProceed(restaurant){
        Utils.logData('selected restaurant '+JSON.stringify(restaurant))
        this.props.navigation.navigate('RestaurantDetails', { 'restaurant': restaurant })
    }

    canShowMyFavourites(){
        AsyncStorage.getItem("userId").then((userId) => {
            console.log("User Id ", userId);
            if(userId)
            {
                currentComponentSearchRestaurants.setState({
                    canShowMyFavourites :true
                })
            }
            }).done();
    }

    retrieveRecentRestaurantsIfAny()
    {
        AsyncStorage.getItem('recentRestaurants', (err, result) => {
            Utils.logData('recentRestaurants retrieved   '+JSON.stringify(result))   
            if (result != null && result != undefined) {
                var searchedRestaurantsArray = JSON.parse(result)
                currentComponentSearchRestaurants.setState({
                     recentRestaurantsDataSource :ds.cloneWithRows(searchedRestaurantsArray)
                })
            }            
    })}

    renderRecentSearchRestaurant(restaurant)
    {
        return(
        <View style={styles.previousSearchItem}>
            <TouchableOpacity style={styles.previousSearchItemWrapper} onPress = {() => this.selectRestaurantAndProceed(restaurant)}>
                <Image source={{ uri: restaurant.cover_image }} style={styles.previousSearchItemImage}></Image>
                    <View style={styles.previousSearchItemDetails}>
                        <Text style={styles.previousSearchItemName}>{restaurant.name}</Text>
                        <Text style={styles.previousSearchItemAddress}>{restaurant.restaurant_address}</Text>
                    </View>
            </TouchableOpacity>
        </View>)
    }

    render() {
        return (
            <View style={{...styles.mainWrapper}}>
                <View style={styles.header}>
                    <View style={styles.headerTitleWrapper}>
                        <TouchableOpacity style={styles.backBtn} onPress={() => this.props.navigation.goBack()}  style={{paddingVertical:10}}>
                            <Icon name="arrow-left" color="#ffffff" style={styles.backBtnIcon}></Icon>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.inputWrapper}>
                            <Icon name="map-pin" color="#ffffff" style={{...styles.inputIcon,left:this.IS_RTL ? -7 : 0}}></Icon>
                            <TextInput placeholderTextColor={'#fff'} placeholder={'Search Restaurants or Cuisines'} style={{...styles.textInput,textAlign:this.IS_RTL ? 'right' : 'left'}}></TextInput>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={styles.contentWrapper}>
                    <View style={styles.topBar}>
                        <TouchableOpacity style={styles.topBarItem} onPress = {() => this.selectAndGoBack(constants.all_restaurants)}>
                            <Text style={styles.topBarItemText}>All Restaurant</Text>
                        </TouchableOpacity>
                        {this.state.canShowMyFavourites ? <TouchableOpacity style={styles.topBarItem} onPress = {() => this.selectAndGoBack(constants.favourites)}>
                            <Text style={styles.topBarItemText}>My Favourites</Text>
                        </TouchableOpacity> : null}
                    </View>
                    <View style={styles.previousSearchWrapper}>
                        <Text style={styles.previousSearchLabel}>Last Search restaurants or cuisines</Text>
                        <ListView
                        removeClippedSubviews={false}
                        dataSource={currentComponentSearchRestaurants.state.recentRestaurantsDataSource}
                        renderRow={(rowData) => this.renderRecentSearchRestaurant(rowData)}
                        />                        
                    </View>
                </View>
                <BottomNav navigation = {this.props.navigation}></BottomNav>
            </View>
        )
    }
}