import React, { Component } from 'react';
import { Text, View, Image, Linking, ScrollView, TouchableOpacity, SafeAreaView, StyleSheet, Dimensions } from 'react-native';
import { reusableStyles } from '../Helpers/CommonStyles.js';
import { TextField } from 'react-native-material-textfield';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import iconmoonConfig from '../selection.json';
const Icon = createIconSetFromIcoMoon(iconmoonConfig);
import I18n from '../Helpers/i18n';
import { getLanguages } from 'react-native-i18n';
import ToastUtils from "../Helpers/ToastUtils";
import constants from "../Helpers/AppConstants";
import RequestHelper from '../Helpers/RequestHelper.js'
import NetworkHelper from '../Helpers/NetworkHelper.js'
import Logo from '../assets/images/book-a-resto-logo-white.png';
import Slide from '../assets/images/slide1.jpg';
import BottomNav from './BottomNav/BottomNav';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { TextInput } from 'react-native-gesture-handler';
import Picker from '../lib/react-native-wheel-picker';
import Moment from 'moment';

var themeRed = "#f01616",
    themeGrey = "#a3a3a3",
    themelightGrey = "#595959",
    fnt1 = 'OpenSans-Regular',
    fnt2 = 'OpenSans-Bold';
fnt3 = 'OpenSans-SemiBold';

const { width, height} = Dimensions.get('window');

const styles = StyleSheet.create({
    mainWrapper: {
        flex:1,
        justifyContent: 'flex-start'
    },
    header: {
        backgroundColor: themeRed,
        height:55,
        alignItems:'flex-start',
        justifyContent:'center',
        paddingHorizontal:20
    },
    headerTitleWrapper: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    headerTitleText:{
        fontFamily:fnt2,
        fontSize: 14,
        color:'#ffffff'
    },
    backBtn:{
        width: 20,
        height:20,
        marginTop: 10,
        marginRight: 10
    },
    backBtnIcon:{
        width: 18,
        height: 18,
        color: '#fff'
    },
    contentWrapper: {
        backgroundColor: '#fefefe',
        paddingBottom: 50,
        flex: 1,
        justifyContent: 'flex-start'
    },
    pickerWrapper:{
        flexDirection:'row',
        justifyContent:'space-between'
    },
    pickerItem:{
        width: 120,
        height:200,
    },
    pickerItemText:{
        fontFamily: fnt3,
        fontSize: 14,
        color:'#000000', 
    },
    submitBtn:{
        paddingVertical:10,
        backgroundColor:themeRed
    },
    submitBtnText:{
        color:'#fff',
        fontFamily:fnt2,
        textAlign:'center',
        textTransform:'uppercase'
    }
})

var PickerItem = Picker.Item;

export default class SearchTable extends React.Component {

    constructor(props){
        super(props);
        dayListArray = [];
        const date = new Date();

        var currentHours = Moment(date).format("HH");
        var currentMinutes = Moment(date).format("mm");
        var modValue = parseInt(currentMinutes) % 15;
        var startTime = currentHours + ":" + currentMinutes;
        if (modValue !== 0){
            var newMinutes = parseInt(currentMinutes) + (15 - modValue);
            startTime = currentHours + ":" + newMinutes;
        }
        console.log("Current Time ", startTime);

        var timeSlots = this.getTimeSlots(startTime, '23:45');
        console.log('timeStops ', timeSlots);

        for (i = 0; i < 365; i++){
            var newDate = Moment(date, "DD-MM-YYYY").add('days', i);
            var formattedDate = newDate.format("ddd, DD MMM");
            if (i == 0){
                dayListArray.push({displayDate : "today", actualDate: formattedDate});
            }
            else if (i == 1){
                dayListArray.push({displayDate : "tomorrow", actualDate: formattedDate});
            }
            else{
                dayListArray.push({displayDate : formattedDate, actualDate: formattedDate});
            }
        }
        this.state = {
            peopleList : ['1 People', '2 People', '3 People', '4 People', '5 People', '6 People', '7 People', '8 People', '9 People', '10 People', '11 People', '12 People', '13 People', '14 People', '15 People', '16 People', '17 People', '18 People', '19 People', '20 People'],
            selectedPeople : '2 People',
            dayList: dayListArray,
            selectedDay : 'Today',
            timeList : timeSlots,
        };
    }

    getTimeSlots(start, end){
        var startTime = Moment(start, 'HH:mm');
        var endTime = Moment(end, 'HH:mm');
  
        if(endTime.isBefore(startTime) ){
            endTime.add(1, 'day');
        } 

        var timeSlots = [];

        while(startTime <= endTime){
            timeSlots.push(new Moment(startTime).format('HH:mm'));
            startTime.add(15, 'minutes');
        }
        return timeSlots;
    }


	onPickerSelect (index) {
		this.setState({
			selectedItem: index,
		})
	}
    

    componentWillMount(){
        this.IS_RTL = this.props.navigation.getScreenProps().IS_RTL;
    }

    render() {
        return (
            <View style={{...styles.mainWrapper}}>
                <View style={styles.header}>
                    <View style={styles.headerTitleWrapper}>
                        <TouchableOpacity style={styles.backBtn} onPress={() => this.props.navigation.goBack()}>
                            <Icon name="arrow-left" color="#ffffff" style={styles.backBtnIcon}></Icon>
                        </TouchableOpacity>
                        <Text style={styles.headerTitleText}>Table for 2, Today</Text>
                    </View>
                </View>
                <View style={styles.contentWrapper}>
                    <View style={styles.pickerWrapper}>
                        <Picker style={styles.pickerItem}
                            onValueChange={(index) => this.onPickerSelect(index)}
                            selectedValue={this.state.selectedPeople}
                            itemStyle={styles.pickerItemText}
                            lineColor="#000000" //to set top and bottom line color (Without gradients)
				            lineGradientColorFrom="#008000" //to set top and bottom starting gradient line color
				            lineGradientColorTo="#FF5733" //to set top and bottom ending gradient line color                    
                            >
                                {this.state.peopleList.map((value, i) => (
                                    <PickerItem label={value} value={i} key={i} />
                                ))}
                        </Picker>
                        <Picker style={styles.pickerItem}
                            selectedValue={this.state.selectedDay}
                            itemStyle={styles.pickerItemText}
                            >
                                {this.state.dayList.map((value, i) => (
                                    <PickerItem style={{color:'#ff0000'}} label={value.displayDate} value={i} key={i} />
                                ))}
                        </Picker>
                        <Picker style={styles.pickerItem}
                            selectedValue={this.state.selectedTime}
                            itemStyle={styles.pickerItemText}
                            >
                                {this.state.timeList.map((value, i) => (
                                    <PickerItem label={value} value={i} key={i} />
                                ))}
                        </Picker>
                    </View>
                    <TouchableOpacity style={styles.submitBtn}>
                        <Text style={styles.submitBtnText}>Done</Text>
                    </TouchableOpacity>
                </View>
                <BottomNav navigation = {this.props.navigation}></BottomNav>
            </View>
        )
    }
}