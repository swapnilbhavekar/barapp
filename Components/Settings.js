import React, { Component } from 'react';
import { Text, View, ScrollView, TouchableOpacity, SafeAreaView, StyleSheet, Button } from 'react-native';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import iconmoonConfig from '../selection.json';
const Icon = createIconSetFromIcoMoon(iconmoonConfig);
import I18n from '../Helpers/i18n';
import { getLanguages } from 'react-native-i18n';
import BottomNav from './BottomNav/BottomNav';
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';

var themeRed = "#f01616",
    themeGrey = "#a3a3a3",
    themelightGrey = "#595959",
    fnt1 = 'OpenSans-Regular',
    fnt2 = 'OpenSans-Bold';
fnt3 = 'OpenSans-SemiBold';

const IS_RTL = false;

const styles = StyleSheet.create({
    mainWrapper: {
        flex: 1,
        justifyContent: 'flex-start'
    },
    header: {
        backgroundColor: themeRed,
        height: 55,
        alignItems: 'flex-start',
        justifyContent: 'center',
        paddingHorizontal: 20
    },
    headerTitleWrapper: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    headerTitle: {
        fontFamily: fnt2,
        fontSize: 16,
        fontWeight: 'bold',
        color: '#fff'
    },
    settingElement: {
        flex: 1,
        flexDirection: 'row',
        paddingVertical: 30,
        borderBottomColor: '#ddd',
        borderBottomWidth: 1,
    },
    settingElementIcon: {
        fontSize: 24,
        lineHeight: 24
    },
    settingElementHeading: {
        fontFamily: fnt2,
        fontSize: 16,
        lineHeight: 16,
        color: '#000',
    },
    settingElementDetails: {
        fontFamily: fnt1,
        fontSize: 12,
        lineHeight: 12,
        color: '#000',
        marginTop: 7,
    },
    contentWrapper: {
        backgroundColor: '#fff',
        paddingTop: 20,
        paddingHorizontal: 20,
        paddingBottom: 50,
        flex: 1,
        alignItems: 'flex-start',
    },
    component: {
        marginTop: 10,
    },
    backIcon: {
        fontSize:18,
        lineHeight:18
    },
})


var radio_props = [
    { label: 'English', value: 0 },
    { label: 'Arabic', value: 1 }
];

export default class Settings extends React.Component {
    state = {
        isRTL: false,
        types: 0,
        types3: [{ label: 'English', value: 0 }, { label: 'Arabic', value: 1 }],
        value3: 0,
        value3Index: 0,
    }

    render() {
        const IS_RTL = this.state.isRTL;
        return (
            <View style={{ ...styles.mainWrapper, direction: IS_RTL ? 'rtl' : 'ltr' }}>
                <View style={styles.header}>
                    <View style={styles.headerTitleWrapper}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Signup')} style={{ paddingVertical: 10 }}>
                            <Icon name="arrow-left" color="#ffffff" style={[styles.backIcon,{ marginRight: 10 }]}></Icon>
                        </TouchableOpacity>
                        <Text style={styles.headerTitle}>Settings</Text>
                    </View>
                </View>
                <ScrollView style={{ height: '100%', width: '100%' }}>
                    <View style={styles.contentWrapper}>
                        <TouchableOpacity style={styles.settingElement}>
                            <View style={{ flex: 1.5, justifyContent: 'center', alignItems: 'center' }}>
                                <Icon name="user" color={themeRed} style={styles.settingElementIcon}></Icon>
                            </View>
                            <View style={{ flex: 7 }}>
                                <Text style={styles.settingElementHeading}>
                                    Account Setting
                                </Text>
                                <Text style={styles.settingElementDetails}>
                                    Manage your account details
                                </Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.settingElement}>
                            <View style={{ flex: 1.5, justifyContent: 'center', alignItems: 'center' }}>
                                <Icon name="card" color={themeRed} style={styles.settingElementIcon}></Icon>
                            </View>
                            <View style={{ flex: 7 }}>
                                <Text style={styles.settingElementHeading}>
                                    Account Setting
                                </Text>
                                <Text style={styles.settingElementDetails}>
                                    Setup your account to allow paying with this app
                                </Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.settingElement}>
                            <View style={{ flex: 1.5, justifyContent: 'center', alignItems: 'center' }}>
                                <Icon name="phone" color={themeRed} style={styles.settingElementIcon}></Icon>
                            </View>
                            <View style={{ flex: 7 }}>
                                <Text style={styles.settingElementHeading}>
                                    Communications
                                </Text>
                                <Text style={styles.settingElementDetails}>
                                    Email, Texts and Notifications
                                </Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.settingElement}>
                            <View style={{ flex: 1.5, justifyContent: 'center', alignItems: 'center' }}>
                                <Icon name="map" color={themeRed} style={styles.settingElementIcon}></Icon>
                            </View>
                            <View style={{ flex: 7 }}>
                                <Text style={styles.settingElementHeading}>
                                    Distance Units
                                </Text>
                                <Text style={styles.settingElementDetails}>
                                    Automatic
                                </Text>
                            </View>
                        </TouchableOpacity>
                        <View style={[styles.settingElement, { borderBottomWidth: 0 }]}>
                            <View style={{ flex: 1.5, justifyContent: 'center', alignItems: 'center' }}>
                                <Icon name="language" color={themeRed} style={styles.settingElementIcon}></Icon>
                            </View>
                            <View style={{ flex: 7 }}>
                                <Text style={styles.settingElementHeading}>
                                    Select Language
                                </Text>
                                <View style={styles.component}>
                                    <RadioForm formHorizontal={true} animation={true} >
                                        {this.state.types3.map((obj, i) => {
                                            var onPress = (value, index) => {
                                                this.setState({
                                                    value3: value,
                                                    value3Index: index
                                                })
                                            }
                                            return (
                                                <RadioButton labelHorizontal={true} key={i} >
                                                    {/*  You can set RadioButtonLabel before RadioButtonInput */}
                                                    <RadioButtonInput
                                                        obj={obj}
                                                        index={i}
                                                        isSelected={this.state.value3Index === i}
                                                        onPress={onPress}
                                                        borderWidth={5}
                                                        buttonInnerColor={'#fff'}
                                                        buttonOuterColor={this.state.value3Index === i ? themeRed : '#ddd'}
                                                        buttonSize={5}
                                                        buttonOuterSize={20}
                                                        buttonWrapStyle={{ marginLeft: 10 }}
                                                    />
                                                    <RadioButtonLabel
                                                        obj={obj}
                                                        index={i}
                                                        onPress={onPress}
                                                        labelStyle={{ marginLeft: 10 }}
                                                        labelWrapStyle={{}}
                                                    />
                                                </RadioButton>
                                            )
                                        })}
                                    </RadioForm>
                                </View>
                            </View>
                        </View>

                    </View>
                </ScrollView>
                <BottomNav navigation={this.props.navigation}></BottomNav>
            </View>
        )
    }
}