import React, { Component } from 'react';
import { Text, View, Image, Linking, ImageBackground, TouchableOpacity, SafeAreaView, Platform, Dimensions, Keyboard } from 'react-native';
import { reusableStyles } from '../Helpers/CommonStyles.js'
import SwitchElement from '../ReusableComponents/SwitchElement.js'
import { TextField } from 'react-native-material-textfield';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import iconmoonConfig from '../selection.json';
const Icon = createIconSetFromIcoMoon(iconmoonConfig);
import I18n from '../Helpers/i18n';
import { getLanguages } from 'react-native-i18n';
import RequestHelper from '../Helpers/RequestHelper.js';
import NetworkHelper from '../Helpers/NetworkHelper.js';
import ToastUtils from '../Helpers/ToastUtils';
import constants from '../Helpers/AppConstants';
import Validator from '../Helpers/Validator.js';
import PhoneInputElement from './PhoneInputElement';
import Spinner from 'react-native-loading-spinner-overlay';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

export default class Signup extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            lastname: '',
            mobileno: '',
            email: '',
            password: '',
            showLoading: false,
            switch1Value: false,
            scrollViewHeight:0,
            actualScrollViewHeight:0
        }
        currentComponentSignUp = this;
    }

    toggleSwitch1 = (value) => {
        this.setState({ switch1Value: value })
        console.log('Switch 1 is: ' + value)
    }

    onPressContinueButton() {
        let errors = {};


        if (!Validator.isValidString(currentComponentSignUp.state.name)) {
            errors["name"] = constants.empty_name_error;
            //ToastUtils.showErrorToast(constants.empty_name_error);
            //return;
        }

        if (!Validator.isValidString(currentComponentSignUp.state.lastname)) {
            errors["lastname"] = constants.empty_last_name_error;
            //ToastUtils.showErrorToast(constants.empty_last_name_error);
            //return;
        }

        if (!Validator.isValidString(currentComponentSignUp.state.mobileno)) {
            errors["mobileno"] = constants.empty_mobile_no_error;
            //ToastUtils.showErrorToast(constants.empty_mobile_no_error);
            //return;
        }

        if (!Validator.isValidString(currentComponentSignUp.state.email)) {
            errors["email"] = constants.empty_email_id_error;
            //ToastUtils.showErrorToast(constants.empty_email_id_error);
            //return;
        }
        else if (!Validator.isValidEmailString(currentComponentSignUp.state.email)) {
            errors["email"] = constants.invalid_email_id_error;
            //ToastUtils.showErrorToast(constants.invalid_email_id_error);
            //return;
        }

        if (!Validator.isValidString(currentComponentSignUp.state.password)) {
            errors["password"] = constants.empty_password_error;
            //ToastUtils.showErrorToast(constants.empty_password_error);
            //return;
        }

        this.setState({ errors });

        if (Object.keys(errors).length === 0){
            currentComponentSignUp.checkUserDetails();
            this.setState({ scrollViewHeight: this.state.actualScrollViewHeight});
        }else{
            let errorLabelHeight = 35 * Object.keys(errors).length;
            this.setState({ scrollViewHeight: (this.state.actualScrollViewHeight + errorLabelHeight) });
        }
    }

    checkUserDetails() {
        currentComponentSignUp.setState({
            showLoading: true,
        })
        console.log('Check User Request');
        var reqHelper = new RequestHelper();
        var requestUrl = reqHelper.checkUserDetails();
        var networkHelper = new NetworkHelper();
        var requestParams = { "Email": this.state.email, "Mobile": this.state.mobileno };
        networkHelper.setUrl(requestUrl);
        networkHelper.setMethod(constants.methodPost);
        networkHelper.setData(JSON.stringify(requestParams));
        networkHelper.execute(function (responseData) {
            console.log("Check User Details Response", responseData);
            if (responseData.IsSuccess == true) {
                //ToastUtils.showSuccessToast(responseData.Message);
                currentComponentSignUp.generateOTP();
            }
            else {
                currentComponentSignUp.setState({
                    showLoading: false,
                })
                ToastUtils.showErrorToast(responseData.Message);
            }

        }, function (errorMessage, statusCode) {
            currentComponentSignUp.setState({
                showLoading: false,
            })
            console.log(errorMessage);
            ToastUtils.showErrorToast(constants.somethingwent_wrong_msg);
        }, function () {
            currentComponentSignUp.setState({
                showLoading: false,
            })
            ToastUtils.showErrorToast(constants.no_network_msg);
        });
    }

    generateOTP() {
        console.log('logging sagar')
        var reqHelper = new RequestHelper();
        var requestUrl = reqHelper.generateOTPRequest();
        var networkHelper = new NetworkHelper();
        var requestParams = { "mobile": this.state.mobileno, "source": "registerUser" };
        networkHelper.setUrl(requestUrl);
        networkHelper.setMethod(constants.methodPost);
        networkHelper.setData(JSON.stringify(requestParams));

        networkHelper.execute(function (responseData) {
            currentComponentSignUp.setState({
                showLoading: false,
            })
            console.log("Generate OTP Response", responseData);
            if (responseData.IsSuccess == true) {
                ToastUtils.showSuccessToast(constants.otp_sent_msg);
                var userParams = {"Firstname": currentComponentSignUp.state.name, "Lastname": currentComponentSignUp.state.lastname, "Email": currentComponentSignUp.state.email, "Password": currentComponentSignUp.state.password, "Mobile": currentComponentSignUp.state.mobileno, "alert": currentComponentSignUp.state.switch1Value};
                currentComponentSignUp.props.navigation.navigate('VerificationCode', { 'mobileNo': currentComponentSignUp.state.mobileno, 'source': 'registerUser', 'generatedOTP': responseData.OTP, 'userParams': userParams});
            }
            else {
                ToastUtils.showErrorToast(responseData.Message);
            }
        }, function (errorMessage, statusCode) {
            currentComponentSignUp.setState({
                showLoading: false,
            })
            console.log(errorMessage);
            ToastUtils.showErrorToast(constants.somethingwent_wrong_msg);
        }, function () {
            currentComponentSignUp.setState({
                showLoading: false,
            })
            ToastUtils.showErrorToast(constants.no_network_msg);
        });
    }

    componentWillMount(){
        this.keyboardOpen = false;        
    }

    onLayout(event) {
        if (!this.keyboardOpen) {
            console.log("keyboard opened");
            var windowHeight = Dimensions.get('window').height;
            const { x, y, height, width } = event.nativeEvent.layout;
            console.log("Opened " + height +" <= Height");
            svHeight = height - (Platform.OS === 'ios' ? 70 : 0);
            this.setState({ scrollViewHeight: svHeight, actualScrollViewHeight: svHeight });
        }
        this.keyboardOpen = true;
    }

    render() {
        let { name } = this.state;
        let { lastname } = this.state;
        let { mobileno } = this.state;
        let { email } = this.state;
        let { password } = this.state;
        let { errors = {} } = this.state;

        var spinner = null
        if (this.state.showLoading) {
            spinner = <Spinner visible={this.state.showLoading} 
            //textContent={"Loading..."} 
            textStyle={reusableStyles.loaderStyle} />
        }

        return (
            <ImageBackground source={require('../assets/images/background-table.jpg')}
                style={reusableStyles.imageBackground} >
                {spinner}
                <SafeAreaView style={reusableStyles.droidSafeArea} >
                    <View style={reusableStyles.container} >
                        <View style={reusableStyles.headerBox}>
                            <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={{paddingVertical:10}}>
                                <Icon name="arrow-left" color="#f01616" style={reusableStyles.backBtnIcon}></Icon>
                            </TouchableOpacity>
                            <View style={reusableStyles.logoContainer}>
                                <Image source={require('../assets/images/book-a-resto-logo.png')}
                                    style={reusableStyles.logo}
                                />
                            </View>
                        </View>
                        <View style={[reusableStyles.formBox]} 
                        onLayout={(event) => this.onLayout(event)} >
                            <KeyboardAwareScrollView keyboardShouldPersistTaps='always' 
                            //onContentSizeChange={(event) => this.updateLayout(event)}
                            contentContainerStyle={{ height: this.state.scrollViewHeight }}
                            ref='scrollBox'>
                                <View style={reusableStyles.scrollingBox}>
                                    <View>
                                        <TextField 
                                            label={I18n.t('signUpNameText')} 
                                            value={name}
                                            onChangeText={(name) => this.setState({ name })}
                                            tintColor={inputColor}
                                            error={errors.name}
                                            containerStyle={[reusableStyles.textField, reusableStyles.w100]} />
                                        <TextField 
                                            label={I18n.t('signUpLastNameText')} 
                                            value={lastname}
                                            onChangeText={(lastname) => this.setState({ lastname })}
                                            tintColor={inputColor}
                                            error={errors.lastname}
                                            containerStyle={[reusableStyles.textField, reusableStyles.w100]} />
                                        <View style={{ width: '100%', height: 80}}>
                                            <View style={{ flex: 1, flexDirection: 'row', height: 150, }}>
                                                <View style={{ width: '32%', marginTop: 35 }}>
                                                    <PhoneInputElement />
                                                </View>
                                                <View style={{ width: '60%' }}>
                                                    <TextField 
                                                        label={I18n.t('signUpMobileNoText')} 
                                                        value={mobileno}
                                                        onChangeText={(mobileno) => this.setState({ mobileno })}
                                                        keyboardType="number-pad"
                                                        tintColor={inputColor}
                                                        error={errors.mobileno}
                                                        containerStyle={[reusableStyles.textField, reusableStyles.w100]} />
                                                </View>
                                            </View>
                                        </View>
                                        <View style={[reusableStyles.switchContainer]}>
                                            <View style={reusableStyles.switchDetails}>
                                                <Text style={reusableStyles.switchDetailsText}>
                                                    {I18n.t('bookingAlertsText')}
                                                </Text>
                                            </View>
                                            <View style={reusableStyles.switchBx}>
                                                <SwitchElement toggleSwitch1={this.toggleSwitch1} switch1Value={this.state.switch1Value} style={reusableStyles.switchElement} />
                                            </View>
                                        </View>
                                        <TextField 
                                            label={I18n.t('signUpEmailText')} 
                                            value={email}
                                            onChangeText={(email) => this.setState({ email })}
                                            tintColor={inputColor}
                                            error={errors.email}
                                            containerStyle={[reusableStyles.textField, reusableStyles.w100]} />
                                        <TextField 
                                            label={I18n.t('signUpPasswordText')} 
                                            value={password}
                                            secureTextEntry={true}
                                            onChangeText={(password) => this.setState({ password })}
                                            tintColor={inputColor}
                                            error={errors.password}
                                            containerStyle={[reusableStyles.textField, reusableStyles.w100]} />
                                    </View>
                                    <View>
                                        <View style={[reusableStyles.termAndConditionContainer]} >
                                            <Text style={reusableStyles.termAndCondition}>
                                                {I18n.t('termsandconditionsText')}<Text style={reusableStyles.redLinkSmall} onPress={() => { Linking.openURL('http://www.example.com/') }} > {I18n.t('termsofuserandconditionsText')} </Text>
                                                {I18n.t('termsandconditionsandText')} <Text style={reusableStyles.redLinkSmall} onPress={() => { Linking.openURL('http://www.example.com/') }} >{I18n.t('privacyPolicyText')}</Text>
                                            </Text>
                                        </View>
                                        <View style={reusableStyles.bottomBtnContainer}>
                                            <TouchableOpacity style={reusableStyles.themeBtn}
                                                onPress={() => this.onPressContinueButton()}>
                                                <Text style={reusableStyles.themeBtnTxt} > {I18n.t('signupBtnText')} </Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>
                            </KeyboardAwareScrollView>
                        </View>
                    </View>
                </SafeAreaView>
            </ImageBackground>
        )
    }
}