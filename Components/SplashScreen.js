import React, { Component } from 'react';
import { AsyncStorage,View,Image} from 'react-native';
import Validator from '../Helpers/Validator.js'
import LoggedInUserData from '../Helpers/LoggedInUserData'
import {StackActions, NavigationActions } from 'react-navigation';


export default class SplashScreen extends React.Component {

  constructor(props)
   {
    super(props);
    currentComponentSplashScreen = this;
  }

  componentWillMount()
  {
    currentComponentSplashScreen.checkIfUserDetailsStoredOrNot()
  }

  checkIfUserDetailsStoredOrNot()
  {
   AsyncStorage.getItem('userData', (err, userDataString) =>
       {
          if (userDataString !=null) {
            var userDataObject = JSON.parse(userDataString); 
            console.log('userDataObject '+JSON.stringify(userDataObject))
            if (userDataObject !=null) {
                  if (Validator.isValidString(userDataObject.user_id)) 
                    {
                      LoggedInUserData.getInstance().setAppData(userDataObject)
                      currentComponentSplashScreen.navigateToHomeScreen()
                      return
                    }
                    else
                    {
                      currentComponentSplashScreen.navigateToHomeScreen()
                    }
            }
            else
                {
                  currentComponentSplashScreen.navigateToHomeScreen()
                }
          } 
          else
              {
                currentComponentSplashScreen.navigateToHomeScreen()
              }       
          
      })
  } 
    navigateToLoginScreen()
    {
      const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: 'Login' })],
      });        
      setTimeout(function(){ 
          currentComponentSplashScreen.props.navigation.dispatch(resetAction);}, 
          1000);
    }


    navigateToHomeScreen()
    {
      const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: 'Home' })],
      });
      setTimeout(function(){ 
          currentComponentSplashScreen.props.navigation.dispatch(resetAction);}, 
          1000);
    }


  render() {
    return (
      <View  style={{justifyContent: 'center',
              alignItems: 'center',flex : 1}}>
            <Image source={require('../assets/images/splash_screen.gif')} style={{width: '100%',
        height: '100%',}} />
      </View>
    );
  }
}
