import React, { Component } from 'react';
import { Text, View, Image, Platform, ImageBackground, TouchableOpacity, SafeAreaView, Dimensions, AsyncStorage } from 'react-native';
import { reusableStyles } from '../Helpers/CommonStyles.js';
import { reusableArStyles } from '../Helpers/CommonArStyles.js';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import iconmoonConfig from '../selection.json';
const Icon = createIconSetFromIcoMoon(iconmoonConfig);
import I18n from '../Helpers/i18n';
import { getLanguages } from 'react-native-i18n';
import Utils from "../Helpers/Utils";
import RequestHelper from '../Helpers/RequestHelper.js';
import NetworkHelper from '../Helpers/NetworkHelper.js';
import ToastUtils from "../Helpers/ToastUtils";
import constants from "../Helpers/AppConstants";
import Spinner from 'react-native-loading-spinner-overlay';
import OtpInputs from 'react-native-otp-inputs';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

export default class VerificationCode extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            showLoading: false,
            otpCode: '',
            scrollViewHeight: 0,
            otpErrorVisible: false

        };
        currentComponentVerifyCode = this;
        mobileNo = this.props.navigation.state.params.mobileNo;
        otpSource = this.props.navigation.state.params.source;
        generatedOTP = this.props.navigation.state.params.generatedOTP;
        registerUserParams = this.props.navigation.state.params.userParams;
    }

    onPressResendButton() {
        currentComponentVerifyCode.generateOTP();
    }

    onPressSubmitButton() {
        console.log("OTP length - ", currentComponentVerifyCode.state.otpCode.length);
        if (currentComponentVerifyCode.state.otpCode.length < 5){
            currentComponentVerifyCode.setState({
                otpErrorVisible: true
            })
            //ToastUtils.showErrorToast(constants.otp_invalid);
        }
        else{
            currentComponentVerifyCode.setState({
                otpErrorVisible: false
            })
            currentComponentVerifyCode.verifyOTP();
        }
    }

    verifyOTP() {
        console.log("OTP Code = ", currentComponentVerifyCode.state.otpCode);
        currentComponentVerifyCode.setState({
            showLoading: true,
        })
        console.log('generate OTP')
        var reqHelper = new RequestHelper();
        var requestUrl = reqHelper.verifyOTPRequest();
        var networkHelper = new NetworkHelper();
        var requestParams = { "mobile": mobileNo, "otp": currentComponentVerifyCode.state.otpCode };
        networkHelper.setUrl(requestUrl);
        networkHelper.setMethod(constants.methodPost);
        networkHelper.setData(JSON.stringify(requestParams));

        networkHelper.execute(function (responseData) {
            currentComponentVerifyCode.setState({
                showLoading: false,
            })
            console.log("Verify OTP Response", responseData);
            if (responseData.IsSuccess == true) {
                ToastUtils.showSuccessToast(responseData.Message);
                if (otpSource == "registerUser") {
                    currentComponentVerifyCode.registerUser();
                }
                else {
                    if (responseData.Data.length > 0){
                        currentComponentVerifyCode.props.navigation.navigate('ResetPassword', { 'mobileNo': mobileNo, 'custId': responseData.Data[0].customer_id});
                    }
                }
            }
            else {
                ToastUtils.showErrorToast(responseData.Message);
            }
        }, function (errorMessage, statusCode) {
            currentComponentVerifyCode.setState({
                showLoading: false,
            })
            console.log(errorMessage);
            ToastUtils.showErrorToast(constants.somethingwent_wrong_msg);
        }, function () {
            currentComponentVerifyCode.setState({
                showLoading: false,
            })
            ToastUtils.showErrorToast(constants.no_network_msg);
        });
    }

    registerUser() {
        currentComponentVerifyCode.setState({
            showLoading: true,
        })
        console.log('Register User Request');
        var reqHelper = new RequestHelper();
        var requestUrl = reqHelper.registerUserRequest();
        var networkHelper = new NetworkHelper();
        console.log("Register User Params ", registerUserParams);
        console.log("FirstName ", registerUserParams.Firstname);
        var requestParams = { "Firstname": registerUserParams["Firstname"], "Lastname": registerUserParams["Lastname"], "Email": registerUserParams["Email"], "Password": registerUserParams["Password"], "Mobile": registerUserParams["Mobile"], "alert": registerUserParams["alert"] };
        networkHelper.setUrl(requestUrl);
        networkHelper.setMethod(constants.methodPost);
        networkHelper.setData(JSON.stringify(requestParams));
        networkHelper.execute(function (responseData) {
            console.log("Registration Response", responseData);
            if (responseData.IsSuccess == true) {
                AsyncStorage.setItem('userId', JSON.stringify(responseData.data));
                AsyncStorage.setItem('emailId', registerUserParams["Email"]);
                AsyncStorage.setItem('mobileNo', registerUserParams["Mobile"]);
                AsyncStorage.setItem('name', registerUserParams["Firstname"] + " " + registerUserParams["Lastname"]);

                ToastUtils.showSuccessToast(responseData.Message);
                currentComponentVerifyCode.props.navigation.navigate('Home');
            }
            else {
                currentComponentVerifyCode.setState({
                    showLoading: false,
                })
                ToastUtils.showErrorToast(responseData.Message);
            }

        }, function (errorMessage, statusCode) {
            currentComponentVerifyCode.setState({
                showLoading: false,
            })
            console.log(errorMessage);
            ToastUtils.showErrorToast(constants.somethingwent_wrong_msg);
        }, function () {
            currentComponentVerifyCode.setState({
                showLoading: false,
            })
            ToastUtils.showErrorToast(constants.no_network_msg);
        });
    }

    generateOTP() {
        currentComponentVerifyCode.setState({
            showLoading: true,
        })
        console.log('Generate OTP Request')
        var reqHelper = new RequestHelper();
        var requestUrl = reqHelper.generateOTPRequest();
        var networkHelper = new NetworkHelper();
        var requestParams = { "mobile": mobileNo, "source": otpSource };
        networkHelper.setUrl(requestUrl);
        networkHelper.setMethod(constants.methodPost);
        networkHelper.setData(JSON.stringify(requestParams));

        networkHelper.execute(function (responseData) {
            currentComponentVerifyCode.setState({
                showLoading: false,
            })
            console.log("Generate OTP Response", responseData);
            if (responseData.IsSuccess == true) {
                ToastUtils.showSuccessToast(constants.otp_sent_msg);
                currentComponentVerifyCode.generatedOTP = responseData.OTP;
            }
            else {
                ToastUtils.showErrorToast(responseData.Message);
            }
        }, function (errorMessage, statusCode) {
            currentComponentVerifyCode.setState({
                showLoading: false,
            })
            console.log(errorMessage);
            ToastUtils.showErrorToast(constants.somethingwent_wrong_msg);
        }, function () {
            currentComponentVerifyCode.setState({
                showLoading: false,
            })
            ToastUtils.showErrorToast(constants.no_network_msg);
        });
    }

    componentDidMount() {
        Utils.logData('props logData ' + JSON.stringify(this.props))
        const navigation = this.props.navigation;
        const userName = navigation.getParam('userName', 'NO-NAME-FOUND');
        Utils.logData('props logData userName ' + userName)
        getLanguages().then(languages => {
            console.log(languages + "verfication page"); // ['en-US', 'en']
            if (languages[0] == "en-US") {
                console.log("Verification code " + reusableStyles.redLinkSmall);
            }
            else if (languages[0] != "en-US") {
                console.log("Its Arabic" + reusableArStyles.themeBtn);
                reusableStyles.themeBtn = reusableArStyles.themeBtn;
                reusableStyles.textField = reusableArStyles.textField;
                reusableStyles.redLinkSmall = reusableArStyles.redLinkSmall;
                reusableStyles.socialLoginElementText = reusableArStyles.socialLoginElementText;
                reusableStyles.infoBox = reusableArStyles.infoBox;
                reusableStyles.infoHeading = reusableArStyles.infoHeading;
                reusableStyles.infoDetails = reusableArStyles.infoDetails;
            }
        });
    }


    componentWillMount() {
        this.keyboardOpen = false;
    }

    onLayout(event) {
        if (!this.keyboardOpen) {
            var windowHeight = Dimensions.get('window').height;
            const { x, y, height, width } = event.nativeEvent.layout;
            svHeight = height - (Platform.OS === 'ios' ? 70 : 0);
            this.setState({ scrollViewHeight: svHeight })
            console.log(height);
        }
        this.keyboardOpen = true;
    }

    render() {
        var spinner = null
        if (this.state.showLoading) {
            spinner = <Spinner visible={this.state.showLoading} 
            //textContent={"Loading..."} 
            textStyle={reusableStyles.loaderStyle} />
        }
        console.log("Generated OTP ", generatedOTP);
        return (
            <ImageBackground source={require('../assets/images/background-table.jpg')}
                style={reusableStyles.imageBackground} >
                {spinner}
                <SafeAreaView style={reusableStyles.droidSafeArea} >
                    <View style={reusableStyles.container} >
                        <View style={reusableStyles.headerBox}>
                            <TouchableOpacity onPress={() => this.props.navigation.goBack()}  style={{paddingVertical:10}}>
                                <Icon name="arrow-left" color="#f01616" style={reusableStyles.backBtnIcon}></Icon>
                            </TouchableOpacity>
                            <View style={reusableStyles.logoContainer}>
                                <Image source={require('../assets/images/book-a-resto-logo.png')}
                                    style={reusableStyles.logo}
                                />
                            </View>
                        </View>

                        <View style={reusableStyles.formBox}
                            onLayout={(event) => this.onLayout(event)} >
                            <KeyboardAwareScrollView keyboardShouldPersistTaps='always' contentContainerStyle={{ height: this.state.scrollViewHeight }}>
                                <View style={reusableStyles.scrollingBox}>
                                    <View>
                                        <View style={reusableStyles.infoBox}>
                                            <Text style={reusableStyles.infoHeading}>{I18n.t('verficationHeadingText')}</Text>
                                            <Text style={reusableStyles.infoDetails}>{I18n.t('verficationInfoText')}</Text>
                                            <Text style={reusableStyles.infoHeading}>+971 {mobileNo}</Text>
                                        </View>
                                        <View style={reusableStyles.fourBoxContainer}>
                                            <OtpInputs handleChange={code => currentComponentVerifyCode.setState({ otpCode: code })} secureTextEntry={true}
                                             numberOfInputs={5} focusedBorderColor="#f01616" /> 
                                        </View>
                                        <Text style={{marginTop:35, fontSize:10, color:'red', marginLeft:10}}>{(this.state.otpErrorVisible == true) ? constants.otp_invalid : ""}</Text>
                                        <View style={{marginTop:50}}>
                                            <Text> OTP -> {generatedOTP}
                                            </Text>
                                        </View>
                                    </View>
                                    <View style={reusableStyles.bottomBtnContainer}>
                                        <TouchableOpacity style={[reusableStyles.themeBtn, reusableStyles.mB10]}
                                            onPress={() => this.onPressSubmitButton()}
                                        >
                                            <Text style={reusableStyles.themeBtnTxt} > {I18n.t('verficationSubmitBtn')} </Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity style={[reusableStyles.themeLink]}
                                            onPress={() => this.onPressResendButton()}>
                                            <Text style={reusableStyles.themeLinkTxt} >{I18n.t('verficationResendOtpBtn')}</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </KeyboardAwareScrollView>
                        </View>
                    </View>
                </SafeAreaView>
            </ImageBackground>
        )
    }
}