import React, { Component } from 'react';
import { Text, View, Image, StyleSheet, TouchableOpacity, ScrollView, SafeAreaView, UIManager , Dimensions } from 'react-native';
import { reusableStyles } from '../../Helpers/CommonStyles.js';
import Slide from '../../assets/images/miniSlide.png';
import Carousel from 'react-native-snap-carousel';
import RestaurantByType from "../../Models/RestaurantByType";
import I18n from '../../Helpers/i18n';
import Utils from "../../Helpers/Utils.js";


var themeRed = "#f01616",
themeGrey="#a3a3a3",
themelightGrey="#595959",
fnt1='OpenSans-Regular',
fnt2='OpenSans-Bold';
fnt3='OpenSans-SemiBold';

const { width, height} = Dimensions.get('window');

const slideWidth = 120;

const IS_RTL = true;

const styles = StyleSheet.create({
    card: {
        paddingVertical: 20,
        paddingLeft: 20,
        backgroundColor: '#ffffff',
        width: width - 40,
        marginBottom: 20,
        borderRadius: 5,
        justifyContent:'flex-start',
        alignItems:'flex-start'
    },
    cardTitle: {
        fontFamily: fnt2,
        fontSize: 16,
        fontWeight: 'bold',
        marginBottom:10,
        color:'#000'
    },
    slideCatTitle:{
        fontFamily: fnt2,
        fontSize: 12,
        marginTop: 10,
        color:'#000',
        textAlign : IS_RTL ? 'left' : 'right'
    },
    slideCatCount:{
        fontFamily: fnt1,
        fontSize: 12,
        textAlign : IS_RTL ? 'left' : 'right'
    },
    slideViewMore: {
        fontFamily: fnt2,
        marginTop: 20,
        color: '#ed1c24',
        textTransform: 'uppercase'
    }
});

export default class SectionCarousel extends React.Component {
    state = {
        data :[],
        titleText:'',
    }
    constructor(props)
    {
        super(props);
        currentComponentSectionCarousel = this;
    }
    componentWillMount()
    {
        Utils.logData("props === "+JSON.stringify(this.props));
        if (RestaurantByType.CUISINES ==this.props.restaurantByType) {
            this.setState({
                titleText : I18n.t('homeBrowseByCuisineHeading')
            })
        }
        else if (RestaurantByType.AREAS ==this.props.restaurantByType) {
            this.setState({
                titleText : I18n.t('homeBrowseByAreasHeading')
            })
        }
        if (this.props.data != undefined) {
            this.setState({
                data : this.props.data
            })
        }
    }
    goToViewMore()
    {
        var searchResult = this.props.searchResult
        this.props.navigation.push('BrowseCuisines',{restaurantByType : this.props.restaurantByType,searchResult : searchResult})
    }

    goToRestaurantListing(item)
    {
        var searchResult = this.props.searchResult
        var otherDetails = {
            "guest_count": "2", "input_search": "","key":"default_search","section":"Single","slot_date":"2019-06-08"
        }
        this.props.navigation.navigate('RestaurantListing',{searchResult : searchResult ,selectedCuisineOrArea : item , otherDetails : otherDetails})
    }

    _renderItem({ item, index }) {
        Utils.logData('item data in section courasel '+JSON.stringify(item))
        return (
            <View>
                <TouchableOpacity onPress = {() => currentComponentSectionCarousel.goToRestaurantListing(item)}>
                    <Image style={{width: 100, height: 50,borderRadius:5}} source={{uri:item.image}}></Image>
                    <Text style={styles.slideCatTitle}>{item.name}</Text>
                    <Text style={styles.slideCatCount}>{item.restaurant_count + I18n.t('homeCarouselCountWord')}</Text>
                </TouchableOpacity>
            </View>
        );
    }

    render() {
        return (
            <SafeAreaView style={reusableStyles.droidSafeArea} >
                <View style={styles.card}>
                    <Text style={styles.cardTitle}>{this.state.titleText}</Text>
                    <Carousel
                        ref={(c) => { this._carousel = c; }}
                        data={this.state.data}
                        renderItem={this._renderItem}
                        sliderWidth={width - 60}
                        itemWidth={slideWidth + 5}
                        inactiveSlideOpacity={1}
                        inactiveSlideScale={1}
                        activeSlideAlignment='start'
                        contentContainerCustomStyle={{
                            overflow:'hidden',
                            width: (slideWidth * this.state.data.length) + 30
                        }}
                    />
                    <TouchableOpacity  onPress={() => this.goToViewMore() }>
                        <Text style={styles.slideViewMore}>{I18n.t('homeCarouselViewMoreText')}</Text>
                    </TouchableOpacity>
                </View>
            </SafeAreaView>
        )
    }
}