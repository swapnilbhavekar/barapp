const constants = {
	googleApiKey : 'AIzaSyAfXVo5RN43UZp0Y4I1SYA2831HcdSl2xs',
	deviceTypeiOS : 'iOS',
	deviceTypeAndroid : 'android',

	deviceType : 'deviceType',
	deviceId : 'deviceId',
	deviceToken : 'deviceToken',

	compressImageMaxWidth : 900,
	compressImageMaxHeight : 900,
	compressImageQuality : 0.72,
	
	success : 200,
	
	status        :"status",	
	methodGet : 'get',
	methodPost : 'post',
	methodDelete : 'delete',

	//below are the constants for service call success response
	user_logged_in_msg : 'User Logged in successfully!',
	
	//below are the constants for service call error response
	somethingwent_wrong_msg : 'Something went wrong',
	no_network_msg : 'Please check your internet connection',

	//below are the constants for errors on input fields
	invalid_credential_error : 'Invalid Credentials',

	empty_name_error : 'Name cannot be empty',

	empty_last_name_error : 'Last Name cannot be empty',

	empty_mobile_no_error : 'Mobile Number cannot be empty',

	empty_email_id_error : 'Email Id cannot be empty',

	invalid_email_id_error : 'Invalid email Id',

	empty_password_error : 'Password cannot be empty',

	otp_sent_msg : 'OTP has been sent to your mobile number',

	otp_invalid : 'Please enter valid 5 digit OTP',

	added_to_favourites : 'Restaurant added to favourites',

	favourites :'favourites',

	default_search : 'default_search',

	all_restaurants : 'all_restaurants',

	Ambience :'Ambience',

	Foods : 'Food',

	All : 'Other',

	indexOfComponentToGet : 0

}
export default constants;