import Utils from "./Utils";
import constants from "./AppConstants";
import RequestHelper from "./RequestHelper";
import NetworkHelper from "./NetworkHelper";

export default class CommonNetworkCalls{
	constructor() {
    }

    addRemoveToFavourites(requestParams,successHandler)
    {
    	var reqHelper = new RequestHelper()
        var requestUrl = reqHelper.addRemoveToFavourites()
        var networkHelper = new NetworkHelper();
        networkHelper.setUrl(requestUrl);
        networkHelper.setMethod(constants.methodPost);
        networkHelper.setData(JSON.stringify(requestParams));

        networkHelper.execute(function (responseData) 
        {
            Utils.logData("addRemoveToFavourites Response "+JSON.stringify(responseData));
            successHandler();
        }, function (errorMessage, statusCode) 
        {
        	Utils.logData("addRemoveToFavourites error Response "+statusCode);
        	ToastUtils.showErrorToast(constants.somethingwent_wrong_msg);
        }, function()
            {
            	Utils.logData("addRemoveToFavourites Response no internet");
            	ToastUtils.showErrorToast(constants.no_network_msg);
            });
    }

    getRestaurants(requestParams,successHandler, errorHandler, networkHandler) {
        var reqHelper = new RequestHelper()
        var requestUrl = reqHelper.getRestaurants()
        var networkHelper = new NetworkHelper();
        networkHelper.setUrl(requestUrl);
        networkHelper.setMethod(constants.methodPost);
        networkHelper.setData(JSON.stringify(requestParams));
        networkHelper.execute(function (responseData) 
        {
            Utils.logData("getRestaurants Response "+JSON.stringify(responseData));
            successHandler(responseData);
        }, function (errorMessage, statusCode) 
        {
            Utils.logData("getRestaurants error Response "+statusCode);
            errorHandler(errorMessage, statusCode)
        }, function()
            {
                Utils.logData("getRestaurants Response no internet");
                networkHandler() 
            });
    }
}