import React from "react";
import { StyleSheet } from "react-native";

var themeRed = "#f01616",
themeGrey="#a3a3a3",
themelightGrey="#595959",
fnt1='OpenSans-Regular',
fnt2='OpenSans-Bold';
fnt3='OpenSans-SemiBold';
inputColor='rgba(247,0,0,0.4)';
export const reusableStyles = StyleSheet.create({
    map: {
        ...StyleSheet.absoluteFillObject,
    },
    mapContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    mB10:{
        marginBottom:10
    },
    mT10:{
        marginTop:10
    },
    imageBackground: {
        width: '100%',
        height: '100%'
    },
    droidSafeArea: {
        justifyContent: 'center',
        alignItems: 'flex-start',
        flex: 1,
        width: '100%',
    },
    container: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
        backgroundColor: '#fff',
        width: '90%',
        margin:'5%',
        borderRadius: 10,
        paddingTop:20,
        paddingLeft:20,
        paddingRight:20,
        paddingBottom:0,
    },
    headerBox:{
        position:'relative',
        width:'100%',
        height:60,
    },
    backBtnIcon:{
        fontSize:18,
        lineHeight:18,
        color:'#f01616'
    },
    logoContainer:{
       flex:5,
       alignSelf:'center',
       marginTop:-30,
    },
    logo: {
    },
    formBox:{
        flex:40,
        width:'100%',
    },
    bottomBtnContainer:{
        width:'100%',
        // flex:2,
    },
    themeBtn: {
        backgroundColor: themeRed,
        textAlign: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        height:35,
        color: '#fff',
        width: '100%',
        fontFamily:fnt1,
    },
    themeBtnTxt: {
        color: '#fff',
        fontSize: 12,
        lineHeight:20,
        fontFamily:fnt2,
        textTransform: 'uppercase'
    },
    themeLink:{
        backgroundColor:'#fff',
        textAlign: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        height:35,
        color: '#fff',
        width: '100%',
        fontFamily:fnt1,
    },
    themeLinkTxt: {
        color:themeRed,
        fontSize: 12,
        lineHeight:20,
        fontFamily:fnt2,
    },
    termAndConditionContainer:{
        //flex:2,
        marginBottom:10
    },
    termAndCondition:{
        color: themeGrey,
        fontSize: 12,
        lineHeight:16,
        fontFamily: fnt1,
    },
    redLinkSmall:{
        color: themeRed,
        fontSize: 12,
        lineHeight:12,
        fontFamily:fnt2,
    },
    redLinkSmallTxt:{
        color: themeRed,
        fontSize: 12,
        fontFamily:fnt2,
    },
    redLinkBig:{
        color: themeRed,
        fontSize: 12,
        fontFamily:fnt2,
    },
    textField:{
        color: themeRed,
        fontSize: 14,
        lineHeight:14,
        fontFamily:fnt1,
        color:'#000',
        // backgroundColor:'grey',
        //borderBottomColor:'rgba(244,22,22,0.20)',
        marginBottom:5
    },
    bsTextField:{
        color: themeRed,
        fontSize: 14,
        fontFamily:fnt1,
        borderBottomColor:'rgba(244,22,22,0.20)',
        borderBottomWidth:1,
        marginBottom:10
    },
    w100:{
        width:'100%'
    },
    switchContainer:{
        flex:1,
        flexDirection: 'row',
        alignItems:'flex-start',
        justifyContent:'flex-start',
        marginBottom:20,
    },
    switchDetails:{
        flex:5,
        height:20
    },
    switchDetailsText:{
        color:'#000',
        fontSize: 10,
        fontFamily: fnt2,
        marginTop:5,
    },
    switchBx:{
        flex:1    
    },
    switchElement:{
        marginTop:5,
    },
    infoBox:{
        //flex:2,
        marginTop:15,
        width:'75%',
    },
    infoHeading:{
        fontSize: 12,
        lineHeight:12,
        fontFamily: fnt2,
        marginBottom:10,
        color:themelightGrey,
    },
    infoDetails:{
        color:themelightGrey,
        fontSize:12,
        lineHeight:18,
        fontFamily: fnt1,
        marginBottom:10,
    },
    fourBoxContainer:{
        // flex:1,
        flexDirection: 'row',
        marginTop:50,
        width:'100%'
    },
    fourBoxElement:{
        flex:1,
        margin:10,
        textAlign:'center'
    },
    separatorBox:{
        marginTop:35,
        marginBottom:35
    },
    separatorTextColor:{
        fontFamily:fnt2,
        alignSelf:'center',
        paddingHorizontal:5,
        color:themeGrey
    },
    socialLoginBox:{
        // flex:1,
        backgroundColor:'#fff',
        // shadowColor: "#000000",
        // shadowOpacity: 0.8,
        // shadowRadius: 0,
    },
    socialLoginElement:{
        // flex:1,
        flexDirection: 'row',
        marginBottom:20,
        height:35,
        alignItems:'center',
        backgroundColor:'#fff',
        // elevation:4,
        // shadowOffset: { width: 5, height: 5 },
        // shadowColor: "grey",
        // shadowOpacity: 0.5,
        // shadowRadius: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        elevation: 2,
        borderRadius:5,
        margin:5,
        padding:5
    },
    socialIcon:{
        fontSize:24,
        lineHeight:24,
        alignSelf:'center',
        justifyContent:'center',
        marginLeft:15,
        marginRight:15,
        width:40,
        borderRightColor:'#e8e8e8',
        borderRightWidth:1,
    },

    socialLoginElementText:{
        fontSize:12,
        lineHeight:12,
        fontFamily:fnt3,
        color:'#000',
    },

    mainWrapper: {
        flex: 1,
        justifyContent: 'flex-start'
    },
    header: {
        backgroundColor: themeRed,
        paddingHorizontal: 20,
        paddingVertical:30,
        // padding: 20,
        // paddingTop: 70,
    },
    headerTitleWrapper: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    headerTitle: {
        fontFamily: fnt2,
        fontSize: 16,
        fontWeight: 'bold',
        color: '#fff'
    },
    contentWrapper: {
        backgroundColor: '#ffefeb',
        paddingTop: 20,
        paddingHorizontal: 20,
        paddingBottom: 50,
        flex: 1,
        justifyContent: 'flex-start'
    },
    scrollWrapper: {
        backgroundColor: '#ffffff',
        flexDirection: 'row',
        justifyContent: 'space-between',
        flexWrap: 'wrap',
        flex: 1,
        paddingHorizontal: 20,
        paddingVertical: 30,
        borderRadius:0
    },
    listItem: {
        width:'100%',
        marginBottom:15,
        paddingBottom:15,
        borderBottomColor:'#dfdfdf',
        borderBottomWidth:1
    },
    lastListItem: {
        width:'100%',
        marginBottom:15,
        paddingBottom:15,
        borderBottomColor:'#dfdfdf',
        borderBottomWidth:0
    },
    hasPoints:{
        paddingBottom:25,
    },
    btmBrdrNone:{
        borderBottomWidth:0
    },
    restaurantItem:{
        height:71,
        flex:1,
        flexDirection: 'column', 
        alignItems: 'flex-end', 
        justifyContent: 'space-between',
        paddingTop:5,
        paddingBottom:5,
        borderRadius:10,
        marginBottom:5
    },
    restaurantHeading:{
        fontSize:14,
        lineHeight:14,
        color:'#000',
        fontFamily:fnt3,
        marginTop:10,
        marginBottom:5
    },
    restaurantHeading2:{
        fontSize:12,
        lineHeight:12,
        color:'#000',
        fontFamily:fnt1,
        marginBottom:5
    },
    details:{
        flex: 1, 
        flexDirection:'row',
        width:'100%',
    },
    detailsText:{
        fontSize:10,
        lineHeight:10,
        color:'#000',
        fontFamily:fnt1,
    },
    brdrRight:{
        paddingRight:10,
        borderRightColor:'#ddd',
        borderRightWidth:1,
        marginRight:10
    },
    ratingHeart:{
        fontSize:10,
        paddingRight:5,
    },
    ratingTextContainer:{
        borderLeftColor:'rgba(255,255,255,0.5)',
        borderLeftWidth:1
    },
    ratingText:{
        fontSize:10,
        lineHeight:10,
        color:'#fff',
        fontFamily:fnt3,
        marginRight:5,
        marginLeft:5,
    },
    btnsContainer:{
        flex:1,
        flexDirection:'row',
        justifyContent:'space-between',
    },
    redThumbnailBtn:{
        backgroundColor:themeRed,
        padding:5,
        borderRadius:5,
        width:65,
        height:25,
        marginTop:10, 
        position:'relative'
    },
    redThumbnailBtnText:{
        color:'#fff',
        fontSize:10,
        fontFamily:fnt1,
        flex:1,
        justifyContent:'center',
        alignSelf:'center'
    },
    pointsLink:{
        color:themeRed,
        fontSize:10,
        fontFamily:fnt1,
        position:'absolute',
        top:30,
        left:12,
    },
    scrollingBox:{
        flex:1,
        justifyContent:'space-between',
        height:'100%',
        width:'100%',
    },
    filterIconWrapper: {
        paddingHorizontal: 15,
    },
    timingWrapper: {
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    timingToggleWrapper: {
        marginRight: 10,
        marginBottom: 10
    },
    timingToggle: {
        paddingVertical: 5,
        paddingHorizontal: 10,
        backgroundColor: '#fff',
        borderColor: themeRed,
        borderWidth: 1,
        borderRadius: 5
    },
    activeTimingToggle: {
        backgroundColor: themeRed,
        borderColor: '#fff',
    },
    timingText: {
        color: themeRed,
        fontFamily: fnt1,
        fontSize: 10
    },
    activeTimingText:{
        color:'#fff'
    },
    timingOfferText: {
        color: themeRed,
        fontSize: 10,
        marginTop: 3,
        paddingLeft: 7
    },
});