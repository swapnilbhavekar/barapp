import React from "react";
import config from '../BuildConfig.js';

export function getRestBaseURL() {
   return {
      'baseUrl': config.baseUrl,
  }
}

var Urls = {
    learnMoreUrl : 'https://www.facebook.com',
    termsOfUseUrl : 'https://www.facebook.com',
    privacyPolicyUrl : 'https://www.facebook.com',
    loginUserRequest: getRestBaseURL().baseUrl+"Users/dologinApp",
    registerUserRequest: getRestBaseURL().baseUrl+"Users/CustomerRegistrationApp",
    generateOTPRequest: getRestBaseURL().baseUrl+"Users/generate_otp",
    verifyOTPRequest: getRestBaseURL().baseUrl+"Users/verify_otp",
    restPasswordRequest: getRestBaseURL().baseUrl+"Users/forgot_password",
    getHomePageAreaAndCuisines: getRestBaseURL().baseUrl+"Get_details/HomePageDetailsApp",
    getBrowserByCuisines: getRestBaseURL().baseUrl+"Get_details/BrowseByCuisinesApp",
    getBrowserByAreas: getRestBaseURL().baseUrl+"Get_details/BrowseByAreaApp",
    getRestaurants: getRestBaseURL().baseUrl+"Get_details/get_restaurants_App",    
    checkUserDetails: getRestBaseURL().baseUrl+"Users/check_user_App",
    addRemoveToFavourites: getRestBaseURL().baseUrl+"Users/favourites",
    getFavourites: getRestBaseURL().baseUrl+"Users/get_favourites_App",
    getLocationDetails:'https://maps.googleapis.com/maps/api/geocode/json?address=latitude,longitude&key=googleApiKey',
    getAutoCompleteAddress:'https://maps.googleapis.com/maps/api/place/autocomplete/json?input=SEARCHTEXT&key=googleApiKey',
    getLatLongFromPlaceId:'https://maps.googleapis.com/maps/api/place/details/json?placeid=PLACE_ID&fields=name,formatted_address,geometry&key=googleApiKey',
    getRestaurantDetails: getRestBaseURL().baseUrl+'Get_details/Get_Restaurant_by_id_App',
    getRestaurantPhotosByType: getRestBaseURL().baseUrl+'Get_details/Get_Photos_by_type_App'
}

export { Urls as default };