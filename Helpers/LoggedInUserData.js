'use strict';

var React = require('React');

var instance = (function() {

  var obj;
  var appData = null
  function LoggedInUserData() {

    if(obj){
      return obj;
    }

    obj = this;
    this.setAppData = function(appData){
      this.appData = appData
    }
  }
    LoggedInUserData.getInstance = function(){
    return obj || new LoggedInUserData();
  }
  return LoggedInUserData;
}());

module.exports = instance;
