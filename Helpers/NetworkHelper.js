let instance = null;
import Utils from "./Utils";
import constants from "./AppConstants";
import { NetInfo } from 'react-native';


export default class NetworkHelper{
  constructor() {
        if(!instance){
              instance = this;
        }

        this.url = "";
        this.method = "";
        this.data = "";
        this.contentType = "application/json";
        return instance;
    }

    setUrl(url){
        this.url = url;
    }
    setMethod(method){
        this.method = method;
    }
    setData(data){
        this.data = data;
    }
    setContentType(contentType){
        this.contentType = contentType;
      }
    execute(successHandler, errorHandler, networkHandler)
    {
      NetInfo.isConnected.fetch().then(isConnected => 
      {
        if (isConnected) 
          {
        if(this.method.toLowerCase() == "get"){
          var requestParam = {
              method : this.method,
            };
        } else {
          var requestParam = {
              method : this.method,
              body: this.data
            };         
        }

        Utils.logData('Final Params  '+JSON.stringify(requestParam));
        fetch(this.url, requestParam)
          .then((response) =>
          {
            var status = response.status.toString();
            Utils.logData('status code===========================================>'+status+'<===========================================');
            Utils.logData('response===========================================>'+JSON.stringify(response)+'<===========================================');
            switch (status) {
            case '200':
                if(response._bodyText != ""){
                  response.json().then((responseData) =>
                  {
                    successHandler(responseData);
                  });
                } else {
                  successHandler({});
                }
                break;
            case '204':
              successHandler({});
                break;
              case '404':
              case '403':
              case '401':
              case '400':
                  errorHandler("Something went wrong.", status);
                  break;
              default:
                  errorHandler("Something went wrong", status);
             }
          }
        )
        .catch(function(err) {
            errorHandler("Something went wrong." + err, 100);
        });
      }
      else
      {
        Utils.logData("network not available")
        networkHandler() 
      }
      });
      
    }
}

