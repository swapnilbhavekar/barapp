import Urls from "./Helpers";

export default class RequestHelper {
  constructor(){
  }

  loginUserRequest(){
         var REQUEST_URL = Urls.loginUserRequest;   
         return REQUEST_URL;
  }

  registerUserRequest(){
         var REQUEST_URL = Urls.registerUserRequest;   
         return REQUEST_URL;
  }

  generateOTPRequest(){
  		var REQUEST_URL = Urls.generateOTPRequest;   
        return REQUEST_URL;
  }

  verifyOTPRequest(){
  		var REQUEST_URL = Urls.verifyOTPRequest;   
        return REQUEST_URL;
  }

  restPasswordRequest(){
      var REQUEST_URL = Urls.restPasswordRequest;   
        return REQUEST_URL;
  }
  
  getHomePageAreaAndCuisines(){
         var REQUEST_URL = Urls.getHomePageAreaAndCuisines;   
         return REQUEST_URL;
  }
  getBrowserByCuisines(){
         var REQUEST_URL = Urls.getBrowserByCuisines;   
         return REQUEST_URL;
  }
  getBrowserByAreas(){
         var REQUEST_URL = Urls.getBrowserByAreas;   
         return REQUEST_URL;
  }
   getRestaurants(){
         var REQUEST_URL = Urls.getRestaurants;
         return REQUEST_URL;
         }   
  checkUserDetails(){
         var REQUEST_URL = Urls.checkUserDetails;   
         return REQUEST_URL;
  }
  getLocationDetails(latitude,longitude,googleApiKey){
         var REQUEST_URL = Urls.getLocationDetails; 
         REQUEST_URL = REQUEST_URL.replace('latitude',latitude)  
         REQUEST_URL = REQUEST_URL.replace('longitude',longitude)  
         REQUEST_URL = REQUEST_URL.replace('googleApiKey',googleApiKey)  
         return REQUEST_URL;
  }
  getAutoCompleteAddress(searchText,googleApiKey){
         var REQUEST_URL = Urls.getAutoCompleteAddress; 
         REQUEST_URL = REQUEST_URL.replace('SEARCHTEXT',searchText)  
         REQUEST_URL = REQUEST_URL.replace('googleApiKey',googleApiKey)  
         return REQUEST_URL;
  }
  getLatLongFromPlaceId(placeId,googleApiKey){
         var REQUEST_URL = Urls.getLatLongFromPlaceId; 
         REQUEST_URL = REQUEST_URL.replace('PLACE_ID',placeId)  
         REQUEST_URL = REQUEST_URL.replace('googleApiKey',googleApiKey)  
         return REQUEST_URL;
  }
  addRemoveToFavourites(){
         var REQUEST_URL = Urls.addRemoveToFavourites;   
         return REQUEST_URL;
  }
  getFavourites(){
         var REQUEST_URL = Urls.getFavourites;   
         return REQUEST_URL;
  }
  getRestaurantDetails(){
         var REQUEST_URL = Urls.getRestaurantDetails;   
         return REQUEST_URL;
  }
  getRestaurantPhotosByType(){
         var REQUEST_URL = Urls.getRestaurantPhotosByType;   
         return REQUEST_URL;
  }
}