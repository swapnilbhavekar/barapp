import React from "react";
import Toast from 'react-native-root-toast';
import Utils from "./Utils";


class ToastUtils
{
  static showToast(message) 
  {
    let toast = Toast.show(message, {
    duration: Toast.durations.LONG,
    position: Toast.positions.CENTER,
    shadow: true,
    animation: true,
    hideOnPress: true,
    delay: 0,
    backgroundColor:'white',
    textColor: 'black',
    onShow: () => {
        Utils.logData('toastttttt onShow')
        // calls on toast\`s appear animation start
    },
    onShown: () => {
        Utils.logData('toastttttt onShown')
        // calls on toast\`s appear animation end.
    },
    onHide: () => {
        Utils.logData('toastttttt onHide')
        // calls on toast\`s hide animation start.
    },
    onHidden: () => {
        Utils.logData('toastttttt onHidden')
        // calls on toast\`s hide animation end.
    }
});
// You can manually hide the Toast, or it will automatically disappear after a `duration` ms timeout.
/*setTimeout(function () {
    Toast.hide(toast);
}, 500);*/
  }

  static showErrorToast(message) 
  {
    let toast = Toast.show(message, {
    duration: Toast.durations.LONG,
    position: Toast.positions.BOTTOM,
    shadow: true,
    animation: true,
    hideOnPress: true,
    delay: 0,
    backgroundColor:'#F44336',
    textColor: 'white',
    onShow: () => {
        Utils.logData('toastttttt onShow')
    },
    onShown: () => {
        Utils.logData('toastttttt onShown')
    },
    onHide: () => {
        Utils.logData('toastttttt onHide')
    },
    onHidden: () => {
        Utils.logData('toastttttt onHidden')
    }
});
  }

  static showSuccessToast(message) 
  {
    let toast = Toast.show(message, {
    duration: Toast.durations.LONG,
    position: Toast.positions.BOTTOM,
    shadow: true,
    animation: true,
    hideOnPress: true,
    delay: 0,
    backgroundColor:'#4CAF50',
    textColor: 'white',
    onShow: () => {
        Utils.logData('toastttttt onShow')
    },
    onShown: () => {
        Utils.logData('toastttttt onShown')
    },
    onHide: () => {
        Utils.logData('toastttttt onHide')
    },
    onHidden: () => {
        Utils.logData('toastttttt onHidden')
    }
});
  }

}
module.exports = ToastUtils