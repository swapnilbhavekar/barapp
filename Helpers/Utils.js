import React from "react";
import {Platform} from 'react-native';
import {AsyncStorage} from 'react-native';
import call from 'react-native-phone-call'
import constants from "./AppConstants";


class Utils{
  static logData (logValue) {
      if (__DEV__){
          console.log(logValue);
        }
    }

  static isIOS () {
  	if (Platform.OS === 'ios')
  		return true
  	else
  		return false
    }

  static platformVersion () {
    if (Platform.OS === 'android')
      return Platform.Version;
    else
      return 0;
    }

  static saveUserDetails (userDetails) {
      var  appData = { 
         'user_id': userDetails.user_id,
         'user_name': userDetails.user_name,
         'mobile': userDetails.mobile,
         'email': userDetails.email,
         'password': userDetails.password,
      }      
      AsyncStorage.setItem('userData', JSON.stringify(appData));
    }

    static getDisplayAddressFromGoogleApiResponse(responseJson)
    {
        var users_formatted_address = ''
        responseJson.results[constants.indexOfComponentToGet].address_components.forEach(component => {
          this.logData('component '+JSON.stringify(component))

          if (component.types.indexOf('political') !== -1 && component.types.indexOf('sublocality') !== -1 && component.types.indexOf('sublocality_level_1') !== -1) {
              this.logData('001 adress to show'+users_formatted_address)
              users_formatted_address = users_formatted_address + " "+component.long_name
          }

          if (component.types.indexOf('political') !== -1 && component.types.indexOf('locality') !== -1) {
              this.logData('002 adress to show'+users_formatted_address)
              users_formatted_address = users_formatted_address + ", "+component.long_name
          }

          if (component.types.indexOf('country') !== -1) {
              this.logData('003 adress to show'+users_formatted_address)
              users_formatted_address = users_formatted_address + ", "+component.short_name
          }
          
          
          /*if (component.types.indexOf('country') !== -1) {
            this.setState({ country: component.long_name });
          }*/
          //users_formatted_address = responseJson.results[5].formatted_address
      });
        this.logData('final adress to show'+users_formatted_address)
        return users_formatted_address
    }

    static getDisplayAddressFromGoogleApiResponses(responseJson)
    {
        var users_formatted_address = ''
        responseJson.results[constants.indexOfComponentToGet].address_components.forEach(component => {
          this.logData('component '+JSON.stringify(component))

          if (component.types.indexOf('political') !== -1 && component.types.indexOf('sublocality') !== -1 && component.types.indexOf('sublocality_level_1') !== -1) {
              this.logData('001 adress to show'+users_formatted_address)
              users_formatted_address = users_formatted_address + " "+component.long_name
          }

          if (component.types.indexOf('political') !== -1 && component.types.indexOf('locality') !== -1) {
              this.logData('002 adress to show'+users_formatted_address)
              users_formatted_address = users_formatted_address + ", "+component.long_name
          }

          if (component.types.indexOf('country') !== -1) {
              this.logData('003 adress to show'+users_formatted_address)
              users_formatted_address = users_formatted_address + ", "+component.short_name
          }
          this.logData('final adress to show'+users_formatted_address)
          return users_formatted_address
          /*if (component.types.indexOf('country') !== -1) {
            this.setState({ country: component.long_name });
          }*/
          //users_formatted_address = responseJson.results[5].formatted_address
      });
    }

    static saveSelectedRestaurant(restaurant){
        var selectedRestaurantsArray = []
        AsyncStorage.getItem('recentRestaurants', (err, data) => {
        this.logData('data   '+JSON.stringify(data))   
        this.logData('err   '+JSON.stringify(err))
        if (data != null && data != undefined) {
            var restaurantJSONObject = JSON.parse(data)
            if (restaurantJSONObject != null && restaurantJSONObject!= undefined) {

                    //if already searched same restaurant so no need to store it.
                    var canStoreRestaurant = true;
                    for (var i = 0; i < restaurantJSONObject.length; i++) 
                    {
                        if (restaurantJSONObject[i].id == restaurant.id) {
                            canStoreRestaurant = false;
                            break;
                        }
                    }
                    if (canStoreRestaurant == false){
                        return                        
                    } 



                if (restaurantJSONObject.length>=5) {
                    //remove first index search and add current search result at last
                    for (var i = 1; i < restaurantJSONObject.length; i++) 
                    {
                        selectedRestaurantsArray.push(restaurantJSONObject[i])
                    }
                    selectedRestaurantsArray.push(restaurant)
                }
                else
                {
                    //add current searched restaurant in last
                    for (var i = 0; i < restaurantJSONObject.length; i++) 
                    {
                        selectedRestaurantsArray.push(restaurantJSONObject[i])
                    }
                    selectedRestaurantsArray.push(restaurant)
                }
            }
            else
            {
                //create new restaurant object and store
                selectedRestaurantsArray.push(restaurant)
            }
        }
        else
        {
            //create new restaurant object and store
            selectedRestaurantsArray.push(restaurant)
            this.logData('selectedRestaurantsArray 5   '+JSON.stringify(selectedRestaurantsArray))  
        }
        this.logData('data before storing   '+JSON.stringify(selectedRestaurantsArray))   
        AsyncStorage.setItem('recentRestaurants',JSON.stringify(selectedRestaurantsArray))
        })
    }

    static isNotificationRegistered(isRegistered)
    {
      AsyncStorage.setItem('isNotificationRegistered',isRegistered);
    }

    static callToNumber(phoneNo)
    {
      const args = {
      number: phoneNo,
      prompt: false
      }
      call(args).catch(console.error)
    }

    static stringToFloat(stringValue)
    {
      var floatValue = parseFloat(stringValue)
      return floatValue
    }

    static roundOfValue(value)
    {
      var floatValue = this.stringToFloat(value)
      var roundedValue = floatValue.toFixed(1)
      return roundedValue
    }

    static getArrayFromString(stringValue)
    {
      var arrayValue = stringValue.split(',');
      return arrayValue
    }

}
module.exports = Utils
