class Validator{
  static isValidString (value) {
      if (value == '' || value == undefined || value == null)
      	{
      		return false;
        }
        else
        {
        	return true;
        }
    }

    static isValidEmailString (value) {
      let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ;
      if(reg.test(value) === false)
      {
        return false;
      }
      else 
      {
        return true;
      }
    }

    static isPasswordMatches(value1, value2){
      if (value1 == value2){
        return true;
      }
      else{
        return false;
      }
    }
}
module.exports = Validator