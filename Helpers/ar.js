export default {  
  //Login
    userEmailId:'Email Id',
    userPassword:'Password',
    forgotPasswordText:'FORGOT PASSWORD?',
    googleSignInText:'Sign in with Google',
    facebookSignInText:'Sign in with Google',
    loginBtnText: 'CONTINUE',
    loginSignUpBtnText: 'SIGN UP',
    termsandconditionsText:'Lorem ipsum dolor sit amet, consectetur adipiscin elit, sed do eiusmod tempor, ',
    termsofuserandconditionsText:'TERMS OF USE',
    termsandconditionsandText:'and',
    privacyPolicyText:'PRIVACY POLICY',
    orText:'Or',

    //SignUp
    signUpNameText:'First Name',
    signUpLastNameText:'Last Name',
    signUpMobileNoText:'Mobile No.',
    bookingAlertsText:'Get your booking & waiting alerts on your phone',
    signUpEmailText:'Email Id',
    signUpPasswordText:'Password',
    signupBtnText:'CONTINUE',

    //Forgot password 
    forgotHeadingText:'FORGOT PASSWORD?',
    forgotInfoText:'Enter the mobile number associated \nwith your account.',
    forgotOTPVerifiyText:'We will send you a OTP to verify',
    forgotPasswordLabel:'Mobile Number',
    forgotPasswordBtn:'SEND',

    //Verification password 
    verficationHeadingText:'VERIFICATION CODE',
    verficationInfoText:'Please type the verification code sent to',
    verficationSubmitBtn:'SUBMIT',
    verficationResendOtpBtn:'RESEND OTP',

    //Reset password 
    resetPasswordHeadingText:'RESET PASSWORD?',
    resetPasswordInfoText:'Enter the mobile number associated with your account.',
    resetPasswordNewPasswordLabel:'New Password',
    resetPasswordConfirmPasswordLabel:'Confirm Password',
    resetPasswordSubmitBtn:'SUBMIT',


    //Home
    homeBannerText:'YOUR LOCATION',
    homeBrowseByCuisineHeading:'Browse by Cuisines',
    homeBrowseByAreasHeading:'Browse by Areas',
    homeCarouselCountWord:' Restaurant',
    homeCarouselViewMoreText:'View More',
    homeBannerCTAText:'Where to eat in Saudia Arabia\'s top cities',
    homeBannerCTAViewMoreText:'View More',


    //Booking
    bookingSpecialRequestText:'Special Request (optional)',
    bookingMobileText:'Mobile Number',
    ctaBtn:'Book',
    

    //BookingTable
    bookingTableHeading:'Booking Table',
    bookingTableUpcomingText:'Upcoming',
    bookingTableHistoryText:'History',
    bookingTableGreenText:'Confirmed',
    bookingTableDateHeadingText:'Date',
    bookingTableGuestHeadingText:'Guest',
    bookingTableNameText:'Name',
    bookingTableCancelBookingText:'CANCEL BOOKING',
    bookingTableAddReviewText:'Add Review',

    //BooingConfirmation
    bookingConfirmationRequestSentText:'Booking Request Sent',
    bookingConfirmationWaitingForConfirmationText:'Waiting For Confirmation',
    bookingConfirmationWaitingText:'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    bookingConfirmationYourDetailsText:'Your Details',
    bookingConfirmationNameText:'Name',
    bookingConfirmationMobileNumberText:'Mobile Number',
    bookingConfirmationAddressText:'Address',
    bookingConfirmationRestaurantDetailsText:'Restaurant Details',
    bookingConfirmationGetDirectionText:'Get Direction',
    bookingConfirmationCallText:'Call',
  };