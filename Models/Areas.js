export default class Areas
{
	id;
	name;
	image;
	latitude;
	longitude;	
	distance_in_km;
	restaurant_count;

	constructor(id,name,image,latitude,longitude,distance_in_km,restaurant_count) 
	{
		this.id = id
		this.name = name
		this.image = image
		this.latitude = latitude
		this.longitude = longitude
		this.distance_in_km = distance_in_km
		this.restaurant_count = restaurant_count
	}
}