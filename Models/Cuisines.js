export default class Cuisines
{
	id;
	name;
	image;
	latitude;
	longitude;
	restaurant_count;

	constructor(id,name,image,latitude,longitude,restaurant_count) 
	{
		this.id = id
		this.name = name
		this.image = image
		this.latitude = latitude
		this.longitude = longitude
		this.restaurant_count = restaurant_count
		
	}
}