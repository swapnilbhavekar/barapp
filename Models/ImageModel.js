export default class ImageModel
{
	path;
	mime;
	size;
	base64Data;

	constructor(path,mime,size,base64Data) 
	{
		this.path = path
		this.mime = mime
		this.size = size
		this.base64Data = base64Data
	}
}