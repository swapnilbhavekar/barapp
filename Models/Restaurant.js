export default class Restaurant
{
	id;
	lattitude;
	longitude;
	distance_in_km;
	cover_image;
	name;
	cuisines;
	city_name;
	avg_cost_per_person;
	turn_time;
	booked_today_count;
	ratings;
	restaurant_address;
	restaurant_info;
	reviews_count;
	offer_title;
	offer_description;
	restaurant_type;
	more_information;
	food_image_count;
	food_image;
	ambience_image_count;
	ambience_image;
	all_image_count;
	all_image;
	top_dishes_people_order;
	people_liked;
	cuisinesArray;
	payment_type;
	payment_type_description;

	constructor(id,lattitude,longitude,distance_in_km,
		cover_image,name,cuisines,city_name,avg_cost_per_person
		,turn_time,booked_today_count,ratings,restaurant_address,
		restaurant_info,reviews_count,offer_title,offer_description,
		restaurant_type,more_information,food_image_count,food_image,
		ambience_image_count,ambience_image,all_image_count,all_image,
		top_dishes_people_order,people_liked,cuisinesArray,payment_type,payment_type_description) 
	{
		this.id = id
		this.lattitude = lattitude
		this.longitude = longitude
		this.distance_in_km = distance_in_km
		this.cover_image = cover_image
		this.name = name
		this.cuisines = cuisines
		this.city_name = city_name
		this.avg_cost_per_person = avg_cost_per_person
		this.turn_time = turn_time
		this.booked_today_count = booked_today_count
		this.ratings = ratings
		this.restaurant_address = restaurant_address
		this.restaurant_info = restaurant_info
		this.reviews_count = reviews_count
		this.offer_title = offer_title
		this.offer_description = offer_description
		this.restaurant_type = restaurant_type
		this.more_information = more_information
		this.food_image_count = food_image_count
		this.food_image = food_image
		this.ambience_image_count = ambience_image_count
		this.ambience_image = ambience_image
		this.all_image_count = all_image_count
		this.all_image = all_image
		this.top_dishes_people_order = top_dishes_people_order
		this.people_liked = people_liked
		this.cuisinesArray = cuisinesArray
		this.payment_type = payment_type
		this.payment_type_description = payment_type_description
	}
}