const RestaurantByType = {
  CUISINES: "cuisines",
  AREAS: "areas"
}
export default RestaurantByType;