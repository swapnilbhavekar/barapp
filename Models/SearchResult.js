export default class SearchResult
{
	description;
	place_id;
	latitude;
	longitude;

	constructor(description,place_id,latitude,longitude) 
	{
		this.description = description
		this.place_id = place_id
		this.latitude = latitude
		this.longitude = longitude
	}
}