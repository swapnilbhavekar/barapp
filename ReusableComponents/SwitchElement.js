import React, { Component } from 'react'
import { Switch } from 'react-native'

export default SwitchElement = (props) => {
   return (
      <Switch
      onValueChange = {props.toggleSwitch1}
      value = {props.switch1Value}/>
   )
}