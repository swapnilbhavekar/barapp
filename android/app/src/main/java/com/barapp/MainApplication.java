package com.barapp;

import android.app.Application;

import com.facebook.react.ReactApplication;
import com.ashideas.rnrangeslider.RangeSliderPackage;
import com.zyu.ReactNativeWheelPickerPackage;
import com.reactnative.ivpusic.imagepicker.PickerPackage;
import com.swmansion.reanimated.ReanimatedPackage;
import com.microsoft.codepush.react.CodePush;
import com.AlexanderZaytsev.RNI18n.RNI18nPackage;
import io.fullstack.oauth.OAuthManagerPackage;
import com.oblador.vectoricons.VectorIconsPackage;
import com.swmansion.gesturehandler.react.RNGestureHandlerPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;

import java.util.Arrays;
import java.util.List;
import com.agontuk.RNFusedLocation.RNFusedLocationPackage;
import com.airbnb.android.react.maps.MapsPackage;

public class MainApplication extends Application implements ReactApplication {

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {

        @Override
        protected String getJSBundleFile() {
        return CodePush.getJSBundleFile();
        }
    
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
          new MainReactPackage(),
            new RangeSliderPackage(),
            new ReactNativeWheelPickerPackage(),
            new PickerPackage(),
            new ReanimatedPackage(),
            new CodePush(getResources().getString(R.string.reactNativeCodePush_androidDeploymentKey), getApplicationContext(), BuildConfig.DEBUG),
            new RNI18nPackage(),
            new OAuthManagerPackage(),
            new VectorIconsPackage(),
            new RNGestureHandlerPackage(),
            new RNFusedLocationPackage(),
            new MapsPackage()
      );
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    SoLoader.init(this, /* native exopackage */ false);
  }
}
